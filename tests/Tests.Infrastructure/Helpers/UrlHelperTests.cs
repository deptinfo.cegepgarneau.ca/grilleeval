﻿using Infrastructure.Helpers;

namespace Tests.Infrastructure.Helpers;

public class UrlHelperTests
{
    //private const string ANY_BASE_URL = "https://localhost:7101";
    
    //[Fact]
    //public void GivenNoQueryParameters_WhenBuildUriWithQueryParameters_ThenReturnBaseUrlOnly()
    //{
    //    // Arrange
    //    var queryParameters = new List<KeyValuePair<string, string>>();

    //    // Act
    //    var uri = UrlHelper.BuildUriWithQueryParameters(ANY_BASE_URL, queryParameters);
        
    //    // Assert
    //    uri.ShouldBe(ANY_BASE_URL);
    //}
    
    //[Fact]
    //public void GivenQueryParameters_WhenBuildUriWithQueryParameters_ThenReturnUrlStartingWithBaseUrl()
    //{
    //    // Arrange
    //    var queryParameters = new List<KeyValuePair<string, string>>
    //    {
    //        new("page", "0"),
    //        new("perPage", "100")
    //    };
        
    //    // Act
    //    var uri = UrlHelper.BuildUriWithQueryParameters(ANY_BASE_URL, queryParameters);
        
    //    // Assert
    //    uri.ShouldStartWith(ANY_BASE_URL);
    //}
    
    //[Fact]
    //public void GivenQueryParameters_WhenBuildUriWithQueryParameters_TheReturnUrlDoesNotEndWithAmpersand()
    //{
    //    // Arrange
    //    var queryParameters = new List<KeyValuePair<string, string>>
    //    {
    //        new("page", "0"),
    //        new("perPage", "100")
    //    };
        
    //    // Act
    //    var uri = UrlHelper.BuildUriWithQueryParameters(ANY_BASE_URL, queryParameters);
        
    //    // Assert
    //    uri.ShouldNotEndWith("&");
    //}
    
    //[Fact]
    //public void GivenQueryParameters_WhenBuildUriWithQueryParameters_TheReturnUrlIncludesAllParameters()
    //{
    //    // Arrange
    //    var queryParameters = new List<KeyValuePair<string, string>>
    //    {
    //        new("page", "0"),
    //        new("perPage", "100")
    //    };
        
    //    // Act
    //    var uri = UrlHelper.BuildUriWithQueryParameters(ANY_BASE_URL, queryParameters);
        
    //    // Assert
    //    uri.ShouldBe($"{ANY_BASE_URL}?page=0&perPage=100");
    //}
}