﻿////using Application.Extensions;
//using Infrastructure.ExternalApis.Azure.Exceptions;

//namespace Tests.Infrastructure.ExternalApis.Azure.Exceptions;

//public class AzureApiExceptionTests
//{
//    private const string SOME_MESSAGE = "Could not upload file to azure blob.";

//    [Fact]
//    public void WhenErrorObject_ThenErrorTypeShouldBeAzureApiException()
//    {
//        // Arrange
//        var azureApiException = new AzureApiException(SOME_MESSAGE);

//        // Act
//        var actual = azureApiException.ErrorObject();

//        // Arrange
//        actual.ErrorType.ShouldBe("AzureApiException");
//    }

//    [Fact]
//    public void WhenErrorObject_ThenErrorMessageShouldBeSpecifiedMessage()
//    {
//        // Arrange
//        var azureApiException = new AzureApiException(SOME_MESSAGE);

//        // Act
//        var actual = azureApiException.ErrorObject();

//        // Arrange
//        actual.ErrorMessage.ShouldBe(SOME_MESSAGE);
//    }
//}