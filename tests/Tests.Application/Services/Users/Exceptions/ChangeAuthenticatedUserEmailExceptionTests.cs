﻿//using Application.Extensions;
//using Application.Services.Users.Exceptions;

//namespace Tests.Application.Services.Users.Exceptions;

//public class ChangeAuthenticatedUserEmailExceptionTests
//{
//    private const string ANY_MESSAGE = "Could not change authenticated user email.";
    
//    [Fact]
//    public void WhenErrorObject_ThenErrorTypeShouldBeChangeAuthenticatedUserEmailException()
//    {
//        // Arrange
//        var changeAuthenticatedUserEmailException = new ChangeAuthenticatedUserEmailException(ANY_MESSAGE);
        
//        // Act
//        var actual = changeAuthenticatedUserEmailException.ErrorObject();
        
//        // Assert
//        actual.ErrorType.ShouldBe("ChangeAuthenticatedUserEmailException");
//    }
    
//    [Fact]
//    public void WhenErrorObject_ThenErrorMessageShouldBeSpecifiedMessage()
//    {
//        // Arrange
//        var changeAuthenticatedUserEmailException = new ChangeAuthenticatedUserEmailException(ANY_MESSAGE);
        
//        // Act
//        var actual = changeAuthenticatedUserEmailException.ErrorObject();
        
//        // Assert
//        actual.ErrorMessage.ShouldBe(ANY_MESSAGE);
//    }
//}