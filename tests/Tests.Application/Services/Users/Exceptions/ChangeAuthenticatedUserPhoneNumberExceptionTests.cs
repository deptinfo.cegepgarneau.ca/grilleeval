﻿//using Application.Extensions;
//using Application.Services.Users.Exceptions;

//namespace Tests.Application.Services.Users.Exceptions;

//public class ChangeAuthenticatedUserPhoneNumberExceptionTests
//{
//    private const string ANY_MESSAGE = "Could not change authenticated user phone number.";
    
//    [Fact]
//    public void WhenErrorObject_ThenErrorTypeShouldBeChangeAuthenticatedPhoneNumberException()
//    {
//        // Arrange
//        var changeAuthenticatedPhoneNumberException = new ChangeAuthenticatedPhoneNumberException(ANY_MESSAGE);
        
//        // Act
//        var actual = changeAuthenticatedPhoneNumberException.ErrorObject();
        
//        // Assert
//        actual.ErrorType.ShouldBe("ChangeAuthenticatedPhoneNumberException");
//    }
    
//    [Fact]
//    public void WhenErrorObject_ThenErrorMessageShouldBeSpecifiedMessage()
//    {
//        // Arrange
//        var changeAuthenticatedPhoneNumberException = new ChangeAuthenticatedPhoneNumberException(ANY_MESSAGE);
        
//        // Act
//        var actual = changeAuthenticatedPhoneNumberException.ErrorObject();
        
//        // Assert
//        actual.ErrorMessage.ShouldBe(ANY_MESSAGE);
//    }
//}