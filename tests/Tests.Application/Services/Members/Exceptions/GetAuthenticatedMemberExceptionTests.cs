﻿using Application.Extensions;
using Application.Services.Members.Exceptions;

namespace Tests.Application.Services.Members.Exceptions;

public class GetAuthenticatedMemberExceptionTests
{
    private const string ANY_MESSAGE = "Could not get authenticated member.";
    
    [Fact]
    public void WhenErrorObject_ThenErrorTypeShouldBeGetAuthenticatedMemberException()
    {
        // Arrange
        var getAuthenticatedMemberException = new GetAuthenticatedMemberException(ANY_MESSAGE);
        
        // Act
        var actual = getAuthenticatedMemberException.ErrorObject();
        
        // Assert
        actual.ErrorType.ShouldBe("GetAuthenticatedMemberException");
    }
    
    [Fact]
    public void WhenErrorObject_ThenErrorMessageShouldBeSpecifiedMessage()
    {
        // Arrange
        var getAuthenticatedMemberException = new GetAuthenticatedMemberException(ANY_MESSAGE);
        
        // Act
        var actual = getAuthenticatedMemberException.ErrorObject();
        
        // Assert
        actual.ErrorMessage.ShouldBe(ANY_MESSAGE);
    }
}