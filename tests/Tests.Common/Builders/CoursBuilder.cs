﻿using Domain.Entities.DossierCours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Common.Builders
{
    public class CoursBuilder
    {
        public Cours Build()
        {
            var cours = new Cours();
            cours.Code = "420-13E-FX";
            cours.NomCours = "Informatique";
            return cours;
        }
    }
}
