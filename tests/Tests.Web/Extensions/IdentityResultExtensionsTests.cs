﻿//using Microsoft.AspNetCore.Identity;
//using Microsoft.Extensions.Localization;
//using Web.Constants;
//using Web.Extensions;

//namespace Tests.Web.Extensions;

//public class IdentityResultExtensionsTests
//{
//    private readonly Mock<IStringLocalizer> _localizer;
    
//    public IdentityResultExtensionsTests()
//    {
//        _localizer = new Mock<IStringLocalizer>();
//    }
    
//    [Fact]
//    public void GivenIdentityResultHasExceptionOfTypeUserAlreadyHasPassword_WhenGetErrorMessageForIdentityResultException_ThenReturnUserAlreadyHasPasswordMessage()
//    {
//        // Arrange
//        const string ERROR_MESSAGE_KEY = "UserAlreadyHasPassword";
//        const string ERROR_MESSAGE = "User already has a password.";
//        _localizer.Setup(x => x[ERROR_MESSAGE_KEY]).Returns(new LocalizedString(ERROR_MESSAGE_KEY, ERROR_MESSAGE));
//        var error = new IdentityError { Code = IdentityResultExceptions.USER_ALREADY_HAS_PASSWORD };
//        var identityResult = IdentityResult.Failed(error);

//        // Act
//        var message = identityResult.GetErrorMessageForIdentityResultException(_localizer.Object);
        
//        message.ShouldBe(ERROR_MESSAGE);
//    }
    
//    [Fact]
//    public void GivenIdentityResultWithoutErrorCodes_WhenGetErrorMessageForIdentityResultException_ThenReturnCouldNotChangePasswordMessage()
//    {
//        // Arrange
//        const string ERROR_MESSAGE_KEY = "CouldNotChangePassword";
//        const string ERROR_MESSAGE = "Could not change password";
//        _localizer.Setup(x => x[ERROR_MESSAGE_KEY]).Returns(new LocalizedString(ERROR_MESSAGE_KEY, ERROR_MESSAGE));
//        var identityResult = new IdentityResult();
        
//        // Act
//        var message = identityResult.GetErrorMessageForIdentityResultException(_localizer.Object);
        
//        message.ShouldBe(ERROR_MESSAGE);
//    }
    
//    [Fact]
//    public void GivenHasErrorOtherThanUserAlreadyHasPassword_WhenGetErrorMessageForIdentityResultException_ThenReturnMessageThatIsNotUserAlreadyHasPassword()
//    {
//        // Arrange
//        const string ERROR_MESSAGE_KEY = "UserAlreadyHasPassword";
//        const string ERROR_MESSAGE = "User already has a password.";
//        _localizer.Setup(x => x[ERROR_MESSAGE_KEY]).Returns(new LocalizedString(ERROR_MESSAGE_KEY, ERROR_MESSAGE));
//        var error = new IdentityError { Code = "OTHER_CODE" };
//        var identityResult = IdentityResult.Failed(error);

//        // Act
//        var message = identityResult.GetErrorMessageForIdentityResultException(_localizer.Object);
        
//        message.ShouldNotBe(ERROR_MESSAGE);
//    }
//}