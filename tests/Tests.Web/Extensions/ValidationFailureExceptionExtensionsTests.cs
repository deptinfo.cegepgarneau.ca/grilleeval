﻿//using Application.Extensions;
//using FastEndpoints;
//using FluentValidation.Results;
//using Web.Extensions;

//namespace Tests.Web.Extensions;

//public class ValidationFailureExceptionExtensionsTests
//{
//    private const string ANY_ERROR_CODE = "ErrorCode";
//    private const string ANY_ERROR_MESSAGE = "Some error message";
    
//    [Fact]
//    public void GivenExceptionWithNullListOfFailures_WhenErrorObjects_ThenReturnEmptyList()
//    {
//        // Arrange
//        var exception = new ValidationFailureException();

//        // Act
//        var errors = exception.ErrorObjects();
        
//        errors.ShouldBeEmpty();
//    }
    
//    [Fact]
//    public void GivenExceptionWithFailures_WhenErrorObjects_ThenReturnListOfError()
//    {
//        // Arrange
//        var failure = new ValidationFailure { ErrorCode = ANY_ERROR_CODE, ErrorMessage = ANY_ERROR_MESSAGE };
//        var exception = new ValidationFailureException(failure.IntoList(), ANY_ERROR_MESSAGE);

//        // Act
//        var errors = exception.ErrorObjects();
        
//        errors.ShouldNotBeEmpty();
//    }
    
//    [Fact]
//    public void GivenExceptionWithFailures_WhenErrorObjects_ThenReturnedErrorTypeShouldBeFailureErrorCode()
//    {
//        // Arrange
//        var failure = new ValidationFailure { ErrorCode = ANY_ERROR_CODE, ErrorMessage = ANY_ERROR_MESSAGE };
//        var exception = new ValidationFailureException(failure.IntoList(), ANY_ERROR_MESSAGE);

//        // Act
//        var errors = exception.ErrorObjects();
        
//        errors.ShouldContain(x => x.ErrorType == ANY_ERROR_CODE);
//    }
    
//    [Fact]
//    public void GivenExceptionWithFailures_WhenErrorObjects_ThenReturnedErrorMessageShouldBeFailureErrorMessage()
//    {
//        // Arrange
//        var failure = new ValidationFailure { ErrorCode = ANY_ERROR_CODE, ErrorMessage = ANY_ERROR_MESSAGE };
//        var exception = new ValidationFailureException(failure.IntoList(), ANY_ERROR_MESSAGE);

//        // Act
//        var errors = exception.ErrorObjects();
        
//        errors.ShouldContain(x => x.ErrorType == ANY_ERROR_CODE);
//    }
//}