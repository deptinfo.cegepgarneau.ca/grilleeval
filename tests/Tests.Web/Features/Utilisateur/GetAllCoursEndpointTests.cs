﻿// using Application.Extensions;
// using Domain.Constants.User;
// using Domain.Repositories;
// using FastEndpoints;
// using Infrastructure.Repositories.Cours;
// using Microsoft.AspNetCore.Authentication.Cookies;
// using Microsoft.AspNetCore.Http;
// using Tests.Common.Builders;
// using Tests.Common.Mapping;
// using Web.Features.Utilisateur.Cours;
// using Web.Features.Utilisateur.Cours.GetAllCours;
// using Web.Mapping.Profiles;

// namespace Tests.Web.Features.Utilisateur
// {
//     public class GetAllCoursEndpointTests
//     {
//         private readonly Mock<ICoursRepository> _coursRepository;
//         private readonly CoursBuilder _coursBuilder;


//         private readonly GetAllCoursEndpoint _endPoint;

//         public GetAllCoursEndpointTests()
//         {
//             _coursBuilder = new CoursBuilder();

//             _coursRepository = new Mock<ICoursRepository>();

//             _endPoint = Factory.Create<GetAllCoursEndpoint>(
//                 new MapperBuilder().WithProfile<ResponseMappingProfile>().Build(),
//                 _coursRepository.Object
//             );
//         }

//         [Fact]
//         public void WhenConfigure_ThenConfigureVerbToBeGet()
//         {
//             // Act
//             _endPoint.Configure();

//             // Assert
//             _endPoint.Definition.Verbs!.ShouldContain(Http.GET.ToString());
//         }

//         [Fact]
//         public void WhenConfigure_ThenConfigureRoute()
//         {
//             // Act
//             _endPoint.Configure();

//             // Assert
//             _endPoint.Definition.Routes!.ShouldContain("cours");
//         }

//         [Fact]
//         public void WhenConfigure_ThenConfigureAllowedRoles()
//         {
//             // Act
//             _endPoint.Configure();

//             // Assert
//             _endPoint.Definition.AllowedRoles!.ShouldContain(Roles.ADMINISTRATOR);
//         }

//         [Fact]
//         public void WhenConfigure_ThenConfigureAuthSchemeToBeCookieAuthenticationScheme()
//         {
//             // Act
//             _endPoint.Configure();

//             // Assert
//             _endPoint.Definition.AuthSchemeNames!.ShouldContain(CookieAuthenticationDefaults.AuthenticationScheme);
//         }

//         [Fact]
//         public async Task WhenHandleAsync_ThenReturnOkResult()
//         {
//             // Act
//             await _endPoint.HandleAsync(default);

//             // Assert
//             _endPoint.HttpContext.Response.StatusCode.ShouldBe(StatusCodes.Status200OK);
//         }

//         [Fact]
//         public async Task WhenHandleAsync_ThenDelegateGettingCoursWithIdToCoursRepository()
//         {
//             // Act
//             await _endPoint.HandleAsync(default);

//             // Assert
//             _coursRepository.Verify(x => x.GetAll());
//         }

//         [Fact]
//         public async Task WhenHandleAsync_ThenReturnCoursFoundByRepositoryAsCoursDto()
//         {
//             // Arrange
//             var cours = _coursBuilder.Build();
//             _coursRepository.Setup(x => x.GetAll()).Returns(cours.IntoList());
            
//             // Act
//             await _endPoint.HandleAsync(default);

//             // Assert
//             _endPoint.Response.ShouldContain(x => x.Id == cours.Id);
//         }
//     }
// }
