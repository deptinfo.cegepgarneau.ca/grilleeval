﻿//using System.Security.Claims;
//using Application.Extensions;
//using Application.Interfaces.Services.Notifications;
//using Core.Flash;
//using Domain.Entities.Identity;
//using Domain.Repositories;
//using Microsoft.AspNetCore.Authentication;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Mvc.Routing;
//using Microsoft.AspNetCore.Mvc.ViewFeatures;
//using Microsoft.Extensions.Localization;
//using Microsoft.Extensions.Logging;
//using Tests.Common.Builders;
//using Tests.Common.Fixtures;
//using Tests.Web.TestCollections;
//using Web.Controllers;
//using Web.ViewModels.Authentication;
//using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

//namespace Tests.Web.Controllers;

//[Collection(WebTestCollection.NAME)]
//public class AuthenticationControllerTests
//{
//    private const string ANY_PASSWORD = "Qwerty123!";
//    private const string OTHER_PASSWORD = "QwertyQwerty123!";
//    private const string ANOTHER_PASSWORD = "123!Qwerty123!";
//    private const string ANY_USERNAME = "john.doe@gmail.com";
//    private const string INEXISTANT_USER_EMAIL = "john.doe12@gmail.com";
//    private const string ANY_RETURN_URL = "/connect/authorize?";
//    private const string ANY_CODE = "454689";
//    private const string ANY_ERROR_MESSAGE = "Invalid code.";
//    private const string ANY_ERROR_MESSAGE_KEY = "InvalidCode";

//    private readonly TestFixture _testFixture;
//    private readonly UserBuilder _userBuilder;

//    private readonly Mock<IFlasher> _flasher;
//    private readonly Mock<UserManager<User>> _userManager;
//    private readonly Mock<SignInManager<User>> _signInManager;
//    private readonly Mock<IMemberRepository> _memberRepository;
//    private readonly Mock<INotificationService> _notificationService;

//    private readonly AuthenticationController _authenticationController;

//    public AuthenticationControllerTests(TestFixture testFixture)
//    {
//        _testFixture = testFixture;
//        _userBuilder = new UserBuilder();
//        _testFixture.ResetBuilders();

//        _flasher = new Mock<IFlasher>();
//        _notificationService = new Mock<INotificationService>();
//        var logger = new Mock<ILogger<AuthenticationController>>();
//        var localizer = new Mock<IStringLocalizer<AuthenticationController>>();
//        localizer
//            .Setup(x => x[It.IsAny<string>()])
//            .Returns(new LocalizedString(ANY_ERROR_MESSAGE_KEY, ANY_ERROR_MESSAGE));
//        var store = new Mock<IUserStore<User>>();
//        _userManager = new Mock<UserManager<User>>(store.Object, null!, null!, null!, null!, null!, null!, null!, null!);

//        _signInManager = new Mock<SignInManager<User>>(
//            _userManager.Object,
//            Mock.Of<IHttpContextAccessor>(),
//            Mock.Of<IUserClaimsPrincipalFactory<User>>(),
//            null!, null!, null!, null!);
//        _memberRepository = new Mock<IMemberRepository>();

//        _authenticationController = new AuthenticationController(
//            logger.Object,
//            _flasher.Object,
//            _signInManager.Object,
//            _memberRepository.Object,
//            _notificationService.Object,
//            localizer.Object)
//        {
//            ControllerContext = new ControllerContext
//            {
//                HttpContext = new DefaultHttpContext { RequestServices = BuildControllerServiceProvider().Object }
//            }
//        };
//    }

//    [Fact]
//    public void WhenGetLogin_ThenReturnViewResult()
//    {
//        // Act
//        var actionResult = _authenticationController.Login(ANY_RETURN_URL);

//        // Assert
//        actionResult.ShouldBeOfType<ViewResult>();
//    }

//    [Fact]
//    public void WhenGetLogin_ThenReturnLoginViewModelLoginViewModelReturnUrlPropertyIsSameAsGivenUrl()
//    {
//        // Act
//        var viewResult = _authenticationController.Login(ANY_RETURN_URL) as ViewResult;

//        // Assert
//        viewResult?.Model.ShouldBeOfType<LoginViewModel>().ReturnUrl.ShouldBe(ANY_RETURN_URL);
//    }

//    [Fact]
//    public async Task GivenNullUserName_WhenPostLogin_ThenModelStateShouldNotBeValid()
//    {
//        // Arrange
//        var viewModel = new LoginViewModel { UserName = null!, Password = ANY_PASSWORD, ReturnUrl = ANY_RETURN_URL };
//        _authenticationController.ModelState.AddModelError(nameof(viewModel.UserName), "");

//        // Act
//        await _authenticationController.Login(viewModel);

//        // Assert
//        _authenticationController.ModelState.IsValid.ShouldBeFalse();
//    }

//    [Fact]
//    public async Task GivenNullPassword_WhenPostLogin_ThenModelStateShouldNotBeValid()
//    {
//        // Arrange
//        var user = await _testFixture.GivenUserInDatabase();
//        var viewModel = new LoginViewModel { UserName = user.UserName, Password = null!, ReturnUrl = ANY_RETURN_URL };
//        _authenticationController.ModelState.AddModelError(nameof(viewModel.Password), "");

//        // Act
//        await _authenticationController.Login(viewModel);

//        // Assert
//        _authenticationController.ModelState.IsValid.ShouldBeFalse();
//    }

//    [Fact]
//    public async Task GivenNoUserWithUserNameExists_WhenPostLogin_ThenDisplayWarning()
//    {
//        // Arrange
//        var viewModel = new LoginViewModel { UserName = INEXISTANT_USER_EMAIL, Password = null!, ReturnUrl = ANY_RETURN_URL };

//        // Act
//        await _authenticationController.Login(viewModel);

//        // Assert
//        _flasher.Verify(x => x.Flash(Types.Warning, It.IsAny<string>(), true));
//    }

//    [Fact]
//    public async Task GivenUserWithUserNameWasDeleted_WhenPostLogin_ThenDisplayWarning()
//    {
//        // Arrange
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).AsDeleted().Build();
//        _userManager.Setup(x => x.Users).Returns(user.IntoList().AsQueryable);
//        var viewModel = new LoginViewModel { UserName = user.Email, Password = null!, ReturnUrl = ANY_RETURN_URL };

//        // Act
//        await _authenticationController.Login(viewModel);

//        // Assert
//        _flasher.Verify(x => x.Flash(Types.Warning, It.IsAny<string>(), true));
//    }

//    [Fact]
//    public async Task GivenUserWithUserNameExistsButIsLinkedToNoMember_WhenPostLogin_ThenDisplayWarning()
//    {
//        // Arrange
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).Build();
//        _userManager.Setup(x => x.Users).Returns(user.IntoList().AsQueryable);
//        var viewModel = new LoginViewModel { UserName = user.Email, Password = null!, ReturnUrl = ANY_RETURN_URL };

//        // Act
//        await _authenticationController.Login(viewModel);

//        // Assert
//        _flasher.Verify(x => x.Flash(Types.Warning, It.IsAny<string>(), true));
//    }

//    [Fact]
//    public async Task GivenCheckPasswordFailed_WhenPostLogin_ThenDisplayWarning()
//    {
//        // Arrange
//        var user = GivenUserExistsAndHasMember();
//        _signInManager
//            .Setup(x => x.CheckPasswordSignInAsync(It.IsAny<User>(), It.IsAny<string>(), false))
//            .ReturnsAsync(SignInResult.Failed);

//        var viewModel = new LoginViewModel
//        {
//            UserName = user.UserName,
//            Password = ANY_PASSWORD,
//            ReturnUrl = ANY_RETURN_URL
//        };

//        // Act
//        await _authenticationController.Login(viewModel);

//        // Assert
//        _flasher.Verify(x => x.Flash(Types.Warning, It.IsAny<string>(), true));
//    }

//    [Fact]
//    public async Task GivenCheckPasswordFailed_WhenPostLogin_ThenReturnViewWithSameTwoFactorViewModel()
//    {
//        // Arrange
//        var user = GivenUserExistsAndHasMember();
//        _signInManager
//            .Setup(x => x.CheckPasswordSignInAsync(It.IsAny<User>(), It.IsAny<string>(), false))
//            .ReturnsAsync(SignInResult.Failed);
//        var expectedViewModel = new LoginViewModel
//        {
//            UserName = user.UserName,
//            Password = ANY_PASSWORD,
//            ReturnUrl = ANY_RETURN_URL
//        };

//        // Act
//        var actionResult = await _authenticationController.Login(expectedViewModel);

//        // Assert
//        var viewResult = actionResult.ShouldBeOfType<ViewResult>();
//        var actualViewModel = viewResult.Model.ShouldBeOfType<LoginViewModel>();
//        actualViewModel.UserName.ShouldBe(expectedViewModel.UserName);
//        actualViewModel.Password.ShouldBe(expectedViewModel.Password);
//        actualViewModel.ReturnUrl.ShouldBe(expectedViewModel.ReturnUrl);
//    }

//    [Fact]
//    public async Task GivenPasswordSignInSucceeded_WhenPostLogin_ThenRedirectToVueAppPageView()
//    {
//        // Arrange
//        var user = GivenUserExistsAndHasMember();
//        _signInManager
//            .Setup(x => x.CheckPasswordSignInAsync(It.IsAny<User>(), OTHER_PASSWORD, false))
//            .ReturnsAsync(SignInResult.Success);
//        _signInManager
//            .Setup(x => x.PasswordSignInAsync(It.IsAny<User>(), OTHER_PASSWORD, false, true))
//            .ReturnsAsync(SignInResult.Success);

//        var viewModel = new LoginViewModel
//        {
//            UserName = user.UserName,
//            Password = OTHER_PASSWORD,
//            ReturnUrl = ANY_RETURN_URL
//        };

//        // Act
//        var actionResult = await _authenticationController.Login(viewModel);

//        // Assert
//        var redirectToActionResult = actionResult.ShouldBeOfType<RedirectToActionResult>();
//        redirectToActionResult.ActionName.ShouldBe("Index");
//        redirectToActionResult.ControllerName.ShouldBe("VueApp");
//    }

//    [Fact]
//    public async Task GivenPasswordSignInFails_WhenPostLogin_ThenReturnToView()
//    {
//        // Arrange
//        var user = GivenUserExistsAndHasMember();
//        _signInManager
//            .Setup(x => x.CheckPasswordSignInAsync(It.IsAny<User>(), It.IsAny<string>(), false))
//            .ReturnsAsync(SignInResult.Success);
//        _signInManager
//            .Setup(x => x.PasswordSignInAsync(It.IsAny<User>(), It.IsAny<string>(), false, true))
//            .ReturnsAsync(SignInResult.Failed);

//        var viewModel = new LoginViewModel
//        {
//            UserName = user.UserName,
//            Password = ANY_PASSWORD,
//            ReturnUrl = ANY_RETURN_URL
//        };

//        // Act
//        var actionResult = await _authenticationController.Login(viewModel);

//        // Assert
//        var redirectToActionResult = actionResult.ShouldBeOfType<RedirectToActionResult>();
//        redirectToActionResult.ActionName.ShouldBe("Index");
//        redirectToActionResult.ControllerName.ShouldBe("VueApp");
//    }

//    [Fact]
//    public async Task GivenPasswordRequiresTwoFactor_WhenPostLogin_ThenRedirectToTwoFactorAuthentication()
//    {
//        // Arrange
//        var user = GivenUserExistsAndHasMember();
//        _signInManager
//            .Setup(x => x.CheckPasswordSignInAsync(It.IsAny<User>(), ANOTHER_PASSWORD, false))
//            .ReturnsAsync(SignInResult.Success);
//        _signInManager
//            .Setup(x => x.PasswordSignInAsync(It.IsAny<User>(), ANOTHER_PASSWORD, false, true))
//            .ReturnsAsync(SignInResult.TwoFactorRequired);

//        var viewModel = new LoginViewModel
//        {
//            UserName = user.UserName,
//            Password = ANOTHER_PASSWORD,
//            ReturnUrl = ANY_RETURN_URL
//        };

//        // Act
//        var actionResult = await _authenticationController.Login(viewModel);

//        // Assert
//        actionResult.ShouldBeOfType<RedirectToActionResult>().ActionName.ShouldBe("TwoFactorAuthentication");
//    }

//    [Fact]
//    public async Task GivenNoUserWithEmailExists_WhenGetTwoFactorAuthentication_ThenRedirectToLogin()
//    {
//        // Act
//        var actionResult = await _authenticationController.TwoFactorAuthentication(INEXISTANT_USER_EMAIL, null!);

//        // Assert
//        actionResult.ShouldBeOfType<RedirectToActionResult>().ActionName.ShouldBe("Login");
//    }

//    [Fact]
//    public async Task WhenGetTwoFactorAuthentication_ThenSendTwoFactorAuthenticationCode()
//    {
//        // Arrange
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).Build();
//        _userManager.Setup(x => x.FindByEmailAsync(user.Email)).ReturnsAsync(user);

//        // Act
//        await _authenticationController.TwoFactorAuthentication(user.Email, null!);

//        // Assert
//        _notificationService.Verify(x => x.SendTwoFactorAuthenticationCodeNotification(It.IsAny<string>(), It.IsAny<string>()));
//    }

//    [Fact]
//    public async Task WhenGetTwoFactorAuthentication_ThenReturnTwoFactorViewModelWithSpecifiedEmail()
//    {
//        // Arrange
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).Build();
//        _userManager.Setup(x => x.FindByEmailAsync(user.Email)).ReturnsAsync(user);

//        // Act
//        var actionResult = await _authenticationController.TwoFactorAuthentication(user.Email, null!);

//        // Assert
//        var viewResult = actionResult.ShouldBeOfType<ViewResult>();
//        var viewModel = viewResult.Model.ShouldBeOfType<TwoFactorViewModel>();
//        viewModel.UserName.ShouldBe(user.Email);
//    }

//    [Fact]
//    public async Task WhenGetTwoFactorAuthentication_ThenReturnTwoFactorViewModelWithSpecifiedReturnUrl()
//    {
//        // Arrange
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).Build();
//        _userManager.Setup(x => x.FindByEmailAsync(user.Email)).ReturnsAsync(user);

//        // Act
//        var actionResult = await _authenticationController.TwoFactorAuthentication(user.Email, ANY_RETURN_URL);

//        // Assert
//        var viewResult = actionResult.ShouldBeOfType<ViewResult>();
//        var viewModel = viewResult.Model.ShouldBeOfType<TwoFactorViewModel>();
//        viewModel.ReturnUrl.ShouldBe(ANY_RETURN_URL);
//    }

//    [Fact]
//    public async Task GivenNullUserName_WhenPostTwoFactorAuthentication_ThenModelStateShouldNotBeValid()
//    {
//        // Arrange
//        var viewModel = new TwoFactorViewModel { UserName = null!, TwoFactorCode = ANY_CODE };
//        _authenticationController.ModelState.AddModelError(nameof(viewModel.UserName), "");

//        // Act
//        await _authenticationController.TwoFactorAuthentication(viewModel);

//        // Assert
//        _authenticationController.ModelState.IsValid.ShouldBeFalse();
//    }

//    [Fact]
//    public async Task GivenNullTwoFactorCode_WhenPostTwoFactorAuthentication_ThenModelStateShouldNotBeValid()
//    {
//        // Arrange
//        var viewModel = new TwoFactorViewModel { UserName = ANY_USERNAME, TwoFactorCode = null! };
//        _authenticationController.ModelState.AddModelError(nameof(viewModel.TwoFactorCode), "");

//        // Act
//        await _authenticationController.TwoFactorAuthentication(viewModel);

//        // Assert
//        _authenticationController.ModelState.IsValid.ShouldBeFalse();
//    }

//    [Fact]
//    public async Task GivenNoUserWithUserNameExists_WhenPostTwoFactorAuthentication_ThenDisplayWarning()
//    {
//        // Arrange
//        var viewModel = new TwoFactorViewModel { UserName = INEXISTANT_USER_EMAIL, TwoFactorCode = ANY_CODE };

//        // Act
//        await _authenticationController.TwoFactorAuthentication(viewModel);

//        // Assert
//        _flasher.Verify(x => x.Flash(Types.Warning, It.IsAny<string>(), true));
//    }

//    [Fact]
//    public async Task GivenUserWithUserNameWasDeleted_WhenPostTwoFactorAuthentication_ThenDisplayWarning()
//    {
//        // Arrange
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).AsDeleted().Build();
//        _userManager.Setup(x => x.Users).Returns(user.IntoList().AsQueryable);
//        var viewModel = new TwoFactorViewModel { UserName = user.Email, TwoFactorCode = ANY_CODE };

//        // Act
//        await _authenticationController.TwoFactorAuthentication(viewModel);

//        // Assert
//        _flasher.Verify(x => x.Flash(Types.Warning, It.IsAny<string>(), true));
//    }

//    [Fact]
//    public async Task GivenTwoFactorSignInSucceedsAndReturnUrlIsSet_WhenPostTwoFactorAuthentication_ThenRedirectToHome()
//    {
//        // Arrange
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).Build();
//        _userManager.Setup(x => x.Users).Returns(user.IntoList().AsQueryable);
//        var viewModel = new TwoFactorViewModel { UserName = user.Email, TwoFactorCode = ANY_CODE, ReturnUrl = ANY_RETURN_URL };
//        _signInManager
//            .Setup(x => x.TwoFactorSignInAsync("Email", viewModel.TwoFactorCode, false, false))
//            .ReturnsAsync(SignInResult.Success);

//        // Act
//        var actionResult = await _authenticationController.TwoFactorAuthentication(viewModel);

//        // Assert
//        var redirectToActionResult = actionResult.ShouldBeOfType<RedirectToActionResult>();
//        redirectToActionResult.ActionName.ShouldBe("Index");
//        redirectToActionResult.ControllerName.ShouldBe("VueApp");
//    }

//    [Fact]
//    public async Task GivenTwoFactorSignInFails_WhenPostTwoFactorAuthentication_ThenAddModelError()
//    {
//        // Arrange
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).Build();
//        _userManager.Setup(x => x.Users).Returns(user.IntoList().AsQueryable);
//        var viewModel = new TwoFactorViewModel { UserName = user.Email, TwoFactorCode = ANY_CODE, ReturnUrl = ANY_RETURN_URL };
//        _signInManager
//            .Setup(x => x.TwoFactorSignInAsync("Email", viewModel.TwoFactorCode, false, false))
//            .ReturnsAsync(SignInResult.Failed);

//        // Act
//        await _authenticationController.TwoFactorAuthentication(viewModel);

//        // Assert
//        _authenticationController.ModelState.IsValid.ShouldBeFalse();
//    }

//    [Fact]
//    public async Task GivenTwoFactorSignInFails_WhenPostTwoFactorAuthentication_ThenDisplayWarning()
//    {
//        // Arrange
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).Build();
//        _userManager.Setup(x => x.Users).Returns(user.IntoList().AsQueryable);
//        var viewModel = new TwoFactorViewModel { UserName = user.Email, TwoFactorCode = ANY_CODE, ReturnUrl = ANY_RETURN_URL };
//        _signInManager
//            .Setup(x => x.TwoFactorSignInAsync("Email", viewModel.TwoFactorCode, false, false))
//            .ReturnsAsync(SignInResult.Failed);

//        // Act
//        await _authenticationController.TwoFactorAuthentication(viewModel);

//        // Assert
//        _flasher.Verify(x => x.Flash(Types.Warning, It.IsAny<string>(), true));
//    }

//    [Fact]
//    public async Task GivenTwoFactorSignInFails_WhenPostTwoFactorAuthentication_ThenReturnViewWithSameTwoFactorViewModel()
//    {
//        // Arrange
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).Build();
//        _userManager.Setup(x => x.Users).Returns(user.IntoList().AsQueryable);
//        var expectedViewModel = new TwoFactorViewModel
//        {
//            UserName = user.Email,
//            TwoFactorCode = ANY_CODE,
//            ReturnUrl = ANY_RETURN_URL
//        };
//        _signInManager
//            .Setup(x => x.TwoFactorSignInAsync("Email", expectedViewModel.TwoFactorCode, false, false))
//            .ReturnsAsync(SignInResult.Failed);

//        // Act
//        var actionResult = await _authenticationController.TwoFactorAuthentication(expectedViewModel);

//        // Assert
//        var viewResult = actionResult.ShouldBeOfType<ViewResult>();
//        var actualViewModel = viewResult.Model.ShouldBeOfType<TwoFactorViewModel>();
//        actualViewModel.UserName.ShouldBe(expectedViewModel.UserName);
//        actualViewModel.TwoFactorCode.ShouldBe(expectedViewModel.TwoFactorCode);
//        actualViewModel.ReturnUrl.ShouldBe(expectedViewModel.ReturnUrl);
//    }

//    [Fact]
//    public async Task WhenLogout_ThenRedirectToHome()
//    {
//        // Act
//        var actionResult = await _authenticationController.Logout();

//        // Assert
//        var redirectToActionResult = actionResult.ShouldBeOfType<RedirectToActionResult>();
//        redirectToActionResult.ActionName.ShouldBe("Login");
//        redirectToActionResult.ControllerName.ShouldBe("Authentication");
//    }

//    private User GivenUserExistsAndHasMember()
//    {
//        var user = _userBuilder.WithId(Guid.NewGuid()).WithEmail(_testFixture.GenerateEmail()).Build();
//        _userManager.Setup(x => x.Users).Returns(user.IntoList().AsQueryable);
//        var member = _testFixture.MemberBuilder.Build();
//        _memberRepository.Setup(x => x.FindByUserId(user.Id, true)).Returns(member);
//        return user;
//    }

//    private Mock<IServiceProvider> BuildControllerServiceProvider()
//    {
//        var authenticationServiceMock = new Mock<IAuthenticationService>();
//        authenticationServiceMock
//            .Setup(a => a.SignInAsync(It.IsAny<HttpContext>(), It.IsAny<string>(), It.IsAny<ClaimsPrincipal>(), It.IsAny<AuthenticationProperties>()))
//            .Returns(Task.CompletedTask);

//        var serviceProviderMock = new Mock<IServiceProvider>();
//        serviceProviderMock
//            .Setup(s => s.GetService(typeof(IAuthenticationService)))
//            .Returns(authenticationServiceMock.Object);

//        var urlHelperFactory = new Mock<IUrlHelperFactory>();
//        serviceProviderMock
//            .Setup(s => s.GetService(typeof(IUrlHelperFactory)))
//            .Returns(urlHelperFactory.Object);

//        var tempDataDictionaryFactory = new Mock<ITempDataDictionaryFactory>();
//        serviceProviderMock
//            .Setup(s => s.GetService(typeof(ITempDataDictionaryFactory)))
//            .Returns(tempDataDictionaryFactory.Object);

//        return serviceProviderMock;
//    }
//}