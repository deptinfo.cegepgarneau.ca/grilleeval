﻿//using Application.Extensions;
//using Application.Settings;
//using Core.Flash;
//using Domain.Repositories;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Localization;
//using Microsoft.Extensions.Logging;
//using Microsoft.Extensions.Options;
//using Tests.Common.Builders;
//using Tests.Common.Fixtures;
//using Tests.Web.TestCollections;
//using Web.Controllers;
//using Web.ViewModels.ResetPassword;

//namespace Tests.Web.Controllers;

//[Collection(WebTestCollection.NAME)]
//public class ResetPasswordControllerTests
//{
//    private const string ANY_RETURN_URL = "/";
//    private const string VALID_PASSWORD = "Qwerty123!";
//    private const string INVALID_PASSWORD = "qwerty";

//    private readonly UserBuilder _userBuilder;

//    private readonly TestFixture _testFixture;

//    private readonly Mock<IFlasher> _flasher;
//    private readonly Mock<IUserRepository> _userRepository;

//    private readonly ResetPasswordController _resetPasswordController;

//    public ResetPasswordControllerTests(TestFixture testFixture)
//    {
//        _testFixture = testFixture;

//        _userBuilder = new UserBuilder();

//        _flasher = new Mock<IFlasher>();
//        _userRepository = new Mock<IUserRepository>();

//        var logger = new Mock<ILogger<ResetPasswordController>>();
//        var localizer = new Mock<IStringLocalizer<ResetPasswordController>>();
//        var applicationSettings = new Mock<IOptions<ApplicationSettings>>();
//        applicationSettings.Setup(x => x.Value).Returns(new ApplicationSettings { RedirectUrl = ANY_RETURN_URL });

//        _resetPasswordController = new ResetPasswordController(_flasher.Object, _userRepository.Object, logger.Object,
//            applicationSettings.Object, localizer.Object)
//        {
//            ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() }
//        };
//    }

//    [Fact]
//    public void WhenGetIndex_ThenReturnViewResultWithResetPasswordViewMode()
//    {
//        // Act
//        var actionResult = _resetPasswordController.Index(default!, default!);

//        // Assert
//        var viewResult = actionResult.ShouldBeOfType<ViewResult>();
//        viewResult.Model.ShouldBeOfType<ResetPasswordViewModel>().ShouldNotBeNull();
//    }

//    [Fact]
//    public async Task GivenNullPassword_WhenPostIndex_ThenModelStateShouldNotBeValid()
//    {
//        // Arrange
//        var viewModel = new ResetPasswordViewModel();
//        _resetPasswordController.ModelState.AddModelError(nameof(viewModel.Password), "");

//        // Act
//        await _resetPasswordController.Index(string.Empty, string.Empty, viewModel);

//        // Assert
//        _resetPasswordController.ModelState.IsValid.ShouldBeFalse();
//    }

//    [Fact]
//    public async Task GivenNoUserWithIdExists_WhenPostIndex_ThenReturnBadRequest()
//    {
//        // Arrange
//        var inexistantUserId = Guid.NewGuid().ToString();
//        var viewModel = new ResetPasswordViewModel { Password = VALID_PASSWORD };

//        // Act
//        var result = await _resetPasswordController.Index(inexistantUserId, string.Empty, viewModel);

//        // Assert
//        result.ShouldBeOfType<BadRequestResult>();
//    }

//    [Fact]
//    public async Task GivenInvalidPassword_WhenPostIndex_ThenDisplayWarning()
//    {
//        // Arrange
//        var encodedToken = string.Empty.Base64UrlEncode();
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).AsDeleted().Build();
//        _userRepository.Setup(x => x.FindById(user.Id)).Returns(user);
//        _userRepository
//            .Setup(x => x.UpdateUserPassword(user, INVALID_PASSWORD, encodedToken.Base64UrlDecode()))
//            .ReturnsAsync(IdentityResult.Failed(new IdentityError()));
//        var viewModel = new ResetPasswordViewModel { Password = INVALID_PASSWORD };

//        // Act
//        await _resetPasswordController.Index(user.Id.ToString(), encodedToken, viewModel);

//        // Assert
//        _flasher.Verify(x => x.Flash(Types.Warning, It.IsAny<string>(), true));
//    }

//    [Fact]
//    public async Task WhenPostIndex_ThenViewResultViewModelShouldHaveRedirectUrl()
//    {
//        // Arrange
//        var encodedToken = string.Empty.Base64UrlEncode();
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).AsDeleted().Build();
//        _userRepository.Setup(x => x.FindById(user.Id)).Returns(user);
//        _userRepository
//            .Setup(x => x.UpdateUserPassword(user, VALID_PASSWORD, encodedToken.Base64UrlDecode()))
//            .ReturnsAsync(IdentityResult.Success);
//        var userId = user.Id.ToString();
//        var viewModel = new ResetPasswordViewModel { Password = VALID_PASSWORD };

//        // Act
//        var actionResult = await _resetPasswordController.Index(userId, encodedToken, viewModel);

//        // Assert
//        var viewResult = actionResult.ShouldBeOfType<ViewResult>();
//        viewResult.Model.ShouldBeOfType<ResetPasswordViewModel>().RedirectUrl.ShouldBe(ANY_RETURN_URL);
//    }
//}