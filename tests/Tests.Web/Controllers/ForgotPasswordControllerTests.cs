﻿//using Application.Interfaces.Services.Notifications;
//using Application.Services.Notifications.Dtos;
//using Application.Settings;
//using Core.Flash;
//using Domain.Entities.Identity;
//using Domain.Repositories;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Mvc.Routing;
//using Microsoft.AspNetCore.Mvc.ViewFeatures;
//using Microsoft.Extensions.Localization;
//using Microsoft.Extensions.Logging;
//using Microsoft.Extensions.Options;
//using Tests.Common.Builders;
//using Tests.Common.Fixtures;
//using Tests.Web.TestCollections;
//using Web.Controllers;
//using Web.ViewModels.ForgotPassword;

//namespace Tests.Web.Controllers;

//[Collection(WebTestCollection.NAME)]
//public class ForgotPasswordControllerTests
//{
//    private const string ANY_BASE_URL = "https://localhost:7101";
//    private const string INEXISTANT_USER_EMAIL = "john.doe12@gmail.com";

//    private readonly UserBuilder _userBuilder;

//    private readonly TestFixture _testFixture;

//    private readonly Mock<IFlasher> _flasher;
//    private readonly Mock<IUserRepository> _userRepository;
//    private readonly Mock<INotificationService> _notificationService;

//    private readonly ForgotPasswordController _forgotPasswordController;

//    public ForgotPasswordControllerTests(TestFixture testFixture)
//    {
//        _testFixture = testFixture;

//        _userBuilder = new UserBuilder();

//        _flasher = new Mock<IFlasher>();
//        _userRepository = new Mock<IUserRepository>();
//        _notificationService = new Mock<INotificationService>();
//        var localizer = new Mock<IStringLocalizer<ForgotPasswordController>>();

//        var applicationSettings = new Mock<IOptions<ApplicationSettings>>();
//        applicationSettings.Setup(x => x.Value).Returns(new ApplicationSettings { BaseUrl = ANY_BASE_URL });
//        var logger = new Mock<ILogger<ForgotPasswordController>>();

//        _forgotPasswordController = new ForgotPasswordController(
//            logger.Object,
//            _userRepository.Object,
//            _flasher.Object,
//            _notificationService.Object,
//            applicationSettings.Object,
//            localizer.Object)
//        {
//            ControllerContext = new ControllerContext
//            {
//                HttpContext = new DefaultHttpContext { RequestServices = BuildControllerServiceProvider().Object }
//            }
//        };
//    }

//    [Fact]
//    public void WhenGetIndex_ThenReturnViewResult()
//    {
//        // Act
//        var actionResult = _forgotPasswordController.Index();

//        // Assert
//        actionResult.ShouldBeOfType<ViewResult>();
//    }

//    [Fact]
//    public async Task GivenNullUserName_WhenPostIndex_ThenModelStateShouldNotBeValid()
//    {
//        // Arrange
//        var viewModel = new ForgotPasswordViewModel();
//        _forgotPasswordController.ModelState.AddModelError(nameof(viewModel.UserName), "");

//        // Act
//        await _forgotPasswordController.Index(viewModel);

//        // Assert
//        _forgotPasswordController.ModelState.IsValid.ShouldBeFalse();
//    }

//    [Fact]
//    public async Task GivenNoUserWithGivenUserNameExists_WhenPostIndex_ThenDisplaySuccessToShowAsLittleInfoToUserAsPossibleToPreventAttacks()
//    {
//        // Arrange
//        var viewModel = new ForgotPasswordViewModel { UserName = INEXISTANT_USER_EMAIL };

//        // Act
//        await _forgotPasswordController.Index(viewModel);

//        // Assert
//        _flasher.Verify(x => x.Flash(Types.Success, It.IsAny<string>(), true));
//    }

//    [Fact]
//    public async Task GivenNoUserWithGivenUserNameExists_WhenPostIndex_ThenReturnView()
//    {
//        // Arrange
//        var viewModel = new ForgotPasswordViewModel { UserName = INEXISTANT_USER_EMAIL };

//        // Act
//        var actionResult = await _forgotPasswordController.Index(viewModel);

//        // Assert
//        actionResult.ShouldBeOfType<ViewResult>();
//    }

//    [Fact]
//    public async Task GivenUserWithGivenUserNameExists_WhenPostIndex_ThenSendForgotPasswordNotification()
//    {
//        // Arrange
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).AsDeleted().Build();
//        _userRepository.Setup(x => x.FindByUserName(user.UserName)).Returns(user);
//        _userRepository.Setup(x => x.GetResetPasswordTokenForUser(user)).ReturnsAsync(string.Empty);
//        var viewModel = new ForgotPasswordViewModel { UserName = user.Email };
//        _notificationService
//            .Setup(x => x.SendForgotPasswordNotification(It.IsAny<User>(), It.IsAny<string>()))
//            .ReturnsAsync(new SendNotificationResponseDto(true));

//        // Act
//        await _forgotPasswordController.Index(viewModel);

//        // Assert
//        _notificationService.Verify(x => x.SendForgotPasswordNotification(It.IsAny<User>(), It.IsAny<string>()));
//    }

//    [Fact]
//    public async Task GivenSendingNotificationWasSuccessful_WhenPostIndex_ThenDisplaySuccessMessage()
//    {
//        // Arrange
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).AsDeleted().Build();
//        _userRepository.Setup(x => x.FindByUserName(user.UserName)).Returns(user);
//        _userRepository.Setup(x => x.GetResetPasswordTokenForUser(user)).ReturnsAsync(string.Empty);
//        var viewModel = new ForgotPasswordViewModel { UserName = user.Email };
//        _notificationService
//            .Setup(x => x.SendForgotPasswordNotification(It.IsAny<User>(), It.IsAny<string>()))
//            .ReturnsAsync(new SendNotificationResponseDto(true));

//        // Act
//        await _forgotPasswordController.Index(viewModel);

//        // Assert
//        _flasher.Verify(x => x.Flash(Types.Success, It.IsAny<string>(), true));
//    }

//    [Fact]
//    public async Task GivenSendingNotificationWasSuccessful_WhenPostIndex_ThenDisplayWarning()
//    {
//        // Arrange
//        var user = _userBuilder.WithEmail(_testFixture.GenerateEmail()).AsDeleted().Build();
//        _userRepository.Setup(x => x.FindByUserName(user.UserName)).Returns(user);
//        _userRepository.Setup(x => x.GetResetPasswordTokenForUser(user)).ReturnsAsync(string.Empty);
//        var viewModel = new ForgotPasswordViewModel { UserName = user.Email };
//        _notificationService
//            .Setup(x => x.SendForgotPasswordNotification(It.IsAny<User>(), It.IsAny<string>()))
//            .ReturnsAsync(new SendNotificationResponseDto(false));

//        // Act
//        await _forgotPasswordController.Index(viewModel);

//        // Assert
//        _flasher.Verify(x => x.Flash(Types.Warning, It.IsAny<string>(), true));
//    }

//    private Mock<IServiceProvider> BuildControllerServiceProvider()
//    {
//        var serviceProviderMock = new Mock<IServiceProvider>();
//        var urlHelperFactory = new Mock<IUrlHelperFactory>();
//        serviceProviderMock
//            .Setup(s => s.GetService(typeof(IUrlHelperFactory)))
//            .Returns(urlHelperFactory.Object);

//        var urlHelper = new Mock<IUrlHelper>();
//        urlHelperFactory
//            .Setup(x => x.GetUrlHelper(It.IsAny<ActionContext>()))
//            .Returns(urlHelper.Object);

//        var tempDataDictionaryFactory = new Mock<ITempDataDictionaryFactory>();
//        serviceProviderMock
//            .Setup(s => s.GetService(typeof(ITempDataDictionaryFactory)))
//            .Returns(tempDataDictionaryFactory.Object);

//        return serviceProviderMock;
//    }
//}