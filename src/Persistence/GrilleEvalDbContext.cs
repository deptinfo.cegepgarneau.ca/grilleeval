using System.Reflection;
using Application.Interfaces;
using Domain.Common;
using Domain.Entities;
using Domain.Entities.Baremes;
using Domain.Entities.Books;
using Domain.Entities.CommentaireGlobaux;
using Domain.Entities.CommentairesEtudiant;
using Domain.Entities.CoursEtudiants;
using Domain.Entities.Criteres;
using Domain.Entities.DossierCours;
using Domain.Entities.Elements;
using Domain.Entities.ElementsEtudiant;
using Domain.Entities.Etudiants;
using Domain.Entities.Grilles;
using Domain.Entities.Identity;
using Domain.Entities.Utilisateur;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Persistence.Extensions;
using Persistence.Interceptors;

namespace Persistence;

public class GrilleEvalDbContext : IdentityDbContext<User, Role, Guid,
    IdentityUserClaim<Guid>, UserRole,
    IdentityUserLogin<Guid>, IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>, IGrilleEvalDbContext
{
    private readonly AuditableAndSoftDeletableEntitySaveChangesInterceptor
        _auditableAndSoftDeletableEntitySaveChangesInterceptor = default!;

    private readonly AuditableEntitySaveChangesInterceptor _auditableEntitySaveChangesInterceptor = default!;
    private readonly UserSaveChangesInterceptor _userSaveChangesInterceptor = default!;
    private readonly EntitySaveChangesInterceptor _entitySaveChangesInterceptor = default!;

    public GrilleEvalDbContext(
        DbContextOptions<GrilleEvalDbContext> options,
        AuditableAndSoftDeletableEntitySaveChangesInterceptor auditableAndSoftDeletableEntitySaveChangesInterceptor,
        AuditableEntitySaveChangesInterceptor auditableEntitySaveChangesInterceptor,
        UserSaveChangesInterceptor userSaveChangesInterceptor,
        EntitySaveChangesInterceptor entitySaveChangesInterceptor)
        : base(options)
    {
        _auditableAndSoftDeletableEntitySaveChangesInterceptor = auditableAndSoftDeletableEntitySaveChangesInterceptor;
        _auditableEntitySaveChangesInterceptor = auditableEntitySaveChangesInterceptor;
        _userSaveChangesInterceptor = userSaveChangesInterceptor;
        _entitySaveChangesInterceptor = entitySaveChangesInterceptor;
    }

    public DbSet<Member> Members { get; set; } = default!;
    public DbSet<Book> Books { get; set; } = default!;
    public DbSet<Etudiant> Etudiants { get; set; }
    public DbSet<Cours> Cours { get; set; }
    public DbSet<EtudiantCours> EtudiantsCours { get; set; }
    public DbSet<Grille> Grilles { get; set; }
    public DbSet<Element> Elements { get; set; }
    public DbSet<Critere> Criteres { get; set; }
    public DbSet<Bareme> Baremes { get; set; }
    public DbSet<CommentaireGlobal> CommentairesGlobaux { get; set; }
    public DbSet<ElementEtudiant> ElementsEtudiant { get; set; }
    public DbSet<CommentaireEtudiant> CommentairesEtudiant { get; set; }

    public GrilleEvalDbContext()
    {
    }

    public GrilleEvalDbContext(DbContextOptions<GrilleEvalDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        // Global query to prevent loading soft-deleted entities
        foreach (var entityType in builder.Model.GetEntityTypes())
        {
            if (!typeof(ISoftDeletable).IsAssignableFrom(entityType.ClrType))
                continue;

            if (entityType.ClrType == typeof(User))
                continue;

            entityType.AddSoftDeleteQueryFilter();
        }

        builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.AddInterceptors(
            _auditableAndSoftDeletableEntitySaveChangesInterceptor,
            _auditableEntitySaveChangesInterceptor,
            _userSaveChangesInterceptor,
            _entitySaveChangesInterceptor);
    }

    public async Task<int> SaveChangesAsync(CancellationToken? cancellationToken = null)
    {
        return await base.SaveChangesAsync(cancellationToken ?? CancellationToken.None);
    }
}