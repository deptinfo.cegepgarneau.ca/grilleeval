using Domain.Constants.User;
using Domain.Entities;
using Domain.Entities.Baremes;
using Domain.Entities.CommentaireGlobaux;
using Domain.Entities.CommentairesEtudiant;
using Domain.Entities.CoursEtudiants;
using Domain.Entities.Criteres;
using Domain.Entities.DossierCours;
using Domain.Entities.Elements;
using Domain.Entities.ElementsEtudiant;
using Domain.Entities.Etudiants;
using Domain.Entities.Grilles;
using Domain.Entities.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;


namespace Persistence;

public class GrilleEvalDbContextInitializer
{

    const string DEFAULT_EMAIL = "admin@gmail.com";

    private readonly ILogger<GrilleEvalDbContextInitializer> _logger;
    private readonly GrilleEvalDbContext _context;
    private readonly RoleManager<Role> _roleManager;
    private readonly UserManager<User> _userManager;

    #region Utilisateur
    private User user1 = new User { Email = "user1@gmail.com", PasswordHash = "$argon2id$v=19$m=32768,t=4,p=1$zErS68C2v1IASX4zlhyLhw$/zDhvyjPK+9gGs8ckSRRjXYjEe3z66CxmwLSQkVt6Vs" };
    private User user2 = new User { Email = "user2@gmail.com", PasswordHash = "$argon2id$v=19$m=32768,t=4,p=1$zErS68C2v1IASX4zlhyLhw$/zDhvyjPK+9gGs8ckSRRjXYjEe3z66CxmwLSQkVt6Vs" };

    #endregion

    #region Seeds Barèmes
    private List<Bareme> baremes1 = new List<Bareme>(){
		new Bareme { Description = "Très bien, vous avez obtenu la note maximale pour cet élément.", MaxValue = 10, MinValue = 9, Titre = "Excellent" },
		new Bareme { Description = "Bien, vous avez obtenu une note supérieure à la moyenne pour cet élément.", MaxValue = 8, MinValue = 7, Titre = "Très bien" },
        new Bareme { Description = "Vous avez obtenu une note moyenne pour cet élément.", MaxValue = 6, MinValue = 5, Titre = "Bien" },
        new Bareme { Description = "Échec, vous n'avez pas atteint la moyenne pour cet élément.", MaxValue = 4, MinValue = 0, Titre = "Échec" }
    };
    private List<Bareme> baremes2 = new List<Bareme>(){
		new Bareme { Description = "Très bien, vous avez obtenu la note maximale pour cet élément.", MaxValue = 10, MinValue = 9, Titre = "Excellent" },
		new Bareme { Description = "Bien, vous avez obtenu une note supérieure à la moyenne pour cet élément.", MaxValue = 8, MinValue = 7, Titre = "Très bien" },
        new Bareme { Description = "Vous avez obtenu une note moyenne pour cet élément.", MaxValue = 6, MinValue = 5, Titre = "Bien" },
        new Bareme { Description = "Échec, vous n'avez pas atteint la moyenne pour cet élément.", MaxValue = 4, MinValue = 0, Titre = "Échec" }
    };
    private List<Bareme> baremes3 = new List<Bareme>(){
		new Bareme { Description = "Très bien, vous avez obtenu la note maximale pour cet élément.", MaxValue = 10, MinValue = 9, Titre = "Excellent" },
		new Bareme { Description = "Bien, vous avez obtenu une note supérieure à la moyenne pour cet élément.", MaxValue = 8, MinValue = 7, Titre = "Très bien" },
        new Bareme { Description = "Vous avez obtenu une note moyenne pour cet élément.", MaxValue = 6, MinValue = 5, Titre = "Bien" },
        new Bareme { Description = "Échec, vous n'avez pas atteint la moyenne pour cet élément.", MaxValue = 4, MinValue = 0, Titre = "Échec" }
        };
    private List<Bareme> baremes4 = new List<Bareme>(){
		new Bareme { Description = "Très bien, vous avez obtenu la note maximale pour cet élément.", MaxValue = 10, MinValue = 9, Titre = "Excellent" },
		new Bareme { Description = "Bien, vous avez obtenu une note supérieure à la moyenne pour cet élément.", MaxValue = 8, MinValue = 7, Titre = "Très bien" },
        new Bareme { Description = "Vous avez obtenu une note moyenne pour cet élément.", MaxValue = 6, MinValue = 5, Titre = "Bien" },
        new Bareme { Description = "Échec, vous n'avez pas atteint la moyenne pour cet élément.", MaxValue = 4, MinValue = 0, Titre = "Échec" }
        };
    private List<Bareme> baremes5 = new List<Bareme>(){
		new Bareme { Description = "Très bien, vous avez obtenu la note maximale pour cet élément.", MaxValue = 10, MinValue = 9, Titre = "Excellent" },
		new Bareme { Description = "Bien, vous avez obtenu une note supérieure à la moyenne pour cet élément.", MaxValue = 8, MinValue = 7, Titre = "Très bien" },
        new Bareme { Description = "Vous avez obtenu une note moyenne pour cet élément.", MaxValue = 6, MinValue = 5, Titre = "Bien" },
        new Bareme { Description = "Échec, vous n'avez pas atteint la moyenne pour cet élément.", MaxValue = 4, MinValue = 0, Titre = "Échec" }
        };
    private List<Bareme> baremes6 = new List<Bareme>(){
		new Bareme { Description = "Très bien, vous avez obtenu la note maximale pour cet élément.", MaxValue = 10, MinValue = 9, Titre = "Excellent" },
		new Bareme { Description = "Bien, vous avez obtenu une note supérieure à la moyenne pour cet élément.", MaxValue = 8, MinValue = 7, Titre = "Très bien" },
        new Bareme { Description = "Vous avez obtenu une note moyenne pour cet élément.", MaxValue = 6, MinValue = 5, Titre = "Bien" },
        new Bareme { Description = "Échec, vous n'avez pas atteint la moyenne pour cet élément.", MaxValue = 4, MinValue = 0, Titre = "Échec" }
        };
    private List<Bareme> baremes7 = new List<Bareme>(){
		new Bareme { Description = "Très bien, vous avez obtenu la note maximale pour cet élément.", MaxValue = 10, MinValue = 9, Titre = "Excellent" },
		new Bareme { Description = "Bien, vous avez obtenu une note supérieure à la moyenne pour cet élément.", MaxValue = 8, MinValue = 7, Titre = "Très bien" },
        new Bareme { Description = "Vous avez obtenu une note moyenne pour cet élément.", MaxValue = 6, MinValue = 5, Titre = "Bien" },
        new Bareme { Description = "Échec, vous n'avez pas atteint la moyenne pour cet élément.", MaxValue = 4, MinValue = 0, Titre = "Échec" }
        };
    private List<Bareme> baremes8 = new List<Bareme>(){
		new Bareme { Description = "Très bien, vous avez obtenu la note maximale pour cet élément.", MaxValue = 10, MinValue = 9, Titre = "Excellent" },
		new Bareme { Description = "Bien, vous avez obtenu une note supérieure à la moyenne pour cet élément.", MaxValue = 8, MinValue = 7, Titre = "Très bien" },
        new Bareme { Description = "Vous avez obtenu une note moyenne pour cet élément.", MaxValue = 6, MinValue = 5, Titre = "Bien" },
        new Bareme { Description = "Échec, vous n'avez pas atteint la moyenne pour cet élément.", MaxValue = 4, MinValue = 0, Titre = "Échec" }
        };
    private List<Bareme> baremes9 = new List<Bareme>(){
		new Bareme { Description = "Très bien, vous avez obtenu la note maximale pour cet élément.", MaxValue = 10, MinValue = 9, Titre = "Excellent" },
		new Bareme { Description = "Bien, vous avez obtenu une note supérieure à la moyenne pour cet élément.", MaxValue = 8, MinValue = 7, Titre = "Très bien" },
        new Bareme { Description = "Vous avez obtenu une note moyenne pour cet élément.", MaxValue = 6, MinValue = 5, Titre = "Bien" },
        new Bareme { Description = "Échec, vous n'avez pas atteint la moyenne pour cet élément.", MaxValue = 4, MinValue = 0, Titre = "Échec" }
        };
    private List<Bareme> baremes10 = new List<Bareme>(){
		new Bareme { Description = "Très bien, vous avez obtenu la note maximale pour cet élément.", MaxValue = 10, MinValue = 9, Titre = "Excellent" },
		new Bareme { Description = "Bien, vous avez obtenu une note supérieure à la moyenne pour cet élément.", MaxValue = 8, MinValue = 7, Titre = "Très bien" },
        new Bareme { Description = "Vous avez obtenu une note moyenne pour cet élément.", MaxValue = 6, MinValue = 5, Titre = "Bien" },
        new Bareme { Description = "Échec, vous n'avez pas atteint la moyenne pour cet élément.", MaxValue = 4, MinValue = 0, Titre = "Échec" }
        };
    private List<Bareme> baremes11 = new List<Bareme>(){
		new Bareme { Description = "Très bien, vous avez obtenu la note maximale pour cet élément.", MaxValue = 10, MinValue = 9, Titre = "Excellent" },
		new Bareme { Description = "Bien, vous avez obtenu une note supérieure à la moyenne pour cet élément.", MaxValue = 8, MinValue = 7, Titre = "Très bien" },
        new Bareme { Description = "Vous avez obtenu une note moyenne pour cet élément.", MaxValue = 6, MinValue = 5, Titre = "Bien" },
        new Bareme { Description = "Échec, vous n'avez pas atteint la moyenne pour cet élément.", MaxValue = 4, MinValue = 0, Titre = "Échec" }
        };

    #endregion

    #region Seeds Critères
    private List<Critere> seedCriteres1 = new List<Critere>() { new Critere { Detail = "Respect des consignes" }, new Critere { Detail = "Présentation claire" }, };
    private List<Critere> seedCriteres2 = new List<Critere>() { new Critere { Detail = "Maîtrise de l’environnement intérieur à rénover." }, new Critere { Detail = "Organisation de l’information claire." }, };
    private List<Critere> seedCriteres3 = new List<Critere>() { new Critere { Detail = "Compétences techniques" }, new Critere { Detail = "Créativité" } };
    private List<Critere> seedCriteres4 = new List<Critere>() { new Critere { Detail = "Communication" }, new Critere { Detail = "Collaboration" }, };
    private List<Critere> seedCriteres5 = new List<Critere>() { new Critere { Detail = "Analyse des besoins" }, new Critere { Detail = "Conception de solutions" }, };
    private List<Critere> seedCriteres6 = new List<Critere>() { new Critere { Detail = "Qualité du code" }, new Critere { Detail = "Tests unitaires" }, };
    private List<Critere> seedCriteres7 = new List<Critere>() { new Critere { Detail = "Performance" }, new Critere { Detail = "Sécurité" }, };
    private List<Critere> seedCriteres8 = new List<Critere>() { new Critere { Detail = "Documentation" }, new Critere { Detail = "Maintenance" }, };
    private List<Critere> seedCriteres9 = new List<Critere>() { new Critere { Detail = "Interface utilisateur" }, new Critere { Detail = "Expérience utilisateur" }, };
    private List<Critere> seedCriteres10 = new List<Critere>() { new Critere { Detail = "Performance" }, new Critere { Detail = "Fiabilité" }, };
    private List<Critere> seedCriteres11 = new List<Critere>() { new Critere { Detail = "Sécurité" }, new Critere { Detail = "Évolutivité" }, };
    #endregion

    #region Seeds Cours
    private Cours cours1 = new Cours { Code = "420-15D-FX", NomCours = "APPLICATIONS WEB ET SÉCURITÉ" };
    private Cours cours2 = new Cours { Code = "350-0BD-FX", NomCours = "INTERAGIR DANS LE MILIEU DU TRAVAIL" };
    private Cours cours3 = new Cours { Code = "570-130-FX", NomCours = "Pratique du design d’intérieur" };
    #endregion

    #region Seeds Etudiant
    private Etudiant etudiant1 = new Etudiant { Da = "2201504", Prenom = "Ziad", Nom = "Affan" };
    private Etudiant etudiant2 = new Etudiant { Da = "2201505", Prenom = "Alexandre", Nom = "Vachon" };
    private Etudiant etudiant3 = new Etudiant { Da = "2201506", Prenom = "Cynthia", Nom = "Fortier" };
    private Etudiant etudiant4 = new Etudiant { Da = "2201507", Prenom = "Ouiza", Nom = "Meziani" };
    private Etudiant etudiant5 = new Etudiant { Da = "2201508", Prenom = "Moussa", Nom = "Diallo" };
    private Etudiant etudiant6 = new Etudiant { Da = "2201509", Prenom = "Khaled", Nom = "Affan" };
    private Etudiant etudiant7 = new Etudiant { Da = "2201510", Prenom = "Étienne", Nom = "Affan" };
    private Etudiant etudiant8 = new Etudiant { Da = "2201511", Prenom = "Nicki", Nom = "Diallo" };
    private Etudiant etudiant9 = new Etudiant { Da = "2201512", Prenom = "Yehya", Nom = "Affan" };
    private Etudiant etudiant10 = new Etudiant { Da = "2201513", Prenom = "Yassine", Nom = "Affan" };
    private Etudiant etudiant11 = new Etudiant { Da = "2201514", Prenom = "Willy", Nom = "Roussel" };
    private Etudiant etudiant12 = new Etudiant { Da = "2201515", Prenom = "Pierre", Nom = "Junior" };
    #endregion

    private List<CommentaireGlobal> seedCommentaireGlobaux = new List<CommentaireGlobal>();
    private List<CommentaireGlobal> seedCommentaireGlobaux1 = new List<CommentaireGlobal>();


    public GrilleEvalDbContextInitializer(ILogger<GrilleEvalDbContextInitializer> logger,
        GrilleEvalDbContext context,
        RoleManager<Role> roleManager,
        UserManager<User> userManager)
    {
        _logger = logger;
        _context = context;
        _roleManager = roleManager;
        _userManager = userManager;
    }

    public async Task InitialiseAsync()
    {
        try
        {
            await _context.Database.MigrateAsync();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while initialising the database.");
            throw;
        }
    }

    private async Task SeedCommentaireGlobaux()
    {
        _logger.LogInformation("Seeding Commentaire Globaux");
        List<CommentaireGlobal> seedCommentaireGlobaux = new List<CommentaireGlobal>()
        {
            new CommentaireGlobal()
            {
                Commentaire="Retard de remise"
            },
            new CommentaireGlobal()
            {
                Commentaire="Excellent travail"
            },
        };

        List<CommentaireGlobal> seedCommentaireGlobaux1 = new List<CommentaireGlobal>()
        {
            new CommentaireGlobal()
            {
                Commentaire="Débit de présentation trop rapide."
            },
            new CommentaireGlobal()
            {
                Commentaire="Type de client non présenté."
            },
            new CommentaireGlobal()
            {
                Commentaire="Durée de présentation respectée."
            },
        };

        if (_context.CommentairesGlobaux.ToList().Contains(seedCommentaireGlobaux[0]))
        {
			_context.Elements.ToList()[0].Commentaire.AddRange(seedCommentaireGlobaux);
			_context.Elements.ToList()[1].Commentaire.AddRange(seedCommentaireGlobaux1);
		}
		
		//var existingCommentaireGlobal = _context.CommentairesGlobaux.IgnoreQueryFilters().FirstOrDefault(x => x.Commentaire == seedCommentaireGlobaux[0].Commentaire);
  //      if (existingCommentaireGlobal != null)
  //          return;
  //      _context.CommentairesGlobaux.AddRange(seedCommentaireGlobaux);
  //      _context.CommentairesGlobaux.AddRange(seedCommentaireGlobaux1);

        await _context.SaveChangesAsync();
    }
    private async Task SeedBareme()
    {
        _logger.LogInformation("Seeding Bareme");

        //List<List<Bareme>> allBaremsSeed = new List<List<Bareme>>()
        //{
        //    baremes1,
        //    baremes2,
        //    baremes3,
        //    baremes4,
        //    baremes5,
        //    baremes6,
        //    baremes7,
        //    baremes8,
        //    baremes9,
        //    baremes10,
        //    baremes11
        //};
        if (_context.Baremes.ToList().Contains(baremes1[0]))
        {
			_context.Elements.ToList()[0].Bareme.AddRange(baremes1);
			_context.Elements.ToList()[1].Bareme.AddRange(baremes2);
			_context.Elements.ToList()[2].Bareme.AddRange(baremes3);
			_context.Elements.ToList()[3].Bareme.AddRange(baremes4);
			_context.Elements.ToList()[4].Bareme.AddRange(baremes5);
			_context.Elements.ToList()[5].Bareme.AddRange(baremes6);
			_context.Elements.ToList()[6].Bareme.AddRange(baremes7);
			_context.Elements.ToList()[7].Bareme.AddRange(baremes8);
			_context.Elements.ToList()[8].Bareme.AddRange(baremes9);
			_context.Elements.ToList()[9].Bareme.AddRange(baremes10);
			_context.Elements.ToList()[10].Bareme.AddRange(baremes11);
		}

		//var baremesToAdd = allBaremsSeed.SelectMany(baremeSeed => baremeSeed)
  //                                  .Where(bareme => !_context.Baremes.Any(b => b.Titre == bareme.Titre))
  //                                  .ToList();

  //      _context.Baremes.AddRange(baremesToAdd);
        await _context.SaveChangesAsync();
    }
    private async Task SeedCritere()
    {
        _logger.LogInformation("Seeding Critere");

        //var allSeedCriteres = new List<List<Critere>>
        //{
        //    seedCriteres1,
        //    seedCriteres2,
        //    seedCriteres3,
        //    seedCriteres4,
        //    seedCriteres5,
        //    seedCriteres6,
        //    seedCriteres7,
        //    seedCriteres8,
        //    seedCriteres9,
        //    seedCriteres10,
        //    seedCriteres11
        //};


        if (_context.Criteres.ToList().Contains(seedCriteres1[0]))
        {
			_context.Elements.ToList()[0].Critere.AddRange(seedCriteres1);
			_context.Elements.ToList()[1].Critere.AddRange(seedCriteres2);
			_context.Elements.ToList()[2].Critere.AddRange(seedCriteres3);
			_context.Elements.ToList()[3].Critere.AddRange(seedCriteres4);
			_context.Elements.ToList()[4].Critere.AddRange(seedCriteres5);
			_context.Elements.ToList()[5].Critere.AddRange(seedCriteres6);
			_context.Elements.ToList()[6].Critere.AddRange(seedCriteres7);
			_context.Elements.ToList()[7].Critere.AddRange(seedCriteres8);
			_context.Elements.ToList()[8].Critere.AddRange(seedCriteres9);
			_context.Elements.ToList()[9].Critere.AddRange(seedCriteres10);
			_context.Elements.ToList()[10].Critere.AddRange(seedCriteres11);
		}
       
		//var criteresToAdd = allSeedCriteres.SelectMany(seedCriteres => seedCriteres)
  //                                     .Where(critere => !_context.Criteres.Any(c => c.Detail == critere.Detail))
                                       //.ToList();

        //_context.Criteres.AddRange(criteresToAdd);
        await _context.SaveChangesAsync();
    }
    private async Task SeedElement()
    {
        _logger.LogInformation("Seeding Element");

        List<Element> seedElement = new List<Element>()
        {
            new Element()
            {
               //Critere=seedCriteres1,
               Ponderation=5,
               //Bareme=baremes1,
               //Commentaire=_context.CommentairesGlobaux.ToList(),
               GrilleId = _context.Grilles.ToList()[0].Id,
               Titre="Clarté",
            },
            new Element()
            {
               //Critere=seedCriteres2,
               Ponderation=15,
               //Bareme=baremes2,
               //Commentaire=_context.CommentairesGlobaux.ToList(),
               GrilleId = _context.Grilles.ToList()[0].Id,
			   Titre="Respect des normes",
            },
            new Element()
            {
               //Critere=seedCriteres3,
               Ponderation=15,
               //Bareme=baremes3,
               //Commentaire=_context.CommentairesGlobaux.ToList(),
               GrilleId = _context.Grilles.ToList()[1].Id,
			   Titre="Clarté du code",
            },
            new Element()
            {
               //Critere=seedCriteres4,
               Ponderation=20,
               //Bareme=baremes4,
               //Commentaire=_context.CommentairesGlobaux.ToList(),
               GrilleId = _context.Grilles.ToList()[1].Id,
			   Titre="Performance du code",
            },
            new Element()
            {
               //Critere=seedCriteres5,
               Ponderation=5,
               //Bareme=baremes5,
               //Commentaire=_context.CommentairesGlobaux.ToList(),
               GrilleId = _context.Grilles.ToList()[2].Id,
			   Titre="Remise à temps",
            },
            new Element()
            {
               //Critere=seedCriteres6,
               Ponderation=15,
               //Bareme=baremes6,
               //Commentaire=_context.CommentairesGlobaux.ToList(),
               GrilleId = _context.Grilles.ToList()[2].Id,
			   Titre="Qualité du français",
            },
            new Element()
            {
               //Critere=seedCriteres7,
               Ponderation=15,
               //Bareme=baremes7,
               //Commentaire=_context.CommentairesGlobaux.ToList(),
               GrilleId = _context.Grilles.ToList()[3].Id,
			   Titre="Push régulier",
            },
            new Element()
            {
               //Critere=seedCriteres8,
               Ponderation=15,
               //Bareme=baremes8,
               //Commentaire=_context.CommentairesGlobaux.ToList(),
               GrilleId = _context.Grilles.ToList()[3].Id,
			   Titre="Sécurité du code",
            },
            new Element()
            {
               //Critere=seedCriteres9,
               Ponderation=15,
               //Bareme=baremes9,
               //Commentaire=_context.CommentairesGlobaux.ToList(),
               GrilleId = _context.Grilles.ToList()[4].Id,
			   Titre="Découpage en méthodes",
            },
            new Element()
            {
               //Critere=seedCriteres10,
               Ponderation=15,
               //Bareme= baremes10,
               //Commentaire= seedCommentaireGlobaux1,
               GrilleId = _context.Grilles.ToList()[4].Id,
			   Titre="Présentation du client",
            },
            new Element()
            {
               //Critere= seedCriteres11,
               Ponderation=15,
               //Bareme=baremes11,
               //Commentaire=_context.CommentairesGlobaux.ToList(),
               GrilleId = _context.Grilles.ToList()[4].Id,
			   Titre="Analyse générale",
            }
        };


        var existingElement = _context.Elements.FirstOrDefault(x => x.Titre == seedElement[0].Titre);
        if (existingElement != null)
            return;
        _context.Elements.AddRange(seedElement);
        await _context.SaveChangesAsync();
    }
    private List<ElementEtudiant> ObtenirListeElementEtudiant(List<Etudiant> listeEtudiants, List<Element> listeElements)
    {
        return _context.ElementsEtudiant.Where(x => listeEtudiants.Contains(x.Etudiant) && listeElements.Contains(x.ElementEvalue)).ToList();
    }
    private async Task SeedGrille()
    {
        _logger.LogInformation("Seeding Grilles");
        var cours1WithEtudiants = await _context.Cours
                .Include(c => c.EtudiantsCours)
                .ThenInclude(ec => ec.Etudiant)
                .FirstOrDefaultAsync(c => c.Code == cours1.Code);

        var cours2WithEtudiants = await _context.Cours
                .Include(c => c.EtudiantsCours)
                .ThenInclude(ec => ec.Etudiant)
                .FirstOrDefaultAsync(c => c.Code == cours2.Code);
        var cours3WithEtudiants = await _context.Cours
                .Include(c => c.EtudiantsCours)
                .ThenInclude(ec => ec.Etudiant)
                .FirstOrDefaultAsync(c => c.Code == cours3.Code);

        //var elements = _context.Elements.ToList();

        var seedGrille = new List<Grille>
    {
        new Grille
        {
            Titre = "TP1",
            Ponderation = 10,
            //Element = elements.Take(2).ToList(),
            Cours = cours1,
            //ListeElementsEtudiants = ObtenirListeElementEtudiant(cours1WithEtudiants!.EtudiantsCours.Select(x => x.Etudiant).ToList(), elements.Take(2).ToList())
        },
        new Grille
        {
            Titre = "TP2",
            Ponderation = 10,
            //Element = elements.Skip(2).Take(2).ToList(),
            Cours = cours1,
            //ListeElementsEtudiants = ObtenirListeElementEtudiant(cours1WithEtudiants.EtudiantsCours.Select(x => x.Etudiant).ToList(), elements.Skip(2).Take(2).ToList())
        },
        new Grille
        {
            Titre = "TP3",
            Ponderation = 10,
            //Element = elements.Skip(4).Take(2).ToList(),
            Cours = cours2,
            //ListeElementsEtudiants = ObtenirListeElementEtudiant(cours2WithEtudiants!.EtudiantsCours.Select(x => x.Etudiant).ToList(), elements.Skip(4).Take(2).ToList())
        },
        new Grille
        {
            Titre = "TP4",
            Ponderation = 10,
            //Element = elements.Skip(6).Take(2).ToList(),
            Cours = cours2,
            //ListeElementsEtudiants = ObtenirListeElementEtudiant(cours2WithEtudiants.EtudiantsCours.Select(x => x.Etudiant).ToList(), elements.Skip(6).Take(2).ToList())
        },
        new Grille
        {
            Titre = "PROJET 3 : Conception d’un projet de rénovation",
            Ponderation = 20,
            //Element = elements.Skip(8).Take(3).ToList(),
            Cours = cours3,
            //ListeElementsEtudiants = ObtenirListeElementEtudiant(cours3WithEtudiants!.EtudiantsCours.Select(x => x.Etudiant).ToList(), elements.Skip(8).Take(3).ToList())

        }
    };
        var grilleToAdd = seedGrille.Where(g => !_context.Grilles.Any(x => x.Titre == g.Titre)).ToList();

        _context.Grilles.AddRange(grilleToAdd);
        await _context.SaveChangesAsync();
    }
    private async Task SeedCours()
    {
        _logger.LogInformation("Seeding Cours");
        List<Cours> allSeedCours = new List<Cours>()
        {
            cours1,
            cours2,
            cours3
        };

        var coursToAdd = allSeedCours.Where(c => !_context.Cours.Any(x => x.Code == c.Code)).ToList();
        var members = _context.Members.ToList();
        cours1.Membre = members.Find(x => x.Email == "admin@gmail.com");
        cours2.Membre = members.Find(x => x.Email == "user1@gmail.com");
        cours3.Membre = members.Find(x => x.Email == "user2@gmail.com");
        _context.Cours.AddRange(coursToAdd);
        await _context.SaveChangesAsync();
    }
    private async Task SeedEtudiantsCours()
    {
        _logger.LogInformation("Seeding EtudiantCours");
        List<EtudiantCours> seedEtudiantsCours = new List<EtudiantCours>()
        {
            new EtudiantCours
            {
                Groupe = "01",
                Cours = cours1,
                Etudiant = etudiant1
            },
            new EtudiantCours
            {
                Groupe = "01",
                Cours = cours1,
                Etudiant = etudiant2
            },
            new EtudiantCours
            {
                Groupe = "01",
                Cours = cours1,
                Etudiant = etudiant3
            },
            new EtudiantCours
            {
                Groupe = "02",
                Cours = cours1,
                Etudiant = etudiant4
            },
            new EtudiantCours
            {
                Groupe = "02",
                Cours = cours1,
                Etudiant = etudiant5
            },
            new EtudiantCours
            {
                Groupe = "02",
                Cours = cours1,
                Etudiant = etudiant6
            },
            new EtudiantCours
            {
                Groupe = "01",
                Cours = cours2,
                Etudiant = etudiant7
            },
            new EtudiantCours
            {
                Groupe = "01",
                Cours = cours2,
                Etudiant = etudiant8
            },
            new EtudiantCours
            {
                Groupe = "01",
                Cours = cours2,
                Etudiant = etudiant9
            },
            new EtudiantCours
            {
                Groupe = "02",
                Cours = cours2,
                Etudiant = etudiant10
            },
            new EtudiantCours
            {
                Groupe = "02",
                Cours = cours2,
                Etudiant = etudiant11
            },
            new EtudiantCours
            {
                Groupe = "02",
                Cours = cours2,
                Etudiant = etudiant12
            },
               new EtudiantCours
            {
                Groupe = "01",
                Cours = cours3,
                Etudiant = etudiant2
            },
            new EtudiantCours
            {
                Groupe = "01",
                Cours = cours3,
                Etudiant = etudiant3
            },
            new EtudiantCours
            {
                Groupe = "02",
                Cours = cours3,
                Etudiant = etudiant4
            },
            new EtudiantCours
            {
                Groupe = "02",
                Cours = cours3,
                Etudiant = etudiant5
            },
            new EtudiantCours
            {
                Groupe = "02",
                Cours = cours3,
                Etudiant = etudiant6
            },
            new EtudiantCours
            {
                Groupe = "01",
                Cours = cours3,
                Etudiant = etudiant7
            },
            new EtudiantCours
            {
                Groupe = "01",
                Cours = cours3,
                Etudiant = etudiant8
            },
            new EtudiantCours
            {
                Groupe = "01",
                Cours = cours3,
                Etudiant = etudiant9
            },
            new EtudiantCours
            {
                Groupe = "02",
                Cours = cours3,
                Etudiant = etudiant10
            },
            new EtudiantCours
            {
                Groupe = "02",
                Cours = cours3,
                Etudiant = etudiant11
            },
            new EtudiantCours
            {
                Groupe = "02",
                Cours = cours3,
                Etudiant = etudiant12
            }
        };

        var etudiantsCoursToAdd = seedEtudiantsCours.Where(ec => !_context.EtudiantsCours.Any(x => x.Cours.Code == ec.Cours.Code && x.Etudiant.Da == ec.Etudiant.Da)).ToList();

        _context.EtudiantsCours.AddRange(etudiantsCoursToAdd);
        await _context.SaveChangesAsync();
    }
    private async Task SeedEtudiant()
    {
        _logger.LogInformation("Seeding Etudiant");
        List<Etudiant> seedEtudiants = new List<Etudiant>()
        {
            etudiant1,
            etudiant2,
            etudiant3,
            etudiant4,
            etudiant5,
            etudiant6,
            etudiant7,
            etudiant8,
            etudiant9,
            etudiant10,
            etudiant11,
            etudiant12
        };

        var etudiantsToAdd = seedEtudiants.Where(e => !_context.Etudiants.Any(x => x.Da == e.Da)).ToList();
        _context.Etudiants.AddRange(etudiantsToAdd);
        await _context.SaveChangesAsync();
    }
    private async Task SeedElementsEtudiant()
    {
        _logger.LogInformation("Seeding ElementsEtudiants");

        List<ElementEtudiant> seedElementEtudiant = new List<ElementEtudiant>();
        foreach (var etudiant in _context.Etudiants.ToList())
        {
            foreach (var element in _context.Elements.ToList())
            {
                seedElementEtudiant.Add(new ElementEtudiant()
                {
                    Etudiant = etudiant,
                    NoteElement = new Random().Next(0, element.Ponderation + 1),
                    Date = DateTime.Now,
                    ElementEvalue = element,
                    CommentairesEtudiant = _context.CommentairesEtudiant.ToList(),
                    GrilleId = element.GrilleId
                });
            }
        }
        seedElementEtudiant[0].NoteElement = null;
        seedElementEtudiant[2].NoteElement = null;
        var existingElementEtudiant = _context.ElementsEtudiant.IgnoreQueryFilters().FirstOrDefault();
        if (existingElementEtudiant != null)
            return;
        _context.ElementsEtudiant.AddRange(seedElementEtudiant);
        await _context.SaveChangesAsync();
    }
    private async Task SeedCommentairesEtudiant()
    {
        _logger.LogInformation("Seeding CommentairesEtudiant");

        List<CommentaireEtudiant> SeedCommentairesEtudiant = new List<CommentaireEtudiant>
        {
            new CommentaireEtudiant
            {
                Commentaire = "Page de présentation absente."
            },
            new CommentaireEtudiant
            {
                Commentaire = "Bonne argumentation."

            },
            new CommentaireEtudiant
            {
                Commentaire = "Le choix des lments ne rpond pas aux besoins du client."
            }
        };
        await _context.SaveChangesAsync();


    }


    public async Task SeedAsync()
    {
        try
        {
            await SeedRoles();
            await SeedUsersAndMembersForRole(2);
            await SeedCours();
            await SeedEtudiant();
            await SeedEtudiantsCours();
            await SeedGrille();
			await SeedElement();
            await SeedCritere();
            await SeedBareme();
            await SeedCommentaireGlobaux();
            await SeedCommentairesEtudiant();
            await SeedElementsEtudiant();


        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while seeding the database.");
            throw;
        }
    }

    private async Task SeedRoles()
    {
        _logger.LogInformation("Seeding Roles");
        if (!_roleManager.RoleExistsAsync(Roles.ADMINISTRATOR).Result)
            await _roleManager.CreateAsync(new Role { Name = Roles.ADMINISTRATOR, NormalizedName = Roles.ADMINISTRATOR.Normalize() });
        await _roleManager.CreateAsync(new Role { Name = Roles.MEMBER, NormalizedName = Roles.MEMBER.Normalize() });
    }

    private async Task SeedUsersAndMembersForRole(int numberOfUsers)
    {
        _logger.LogInformation($"Seeding Admin(s) and {numberOfUsers} User(s)");

        // Création des administrateurs

        var adminEmail = $"admin@gmail.com";
        var admin = await _userManager.FindByEmailAsync(adminEmail);

        if (admin == null)
        {
            admin = BuildUser(adminEmail);

            var result = await _userManager.CreateAsync(admin, "Qwerty123!");

            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(admin, Roles.ADMINISTRATOR);
                _logger.LogInformation($"Admin {admin.Email} created and added to role Admin");

                var member = new Member("ADMIN", "MEMBER", 1, "123, my street", "my city", "A1A 1A1");
                member.IsAdmin = true;
                member.SetUser(admin);
                member.Activate();
                _context.Members.Add(member);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new Exception($"Could not seed/create Admin user.");
            }
        }
        else
        {
            _logger.LogInformation($"Admin {adminEmail} already exists.");
        }


        // Création des utilisateurs
        for (int i = 0; i < numberOfUsers; i++)
        {
            var userEmail = $"user{i + 1}@gmail.com";
            var user = await _userManager.FindByEmailAsync(userEmail);

            if (user == null)
            {
                user = BuildUser(userEmail);

                var result = await _userManager.CreateAsync(user, "Qwerty123!");

                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, Roles.MEMBER);
                    _logger.LogInformation($"User {user.Email} created and added to role Utilisateur");

                    var member = new Member($"User{i + 1}", "MEMBER", 1, "123, my street", "my city", "A1A 1A1");
                    member.SetUser(user);
                    member.Activate();
                    _context.Members.Add(member);
                    await _context.SaveChangesAsync();
                }
                else
                {
                    throw new Exception($"Could not seed/create Utilisateur user.");
                }
            }
            else
            {
                _logger.LogInformation($"User {userEmail} already exists.");
            }
        }
    }


    private User BuildUser(string email)
    {
        return new User
        {
            Email = email,
            UserName = email,
            NormalizedEmail = email.Normalize(),
            NormalizedUserName = email,
            PhoneNumber = "555-555-5555",
            EmailConfirmed = true,
            PhoneNumberConfirmed = true,
            TwoFactorEnabled = false,
        };
    }


}