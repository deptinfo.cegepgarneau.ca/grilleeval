﻿using System.Diagnostics.CodeAnalysis;
using Application.Extensions;
using Application.Settings;
using Core.Flash;
using Domain.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Web.Extensions;
using Web.ViewModels.ResetPassword;

namespace Web.Controllers;

public class ResetPasswordController : BaseController
{
    private readonly string _redirectUrl;

    private readonly IFlasher _flasher;
    private readonly IUserRepository _userRepository;
    private readonly IStringLocalizer<ResetPasswordController> _localizer;

    public ResetPasswordController(
        IFlasher flasher,
        IUserRepository userRepository,
        ILogger<ResetPasswordController> logger,
        IOptions<ApplicationSettings> applicationSettings,
        IStringLocalizer<ResetPasswordController> localizer) : base(logger)
    {
        _flasher = flasher;
        _localizer = localizer;
        _userRepository = userRepository;
        _redirectUrl = applicationSettings.Value.RedirectUrl;
    }

    [SuppressMessage("ReSharper", "UnusedParameter.Global")]
    public IActionResult Index(string userId, string token)
    {
        return View(new ResetPasswordViewModel());
    }

    [HttpPost]
    public async Task<IActionResult> Index([FromQuery]string userId,
        [FromQuery]string token,
        [FromForm] ResetPasswordViewModel model)
    {
        if (!ModelState.IsValid)
            return View(model);

        var parsingSucceeded = Guid.TryParse(userId, out var id);
        if (!parsingSucceeded)
            return BadRequest();

        var user = _userRepository.FindById(id);
        if (user == null)
            return BadRequest();

        var identityResult = await _userRepository.UpdateUserPassword(user, model.Password, token.Base64UrlDecode());
        if (!identityResult.Succeeded)
        {
            _flasher.Flash(Types.Warning, identityResult.GetErrorMessageForIdentityResultException(_localizer), true);
            return View(model);
        }

        _flasher.Flash(Types.Success, _localizer["PasswordChangedFollowedByRedirect"], true);

        return View(new ResetPasswordViewModel { RedirectUrl = _redirectUrl });
    }
}