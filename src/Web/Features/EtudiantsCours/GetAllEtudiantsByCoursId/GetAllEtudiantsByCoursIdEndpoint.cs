using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.EtudiantsCours.GetAllEtudiantsByCoursId
{
    public class GetAllEtudiantsByCoursIdEndpoint : Endpoint<GetAllEtudiantsByCoursIdRequest,List<EtudiantCoursDto>>
    {
        public readonly IMapper _mapper;
        public readonly IEtudiantCoursRepository _etudiantCoursRepository;
        public GetAllEtudiantsByCoursIdEndpoint(IMapper mapper, IEtudiantCoursRepository etudiantCoursRepository)
        {
            _mapper = mapper;
            _etudiantCoursRepository = etudiantCoursRepository;
        }
        public override void Configure()
        {
            DontCatchExceptions();
            
            Get("etudiantsCours/{coursId}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }
        public override async Task HandleAsync(GetAllEtudiantsByCoursIdRequest req, CancellationToken ct)
        {
            var etudiants = _etudiantCoursRepository.GetAllByIdCours(req.CoursId);            
            var etudiantsDto = _mapper.Map<List<EtudiantCoursDto>>(etudiants);
            await SendOkAsync(etudiantsDto, ct);
        }
    }
}