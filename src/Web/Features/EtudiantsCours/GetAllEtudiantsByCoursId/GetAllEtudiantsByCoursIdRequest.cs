using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Features.EtudiantsCours.GetAllEtudiantsByCoursId
{
    public class GetAllEtudiantsByCoursIdRequest
    {
        public  Guid CoursId { get; set; }
    }
}