﻿namespace Web.Features.EtudiantsCours.DeleteEtudiantCours
{
    public class DeleteEtudiantCoursByIdRequest
    {
        public Guid Id { get; set; }
    }
}
