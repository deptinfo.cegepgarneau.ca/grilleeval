﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Web.Features.EtudiantsCours.DeleteEtudiantCours
{
    public class DeleteEtudiantCoursByIdEndPoint: Endpoint<DeleteEtudiantCoursByIdRequest, EmptyResponse>
    {
        private readonly IEtudiantCoursRepository _etudiantCoursRepository;

        public DeleteEtudiantCoursByIdEndPoint(IEtudiantCoursRepository etudiantCoursRepository)
        {
            _etudiantCoursRepository = etudiantCoursRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Delete("etudiantsCours/{id}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(DeleteEtudiantCoursByIdRequest request, CancellationToken ct)
        {
            await _etudiantCoursRepository.DeleteEtudiantCoursWithId(request.Id);
            await SendNoContentAsync(ct);
        }
    }
}
