using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities.DossierCours;
using Domain.Entities.Etudiants;
using Web.Features.Etudiants;
using Web.Features.Utilisateur.Cours;

namespace Web.Features.EtudiantsCours
{
    public class EtudiantCoursDto
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public Guid EtudiantId { get; set; }
        public Cours Cours { get; set; } = default!;
        public Guid CoursId { get; set; }
        public Etudiant Etudiant { get; set; } = default!;
        public string Groupe { get; set; } = default!;
    }
}