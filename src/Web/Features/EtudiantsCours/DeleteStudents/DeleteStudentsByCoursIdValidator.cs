﻿using System.ComponentModel.DataAnnotations;
using FastEndpoints;
using FluentValidation;

namespace Web.Features.EtudiantsCours.DeleteStudentsByCoursId
{
    public class DeleteStudentsByCoursIdValidator: Validator<DeleteStudentsByCoursIdRequest>
    {
        public DeleteStudentsByCoursIdValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyCoursId")
                .WithMessage("Cours id is required.");
        }
    }
}


