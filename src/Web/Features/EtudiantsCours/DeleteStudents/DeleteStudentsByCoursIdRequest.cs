﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.EtudiantsCours.DeleteStudentsByCoursId
{
    public class DeleteStudentsByCoursIdRequest
    {
        public Guid Id { get; set; }
    }
}


