﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.EtudiantsCours.DeleteStudentsByCoursId;
using IMapper = AutoMapper.IMapper;


namespace Web.Features.EtudiantsCours.DeleteStudentsByCoursId
{
    public class DeleteStudentsByCoursIdEndpoint : Endpoint<DeleteStudentsByCoursIdRequest, EmptyResponse>
    {
        private readonly IEtudiantCoursRepository _etudiantCoursRepository;

        public DeleteStudentsByCoursIdEndpoint(IEtudiantCoursRepository etudiantCoursRepository)
        {
            _etudiantCoursRepository = etudiantCoursRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Delete("cours/{id}/deleteStudents");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }
        public override async Task HandleAsync(DeleteStudentsByCoursIdRequest req, CancellationToken ct)
        {
            await _etudiantCoursRepository.DeleteStudentsWithCourseId(req.Id);
            await SendNoContentAsync(ct);
        }
    }
}


