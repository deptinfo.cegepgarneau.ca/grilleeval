﻿using Domain.Entities;

namespace Web.Features.Utilisateur.Cours.CreateCours
{
    public class CreateCoursRequest
    {
        public string Code { get; set; }
        public string NomCours { get; set; }
        public Member Membre { get; set; }
        public Guid IdMembre { get; set; }
    }
}
