﻿using FastEndpoints;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;
using Application.Interfaces.Services;
using Application.Interfaces.Services.Cours;
using Microsoft.AspNetCore.Authentication.Cookies;
using Domain.Entities.Books;
using System.Diagnostics;
using Application.Interfaces.Services.Members;


namespace Web.Features.Utilisateur.Cours.CreateCours
{
    public class CreateCoursEndpoint : Endpoint<CreateCoursRequest,SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly ICoursCreationServices _coursCreationService;
        private readonly IMemberCreationService _membreCreationService;

        public CreateCoursEndpoint(IMapper mapper, ICoursCreationServices coursCreationServices,
                                   IMemberCreationService membreCreationService)
        {
            _mapper = mapper;
            _coursCreationService = coursCreationServices;
            _membreCreationService = membreCreationService;
        }
        public override void Configure()
        {
            AllowFileUploads();
            DontCatchExceptions();

            Post("cours");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }
        public override async Task HandleAsync(CreateCoursRequest req, CancellationToken ct)
        {
            var cours = _mapper.Map<Domain.Entities.DossierCours.Cours>(req);
            var membre = _membreCreationService.ObtenirMember(req.IdMembre);
            cours.Membre = membre;
            await _coursCreationService.CreateCours(cours);
            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }

    }
}
