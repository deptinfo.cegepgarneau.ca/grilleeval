﻿using Microsoft.IdentityModel.Tokens;
using FastEndpoints;
using FluentValidation;
using FluentValidation.Validators;
using Web.Extensions;

namespace Web.Features.Utilisateur.Cours.CreateCours
{
    public class CreateCoursValidator : Validator<CreateCoursRequest>
    {
        public CreateCoursValidator()
        {
            RuleFor(c => c.Code)
                .NotNull()
                .NotEmpty()
                .Must(c => c.IsValidCode())
                .WithErrorCode("InvalidCode")
                .WithMessage("Code doit être maximum 10 caratères.")
                .Matches("^(.{3})-(.{3})-(.{2})$");
            RuleFor(c => c.NomCours)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidNameCours")
                .WithMessage("NomCours ne peut pas être null ou vide")
                .Length(3, 50);
        }
    }
}
