﻿using Domain.Entities;
using Domain.Entities.CoursEtudiants;
using System.Collections.Generic;

namespace Web.Features.Utilisateur.Cours
{
    public class CoursDto
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public string Code { get; set; } = default!;
        public string NomCours { get; set; } = default!;
        public List<EtudiantCours> listEtudiants { get; set; } = default!;
        public Member Membre { get; set; } = default!;
        public Guid IdMembre { get; set; }
    }
}
