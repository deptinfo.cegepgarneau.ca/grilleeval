﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Utilisateur.Cours.GetCours
{
    public class GetCoursEndpoint : Endpoint<GetCoursRequest, CoursDto>
    {
        private readonly IMapper _mapper;
        private readonly ICoursRepository _coursRepository;

        public GetCoursEndpoint(IMapper mapper, ICoursRepository coursRepository)
        {
            _mapper = mapper;
            _coursRepository = coursRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("cours/{id}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetCoursRequest req, CancellationToken ct)
        {
            var cours = _coursRepository.FindById(req.Id);
            await SendOkAsync(_mapper.Map<CoursDto>(cours));
        }
    }
}
