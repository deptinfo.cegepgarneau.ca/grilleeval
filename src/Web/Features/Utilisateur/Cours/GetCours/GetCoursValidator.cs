﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Utilisateur.Cours.GetCours
{
    public class GetCoursValidator : Validator<GetCoursRequest>
    {
        public GetCoursValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyRegleId")
                .WithMessage("Cours id is required.");
        }
    }
}
