﻿namespace Web.Features.Utilisateur.Cours.GetCoursByMember
{
    public class GetCoursByMemberRequest
    {
        public Guid IdMember { get; set; }
    }
}
