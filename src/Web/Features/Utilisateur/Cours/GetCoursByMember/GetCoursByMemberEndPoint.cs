﻿using Domain.Repositories;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Utilisateur.Cours.GetCours;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Utilisateur.Cours.GetCoursByMember
{
    public class GetCoursByMemberEndPoint : Endpoint<GetCoursByMemberRequest, List<CoursDto>>
    {
        private readonly IMapper _mapper;
        private readonly ICoursRepository _coursRepository;

        public GetCoursByMemberEndPoint(IMapper mapper, ICoursRepository coursRepository)
        {
            _mapper = mapper;
            _coursRepository = coursRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("coursMember/{idMember}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetCoursByMemberRequest req, CancellationToken ct)
        {
            var cours = _coursRepository.GetAllByMember(req.IdMember);
            await SendOkAsync(_mapper.Map<List<CoursDto>>(cours));
        }
    }
}
