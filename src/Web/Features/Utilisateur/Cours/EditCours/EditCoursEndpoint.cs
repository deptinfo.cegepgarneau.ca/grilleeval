﻿using Application.Interfaces.Services.Cours;
using FastEndpoints;
using Web.Features.Common;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Utilisateur.Cours.EditCours
{
    public class EditCoursEndpoint : Endpoint<EditCoursRequest,SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly ICoursUpdateService _coursUpdateService;

        public EditCoursEndpoint(IMapper mapper, ICoursUpdateService coursUpdateService)
        {
            _mapper = mapper;
            _coursUpdateService = coursUpdateService;
        }

        public override void Configure()
        {
            AllowFileUploads();
            DontCatchExceptions();

            Put("cours/{id}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(EditCoursRequest req, CancellationToken ct)
        {
            var cours = _mapper.Map<Domain.Entities.DossierCours.Cours>(req);
            await _coursUpdateService.UpdateCours(cours);
            await SendOkAsync(new SucceededOrNotResponse(true),ct);
        }
    }
}
