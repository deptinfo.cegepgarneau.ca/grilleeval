﻿using FastEndpoints;
using FluentValidation;
using Microsoft.IdentityModel.Tokens;
using Web.Extensions;

namespace Web.Features.Utilisateur.Cours.EditCours
{
    public class EditCoursValidator : Validator<EditCoursRequest>
    {
        public EditCoursValidator()
        {
            RuleFor(c => c.Id)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("EmptyCoursId")
                .WithMessage("L'identifiant ne peut pas être vide ou null");

            RuleFor(c => c.Code)
                .NotNull()
                .NotEmpty()
                .Must(c => c.IsValidCode())
                .WithErrorCode("InvalidCode")
                .WithMessage("Code doit être maximum 10 caratères.");
            RuleFor(c => c.NomCours)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidNameCours")
                .WithMessage("NomCours ne peut pas être null ou vide");
        }
    }
}
