﻿namespace Web.Features.Utilisateur.Cours.EditCours
{
    public class EditCoursRequest
    {
        public  Guid Id { get; set; }
        public  string Code { get; set; }
        public  string NomCours { get; set; }
    }
}
