﻿using System.Diagnostics.CodeAnalysis;

namespace Web.Features.Utilisateur.Cours.GetAllCours
{
    public class CoursCodeComparer : IEqualityComparer<Domain.Entities.DossierCours.Cours>
    {
        public bool Equals(Domain.Entities.DossierCours.Cours? x, Domain.Entities.DossierCours.Cours? y)
        {
            if (Object.ReferenceEquals(x, y)) return true;

            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            return x.Code == y.Code;
        }

        public int GetHashCode([DisallowNull] Domain.Entities.DossierCours.Cours obj)
        {
            if (Object.ReferenceEquals(obj, null)) return 0;

            return obj.Code.GetHashCode();
        }
    }
}
