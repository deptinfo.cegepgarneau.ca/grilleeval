﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Utilisateur.Cours.GetAllCours
{
    public class GetAllCoursEndpoint : EndpointWithoutRequest<List<CoursDto>>
    {
        public readonly IMapper _mapper;
        public readonly ICoursRepository _coursRepository;

        public GetAllCoursEndpoint(IMapper mapper, ICoursRepository coursRepository)
        {
            _mapper = mapper;
            _coursRepository = coursRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("cours");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var cours = _coursRepository.GetAll();
            await SendOkAsync(_mapper.Map<List<CoursDto>>(cours), ct);
        }
    }
}
