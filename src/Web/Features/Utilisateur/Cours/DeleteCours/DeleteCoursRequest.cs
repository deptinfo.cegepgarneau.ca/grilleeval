﻿namespace Web.Features.Utilisateur.Cours.DeleteCours
{
    public class DeleteCoursRequest
    {
        public Guid Id { get; set; }
    }
}
