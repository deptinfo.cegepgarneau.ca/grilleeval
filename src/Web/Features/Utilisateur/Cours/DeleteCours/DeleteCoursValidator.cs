﻿using FastEndpoints;
using FluentValidation;
using Microsoft.IdentityModel.Tokens;

namespace Web.Features.Utilisateur.Cours.DeleteCours
{
    public class DeleteCoursValidator : Validator<DeleteCoursRequest>
    {
        public DeleteCoursValidator()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyCodeError")
                .WithMessage("L'id ne peut etre null ou vide");
        }
    }
}
