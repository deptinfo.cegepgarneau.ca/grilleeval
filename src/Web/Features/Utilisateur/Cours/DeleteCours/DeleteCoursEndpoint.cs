﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Web.Features.Utilisateur.Cours.DeleteCours
{
    public class DeleteCoursEndpoint : Endpoint<DeleteCoursRequest,EmptyResponse>
    {
        private readonly ICoursRepository _coursRepository;

        public DeleteCoursEndpoint(ICoursRepository coursRepository)
        {
            _coursRepository = coursRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Delete("cours/{id}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(DeleteCoursRequest req, CancellationToken ct)
        {
            await _coursRepository.DeleteCoursWithId(req.Id);
            await SendNoContentAsync(ct);
        }

    }
}
