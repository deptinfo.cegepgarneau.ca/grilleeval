using FastEndpoints;
using FluentValidation;

namespace Web.Features.Etudiants.GetEtudiant
{
    public class GetEtudiantValidator : Validator<GetEtudiantRequest>
    {
        public GetEtudiantValidator()
        {
            RuleFor(x => x.Da)
                .NotEmpty()
                .WithErrorCode("EmptyRegleId")
                .WithMessage("Cours id is required.");
        }
    }
}