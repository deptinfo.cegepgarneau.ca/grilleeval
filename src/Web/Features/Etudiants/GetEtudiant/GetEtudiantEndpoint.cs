using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Etudiants.GetEtudiant
{
    public class GetEtudiantEndpoint : Endpoint<GetEtudiantRequest, EtudiantDto>
    {
        private readonly IMapper _mapper;
        private readonly IEtudiantRepository _etudiantRepository;

        public GetEtudiantEndpoint(IMapper mapper, IEtudiantRepository etudiantRepository)
        {
            _mapper = mapper;
            _etudiantRepository = etudiantRepository;
        }
        public override void Configure()
        {
            DontCatchExceptions();

            Get("etudiants/{id}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetEtudiantRequest req, CancellationToken ct)
        {
            var etudiant = _etudiantRepository.FindById(req.Da);
            await SendOkAsync(_mapper.Map<EtudiantDto>(etudiant));
        }
    }
}