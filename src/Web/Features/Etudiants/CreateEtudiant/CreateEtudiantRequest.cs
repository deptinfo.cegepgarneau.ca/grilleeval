using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Features.Etudiants.CreateEtudiant
{
    public class CreateEtudiantRequest
    {
        public Guid Id { get; set; }
        public string? Da { get; set; }
        public string? Prenom { get; set; }
        public string? Nom { get; set; }
    }
}