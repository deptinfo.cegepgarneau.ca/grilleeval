﻿using IMapper = AutoMapper.IMapper;
using FastEndpoints;
using Web.Features.Common;
using Domain.Repositories;
using Microsoft.AspNetCore.Authentication.Cookies;
using Domain.Entities.Etudiants;

namespace Web.Features.Etudiants.CreateEtudiant
{
    public class CreateEtudiantEndpoint : Endpoint<CreateEtudiantRequest, SucceededOrNotResponse>
    {
        public readonly IMapper _mapper;
        public readonly IEtudiantRepository _etudiantRepository;

        public CreateEtudiantEndpoint(IMapper mapper, IEtudiantRepository etudiantRepository)
        {
            _mapper = mapper;
            _etudiantRepository = etudiantRepository;
        }
        public override void Configure()
        {
            AllowFileUploads();
            DontCatchExceptions();
            Post("etudiant");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CreateEtudiantRequest req, CancellationToken ct)
        {
            var etudiant = _mapper.Map<Etudiant>(req);
            await _etudiantRepository.AddEtudiant(etudiant);
            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }

    }
}


