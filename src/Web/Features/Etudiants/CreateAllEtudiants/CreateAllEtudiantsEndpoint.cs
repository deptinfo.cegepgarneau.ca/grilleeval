using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMapper = AutoMapper.IMapper;
using FastEndpoints;
using Web.Features.Common;
using Domain.Repositories;
using Microsoft.AspNetCore.Authentication.Cookies;
using Domain.Entities.Etudiants;
using Application.Interfaces.Services.Etudiants;
using Azure.Core;
using Application.Interfaces.Services.EtudiantsCours;
using Domain.Entities.CoursEtudiants;
using ArgumentNullException = System.ArgumentNullException;

namespace Web.Features.Etudiants.CreateAllEtudiants
{
    public class CreateAllEtudiantsEndpoint : Endpoint<CreateAllEtudiantsRequest, SucceededOrNotResponse>
    {
        public readonly IMapper _mapper;
        public readonly IEtudiantsCreationService _etudiantCreationService;
        public readonly IEtudiantsCoursCreationService _etudiantsCoursCreationService;
        public readonly IEtudiantRepository _etudiantRepository;

        public CreateAllEtudiantsEndpoint(IMapper mapper, IEtudiantsCreationService etudiantCreationService, IEtudiantsCoursCreationService etudiantsCoursCreationService, IEtudiantRepository etudiantRepository)
        {
            _mapper = mapper;
            _etudiantCreationService = etudiantCreationService;
            _etudiantsCoursCreationService = etudiantsCoursCreationService;
            _etudiantRepository = etudiantRepository;
        }
        public override void Configure()
        {
            DontCatchExceptions();
            Post("{coursId}/etudiants");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }
        public override async Task HandleAsync(CreateAllEtudiantsRequest req, CancellationToken ct)
        {
            Guid coursId = Route<Guid>("coursId");
            
            if(coursId==Guid.Empty)
                throw new ArgumentException("Invalid CoursId. Please provide a valid course ID.", nameof(coursId));
            if(req.Etudiants==null || req.Etudiants.Count==0)
                throw new ArgumentException("No students to create. Please provide a list of students.", nameof(req.Etudiants));
            
            List<EtudiantDto> etudiantsDto = req.Etudiants;
            List<Etudiant> etudiants = req.Etudiants?.Select(e => _mapper.Map<Etudiant>(e)).ToList();
            
            if(etudiants==null || etudiants.Count==0)
                throw new ArgumentException("No students to create. The mapped list of students is empty.", nameof(etudiants));
            
            await _etudiantCreationService.CreateEtudiants(etudiants);
            List<Etudiant> lstEtudiant = new List<Etudiant>();
            foreach (var e in etudiants)
            {
                var etudiant = _etudiantRepository.FindById(e.Da);
                lstEtudiant.Add(etudiant);
            }            
            List<EtudiantCours> etudiantsCours = new List<EtudiantCours>();
            for (int i = 0; i < etudiantsDto.Count; i++)
            {
                var etudiantCours = new EtudiantCours
                {
                    Groupe = etudiantsDto[i].Groupe,
                    CoursId = coursId,
                    EtudiantId = lstEtudiant[i].Id
                };

                etudiantsCours.Add(etudiantCours);
            }
            
            await _etudiantsCoursCreationService.CreateEtudiantsCours(etudiantsCours);

            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }
    }
}