using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities.Etudiants;

namespace Web.Features.Etudiants.CreateAllEtudiants
{
    public class CreateAllEtudiantsRequest
    {
        public List<EtudiantDto>? Etudiants { get; set; }
    }
}