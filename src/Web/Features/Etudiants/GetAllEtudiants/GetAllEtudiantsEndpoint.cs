using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Repositories;
using FastEndpoints;
using IMapper = AutoMapper.IMapper;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Web.Features.Etudiants.GetAllEtudiants
{
    public class GetAllEtudiantsEndpoint : EndpointWithoutRequest<List<EtudiantDto>>
    {
        private readonly IMapper _mapper;
        private readonly IEtudiantRepository _etudiantRepository;
        public GetAllEtudiantsEndpoint(IMapper mapper, IEtudiantRepository etudiantRepository)
        {
            _mapper = mapper;
            _etudiantRepository = etudiantRepository;
        }
        public override void Configure()
        {
            DontCatchExceptions();
            
            Get("etudiants");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var etudiants = _etudiantRepository.GetAll();
            await SendOkAsync(_mapper.Map<List<EtudiantDto>>(etudiants), ct);

        }
    }
}