namespace Web.Features.Etudiants
{
    public class EtudiantDto
    {
        public Guid Id { get; set; }
        public string Da { get; set; } = default!;
        public string Nom { get; set; } = default!;
        public string Prenom { get; set; } = default!;
        public string? Groupe { get; set; }
    }
}