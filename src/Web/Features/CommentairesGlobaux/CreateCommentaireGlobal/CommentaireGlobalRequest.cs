
public class CreateCommentaireGlobalRequest
{
	public Guid ElementId { get; set; }
	public string Commentaire { get; set; } = default!;
}