using Application.Interfaces.Services.CommentairesGlobaux;
using Domain.Entities.CommentaireGlobaux;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

public class CreateCommentaireGlobalEndPoint: Endpoint<CreateCommentaireGlobalRequest, SucceededOrNotResponse>
{
	private readonly IMapper _mapper;
	private readonly ICommentaireGlobalCreationService _commentaireGlobalCreationService;

	public CreateCommentaireGlobalEndPoint(IMapper mapper, ICommentaireGlobalCreationService commentaireGlobalCreationService)
	{
		_mapper = mapper;
		_commentaireGlobalCreationService = commentaireGlobalCreationService;
	}

	public override void Configure()
	{
		AllowFileUploads();
		DontCatchExceptions();

		Post("commentaireGlobal/create");
		AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
	}

	public override async Task HandleAsync(CreateCommentaireGlobalRequest req, CancellationToken ct)
	{
		var comment = _mapper.Map<CommentaireGlobal>(req);

		await _commentaireGlobalCreationService.CreateCommentaireGlobal(comment, req.ElementId);
		await SendOkAsync(new SucceededOrNotResponse(true), ct);
	}
}