﻿namespace Web.Features.CommentairesGlobaux
{
    public class CommentairesGlobauxDto
    {
        public string Commentaire { get; set; } = default!;

        public Guid Id { get; set; }

        public Guid ElementId { get; set; }

    }
}
