using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;

public class DeleteCommentaireGlobalEndPoint: Endpoint<DeleteCommentaireGlobalRequest, EmptyRequest>
{
	private readonly ICommentaireGlobalRepository _commentaireGlobalRepository;

	public DeleteCommentaireGlobalEndPoint(ICommentaireGlobalRepository commentaireGlobalRepository)
	{
		_commentaireGlobalRepository = commentaireGlobalRepository;
	}

	public override void Configure()
	{
		DontCatchExceptions();

		Delete("commentaireGlobal/{id}");

		AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
	}

	public override async Task HandleAsync(DeleteCommentaireGlobalRequest req, CancellationToken ct)
	{
		await _commentaireGlobalRepository.DeleteCommentaireGlobal(req.Id);
		await SendNoContentAsync(ct);
	}
}