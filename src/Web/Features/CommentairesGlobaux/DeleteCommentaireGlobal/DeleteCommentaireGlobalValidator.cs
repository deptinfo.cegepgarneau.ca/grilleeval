using FastEndpoints;
using FluentValidation;

public class DeleteCommentaireGlobalValidator: Validator<DeleteCommentaireGlobalRequest>
{
	public DeleteCommentaireGlobalValidator()
	{
		RuleFor(x => x.Id)
			.NotEqual(Guid.Empty)
			.WithErrorCode("EmptyCommentaireGlobalId")
			.WithMessage("CommentaireGlobal id is required");
	}
}