using Application.Interfaces.Services.Elements;
using Domain.Entities.Elements;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

public class CreateElementEndPoint : Endpoint<CreateElementRequest, SucceededOrNotResponse>
{
	public readonly IMapper _mapper;
	public readonly IElementCreationService _elementCreationService;

	public CreateElementEndPoint(IMapper mapper, IElementCreationService elementCreationService)
	{
		_mapper = mapper;
		_elementCreationService = elementCreationService;
	}

	public override void Configure()
	{
		AllowFileUploads();
		DontCatchExceptions();

		Post("element/create");
		AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);

	}

	public override async Task HandleAsync(CreateElementRequest req, CancellationToken ct)
	{
		var element = _mapper.Map<Element>(req);

		await _elementCreationService.CreateElement(element, req.GrilleId);
		await SendOkAsync(new SucceededOrNotResponse(true), ct);
	}
}