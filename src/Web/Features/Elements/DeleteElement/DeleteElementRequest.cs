namespace Web.Features.Elements.DeleteElement;

public class DeleteElementRequest
{
	public Guid Id { get; set; }
}
