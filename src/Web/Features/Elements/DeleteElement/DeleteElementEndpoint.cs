using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Web.Features.Elements.DeleteElement;

public class DeleteElementEndPoint : Endpoint<DeleteElementRequest, EmptyResponse>
{
	private readonly IElementRepository _elementRepository;

	public DeleteElementEndPoint(IElementRepository elementRepository)
	{
		_elementRepository = elementRepository;
	}

	public override void Configure()
	{
		DontCatchExceptions();

		Delete("element/{id}");

		AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
	}

	public override async Task HandleAsync(DeleteElementRequest req, CancellationToken ct)
	{
		await _elementRepository.DeleteElement(req.Id);
		await SendNoContentAsync(ct);
	}
}