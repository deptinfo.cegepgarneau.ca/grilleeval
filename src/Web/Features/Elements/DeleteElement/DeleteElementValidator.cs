using FastEndpoints;
using FluentValidation;

namespace Web.Features.Elements.DeleteElement;

public class DeleteElementValidator : Validator<DeleteElementRequest>
{
	public DeleteElementValidator()
	{
		RuleFor(x => x.Id)
			.NotEqual(Guid.Empty)
			.WithErrorCode("EmptyElementId")
			.WithMessage("Element id is required");
	}
}