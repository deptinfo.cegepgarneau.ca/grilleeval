﻿using Domain.Entities.Baremes;
using Domain.Entities.CommentaireGlobaux;
using Domain.Entities.Criteres;

namespace Web.Features.Elements
{
    public class ElementDto
    {
        public Guid Id { get; set; }
        //public Guid GrilleId { get; set; }
        public string Titre { get; set; } = default!;
        public int Ponderation { get; set; } = default!;
        public List<CommentaireGlobal> CommentairesGlobaux { get; set; } = default!;
        public List<Critere> Criteres { get; set; } = default!;
        public List<Bareme> Baremes { get; set; } = default!;
    }
}
