using Domain.Entities.Baremes;
using Domain.Entities.CommentaireGlobaux;
using Domain.Entities.Criteres;

public class UpdateElementRequest
{
	public Guid Id { get; set; }
	public Guid GrilleId { get; set; }
	public string Titre { get; set; } = default!;
	public double Ponderation { get; set; } = default!;
	public List<Bareme>? Bareme { get; set; }
	public List<CommentaireGlobal>? CommentaireGlobal { get; set; }
	public List<Critere>? Critere { get; set; }
}