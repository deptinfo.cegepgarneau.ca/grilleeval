using Application.Interfaces.Services.Elements;
using Domain.Entities.Elements;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

public class UpdateElementEndPoint : Endpoint<UpdateElementRequest, SucceededOrNotResponse>
{
	public readonly IMapper _mapper;
	public readonly IElementCreationService _elementCreationService;

	public UpdateElementEndPoint(IMapper mapper, IElementCreationService elementCreationService)
	{
		_mapper = mapper;
		_elementCreationService = elementCreationService;
	}

	public override void Configure()
	{
		AllowFileUploads();
		DontCatchExceptions();

		Put("element/{id}");
		AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);

	}

	public override async Task HandleAsync(UpdateElementRequest req, CancellationToken ct)
	{
		var element = _mapper.Map<Element>(req);

		await _elementCreationService.UpdateElement(element);
		await SendOkAsync(new SucceededOrNotResponse(true), ct);
	}
}