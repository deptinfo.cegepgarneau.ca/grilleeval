﻿using Application.Interfaces.Services.Books;
using Application.Interfaces.Services.ElementsEtudiant;
using Application.Interfaces.Services.Grilles;
using Application.Services.Grilles;
using Domain.Entities.ElementsEtudiant;
using Domain.Entities.Etudiants;
using Domain.Entities.Grilles;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using Web.Features.EtudiantsCours;
using IMapper = AutoMapper.IMapper;



namespace Web.Features.ElementsEtudiant.CreateElementsEtudiant;

public class CreateElementsEtudiantEndpoint : Endpoint<CreateElementsEtudiantRequest, SucceededOrNotResponse>
{

    private readonly IMapper _mapper;

    private readonly IElementEtudiantCreationService _elementEtudiantCreationService;


    public CreateElementsEtudiantEndpoint(IMapper mapper, IElementEtudiantCreationService elementEtudiantCreationService)
    {
        _mapper = mapper;
        _elementEtudiantCreationService = elementEtudiantCreationService;
     
    }

    public override void Configure()
    {
   
        DontCatchExceptions();
        Post("elementsEtudiant");
        AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
    }


    public override async Task HandleAsync(CreateElementsEtudiantRequest req, CancellationToken ct)
    {
        await _elementEtudiantCreationService.CreateElementEtudiant(req.ElementsEtudiant);
        await SendOkAsync(new SucceededOrNotResponse(true), ct);
    }

}
