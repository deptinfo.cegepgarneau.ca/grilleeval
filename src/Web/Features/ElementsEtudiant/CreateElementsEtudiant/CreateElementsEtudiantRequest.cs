﻿using Application.Interfaces.Services.ElementsEtudiant;
using Application.Services.ElementsEtudiant;

namespace Web.Features.ElementsEtudiant.CreateElementsEtudiant
{
    public class CreateElementsEtudiantRequest
    {

        public List<ElementEtudiantSimplifie> ElementsEtudiant { get; set; } = new List<ElementEtudiantSimplifie>();

    }
  
}
