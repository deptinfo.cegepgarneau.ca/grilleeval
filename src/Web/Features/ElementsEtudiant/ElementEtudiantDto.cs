﻿using Domain.Entities.CommentairesEtudiant;
using Domain.Entities.Elements;
using Domain.Entities.Grilles;

namespace Web.Features.ElementsEtudiant
{
    public class ElementEtudiantDto
    {
        public Guid Id { get; set; }

        public Guid GrilleId { get; set; }

        public Grille Grille { get; set; } = default!;

        public float NoteElement { get; set; } = default!;

        public DateTime Date { get; set; } = default!;

        public Element ElementEvalue { get; set; } = default!;

        public List<CommentaireEtudiant> CommentairesEtudiant { get; set; } = default!;



    }
}
