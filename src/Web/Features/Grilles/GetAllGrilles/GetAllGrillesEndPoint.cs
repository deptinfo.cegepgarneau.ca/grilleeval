﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;


namespace Web.Features.Grilles.GetAllGrilles
{
    public class GetAllGrillesEndPoint : EndpointWithoutRequest<List<GrilleDto>>
    {
        private readonly IMapper _mapper;
        private readonly IGrilleRepository _grilleRepository;

        public GetAllGrillesEndPoint(IMapper mapper, IGrilleRepository grilleRepository)
        {
            _mapper = mapper;
            _grilleRepository = grilleRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("grilles");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var grilles = _grilleRepository.GetAll();
            await SendOkAsync(_mapper.Map<List<GrilleDto>>(grilles), ct);
        }
    }
}
