
using Application.Interfaces.Services.Grilles;
using Domain.Entities.Grilles;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

public class CreateGrilleEndPoint : Endpoint<CreateGrilleRequest, SucceededOrNotResponse>
{
	public readonly IMapper _mapper;
	public readonly IGrilleCreationService _grilleCreationService;
	public readonly ICoursRepository _coursRepository;

	public CreateGrilleEndPoint(IMapper mapper, IGrilleCreationService grilleCreationService, ICoursRepository coursRepository)
	{
		_mapper = mapper;
		_grilleCreationService = grilleCreationService;
		_coursRepository = coursRepository;
	}

	public override void Configure()
	{
		AllowFileUploads();
		DontCatchExceptions();

		Post("grille/create");
		//Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
		AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
	}

	public override async Task HandleAsync(CreateGrilleRequest req, CancellationToken ct)
	{
		var grille = _mapper.Map<Grille>(req);
		// grille.Cours = _coursRepository.FindById(req.CoursId);
		await _grilleCreationService.CreateGrille(grille, req.CoursId);
		await SendOkAsync(new SucceededOrNotResponse(true), ct);
	}
}