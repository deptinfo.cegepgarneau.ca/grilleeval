

using FastEndpoints;
using FluentValidation;

public class CreateGrilleValidator: Validator<CreateGrilleRequest>
{
	public CreateGrilleValidator()
	{
		RuleFor(x => x.Titre)
			.NotEmpty()
			.NotNull()
			.WithErrorCode("InvalideTitre")
			.WithMessage("Le titre de la grille ne peut �tre null ou vide.");

		RuleFor(x => x.Ponderation)
			.GreaterThan(0)
			.LessThan(100)
			.NotNull()
			.WithErrorCode("InvalidePonderation")
			.WithMessage("La pond�ration ne peut pas �tre �gal ou inf�rieur � 0 et ne peut d�passer 100.");

		RuleFor(x => x.CoursId)
			.NotNull()
			.NotEmpty()
			.WithErrorCode("InvalideCours")
			.WithMessage("Le cours ne peut �tre null.");
	}
}