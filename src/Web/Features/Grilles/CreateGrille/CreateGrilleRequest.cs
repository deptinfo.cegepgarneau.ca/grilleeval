using Domain.Entities.DossierCours;
using Domain.Entities.Elements;
using Web.Features.Elements;

public class CreateGrilleRequest
{
	public Guid Id { get; set; }
	public string Titre { get; set; } = default!;
	public double Ponderation { get; set; } = default!;
	public List<ElementDto>? Element { get; set; }
	public Guid CoursId { get; set; } = default!;
}