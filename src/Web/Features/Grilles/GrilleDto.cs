﻿using Domain.Entities.DossierCours;
using Domain.Entities.Elements;
using Domain.Entities.ElementsEtudiant;
using Web.Features.Utilisateur.Cours;

namespace Web.Features.Grilles
{
    public class GrilleDto
    {
        public Guid Id { get; set; }
        public string Titre { get; set; } = default!;
        public float Ponderation { get; set; } = default!;
        public List<Element> Element { get; set; } = default!;
        public Cours Cours { get; set; } = default!;
        public List<ElementEtudiant> ListeElementsEtudiants {  get; set; } = default!;
    
		public Guid CoursId { get; set; }

	}
}
