using IMapper = AutoMapper.IMapper;
using Web.Features.Grilles;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Domain.Entities.Grilles;
public class GetGrilleWithCoursIdEndPoint : Endpoint<GetGrilleWithCoursIdRequest, List<GrilleDto>>
{
	private readonly IMapper _mapper;
	private readonly IGrilleRepository _grilleRepository;

	public GetGrilleWithCoursIdEndPoint(IMapper mapper, IGrilleRepository grilleRepository)
	{
		_mapper = mapper;
		_grilleRepository = grilleRepository;
	}

	public override void Configure()
	{
		DontCatchExceptions();

		Get("grilles/cours/{id}");
		AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
	}

	public override async Task HandleAsync(GetGrilleWithCoursIdRequest request, CancellationToken ct)
	{
		var grilles = _grilleRepository.GetGrilleWithCoursId(request.Id);
		await SendOkAsync(_mapper.Map<List<GrilleDto>>(grilles), ct);
	}
}