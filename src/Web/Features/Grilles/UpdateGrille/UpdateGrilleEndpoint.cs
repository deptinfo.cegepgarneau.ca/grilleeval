using Application.Interfaces.Services.Grilles;
using Domain.Entities.Grilles;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

public class UpdateGrilleEndPoint : Endpoint<UpdateGrilleRequest, SucceededOrNotResponse>
{
	public readonly IMapper _mapper;
	public readonly IGrilleCreationService _grilleCreationService;

	public UpdateGrilleEndPoint(IMapper mapper,  IGrilleCreationService grilleCreationService)
	{
		_mapper = mapper;
		_grilleCreationService = grilleCreationService;
	}

	public override void Configure()
	{
		AllowFileUploads();
		DontCatchExceptions();

		Put("grille/{id}");
		AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
	}

	public override async Task HandleAsync(UpdateGrilleRequest req, CancellationToken ct)
	{
		var grille = _mapper.Map<Grille>(req);

		await _grilleCreationService.UpdateGrille(grille);
		await SendOkAsync(new SucceededOrNotResponse(true), ct);
	}
}