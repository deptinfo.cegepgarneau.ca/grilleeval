using Domain.Entities.Elements;

public class UpdateGrilleRequest
{
	public Guid Id { get; set; }
	public Guid CoursId { get; set; }
	public string Titre { get; set; } = default!;
	public double Ponderation { get; set; } = default!;

	public List<Element> Element { get; set; } = default!;
}