﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Admins.Books.GetBook;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Grilles.GetGrille
{
    public class GetGrilleEndPoint : Endpoint<GetGrilleRequest, GrilleDto>
    {
        private readonly IMapper _mapper;
        private readonly IGrilleRepository _grilleRepository;

        public GetGrilleEndPoint(IMapper mapper, IGrilleRepository grilleRepository)
        {
            _mapper = mapper;
            _grilleRepository = grilleRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("grilles/{id}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetGrilleRequest request, CancellationToken ct)
        {
            var grille = _grilleRepository.FindById(request.Id);
            var grilleDto = _mapper.Map<GrilleDto>(grille);
            await SendOkAsync(grilleDto, ct);
        }
    }
}
