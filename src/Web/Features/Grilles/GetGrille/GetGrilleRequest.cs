﻿namespace Web.Features.Grilles.GetGrille
{
    public class GetGrilleRequest
    {
        public Guid Id { get; set; }
    }
}
