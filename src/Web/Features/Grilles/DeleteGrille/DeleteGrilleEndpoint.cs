﻿using Azure.Core;
using Domain.Entities;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Members.DeleteMember;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Grilles.DeleteGrille;

public class DeleteGrilleEndpoint: Endpoint<DeleteGrilleRequest, EmptyResponse>
{
    private readonly IGrilleRepository _grilleRepository;

    public DeleteGrilleEndpoint(IGrilleRepository grilleRepository)
    {
        _grilleRepository = grilleRepository;
    }

    public override void Configure()
    {
        DontCatchExceptions();

        Delete("grilles/{id}");
        Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
        AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
    }

    public override async Task HandleAsync(DeleteGrilleRequest request, CancellationToken ct)
    {
        await _grilleRepository.DeleteGrilleWithId(request.Id);
        await SendOkAsync(ct);
    }
}