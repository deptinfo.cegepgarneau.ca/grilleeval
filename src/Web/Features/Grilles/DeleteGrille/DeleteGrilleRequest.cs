﻿namespace Web.Features.Grilles.DeleteGrille;

public class DeleteGrilleRequest
{
    public Guid Id { get; set; }
}