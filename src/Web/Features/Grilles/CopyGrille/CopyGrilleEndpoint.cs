using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using Web.Features.Grilles.CopyGrille;
using IMapper = AutoMapper.IMapper;

public class CopyGrilleEndpoint : Endpoint<CopyGrilleRequest, SucceededOrNotResponse>
{
    public readonly IGrilleRepository _grilleRepository;
    public readonly IMapper _mapper;

    public CopyGrilleEndpoint(IGrilleRepository grilleRepository, IMapper mapper)
    {
        _grilleRepository = grilleRepository;
        _mapper = mapper;
    }
    public override void Configure()
    {
        AllowFileUploads();
        DontCatchExceptions();

        Post("grille/copy");
        //Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
        AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
    }
    public override async Task HandleAsync(CopyGrilleRequest req, CancellationToken ct)
    {
        await _grilleRepository.CopyGrille(req.grilleId, req.coursId,req.titre,req.ponderation);
        await SendOkAsync(new SucceededOrNotResponse(true), ct);

    }
}