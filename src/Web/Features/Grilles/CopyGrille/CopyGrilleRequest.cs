namespace Web.Features.Grilles.CopyGrille;

public class CopyGrilleRequest
{
    public Guid grilleId { get; set; }
    public Guid coursId { get; set; }
    public string titre { get; set; }
    public int ponderation { get; set; }
}