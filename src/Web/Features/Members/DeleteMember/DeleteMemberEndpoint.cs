﻿

using Azure.Core;
using Domain.Entities;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Members.DeleteMember;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Members.DeleteMember;

public class DeleteMemberEndpoint: Endpoint<DeleteMemberRequest, EmptyResponse>
{
    private readonly IMapper _mapper;
    private readonly IMemberRepository _memberRepository;

    public DeleteMemberEndpoint(IMapper mapper, IMemberRepository memberRepository)
    {
        _mapper = mapper;
        _memberRepository = memberRepository;
    }

    public override void Configure()
    {
        DontCatchExceptions();

        Delete("delete-member/{id}");
        Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
        AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
    }

    public override async Task HandleAsync(DeleteMemberRequest request, CancellationToken ct)
    {
        await _memberRepository.DeleteMemberById(request.Id);
        await SendOkAsync(ct);
    }
}