﻿namespace Web.Features.Members.DeleteMember;

public class DeleteMemberRequest
{
    public Guid Id { get; set; }
}