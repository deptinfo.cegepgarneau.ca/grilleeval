﻿namespace Web.Features.Members.CreateMember
{
    public class CreateMemberRequest
    {
        public string LastName { get; set; } = default!;
        public string FirstName { get; set; } = default!;
        public string Email { get; set; } = default!;
        public string Password { get; set; } = default!;
        public bool IsAdmin { get; set; } = default!;
    }
}