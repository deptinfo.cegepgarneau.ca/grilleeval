﻿using System.Data;
using FastEndpoints;
using FluentValidation;
using Microsoft.IdentityModel.Tokens;
using Web.Features.Members.CreateMember;

namespace Web.Features.Members;

public class CreateMemberValidator : Validator<CreateMemberRequest>
{
    public CreateMemberValidator()
    {
        RuleFor(x => x.LastName)
            .NotEmpty()
            .NotNull()
            .Must(x => x.Trim().Length <= 50)
            .WithErrorCode("InvalidLastName")
            .WithMessage("Le nom ne doit pas contenir plus de 50 caractères.");
        RuleFor(x => x.FirstName)
            .NotEmpty()
            .NotNull()
            .Must(x => x.Trim().Length <= 25)
            .WithErrorCode("InvalidFirstName")
            .WithMessage("Le prénom ne doit pas contenir plus de 25 caractères.");
        RuleFor(x => x.Email).NotEmpty()
            .NotNull()
            .NotEmpty()
            .WithErrorCode("InvalidEmail")
            .WithMessage("Le nom d'un ingredient ne peut être null ou vide.");
        RuleFor(x => x.Password)
            .NotEmpty()
            .NotNull()
            .WithErrorCode("InvalidPassword")
            .WithMessage("Le nom d'un ingredient ne peut être null ou vide.");
    }
}
