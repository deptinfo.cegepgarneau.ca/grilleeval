﻿using Xero.NetStandard.OAuth2.Model.Files;

namespace Web.Features.Members
{
    public class MemberDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; } = default!;
        public string LastName { get; set; } = default!;
        
        public string Password { get; set; } = default!;
        public List<string> Roles { get; set; } = default!;
        public string Email { get; set; } = default!;
        
        public bool IsAdmin { get; set; } = default!;
    }
}
