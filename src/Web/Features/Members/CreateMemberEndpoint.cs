﻿using Application.Common;
using Application.Interfaces.Services.Members;
using Domain.Entities;
using Domain.Entities.Identity;
using Domain.Repositories;
using FastEndpoints;
using Infrastructure.Repositories.Users;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Web.Features.Common;
using Web.Features.Members.CreateMember;
using IMapper = AutoMapper.IMapper;



namespace Web.Features.Members
{
    public class CreateMemberEndpoint : Endpoint<CreateMemberRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IMemberCreationService _memberCreationService;
        private readonly IUserRepository _userRepository;
        private UserManager<User> _userManager;


        public CreateMemberEndpoint(IMapper mapper, IMemberCreationService memberCreationService, IUserRepository userRepository,UserManager<User> userManager)
        {
            _mapper = mapper;
            _memberCreationService = memberCreationService;
            _userRepository = userRepository;
            _userManager = userManager;
        }

        public override void Configure()
        {
            DontCatchExceptions();
            Post("utilisateur");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CreateMemberRequest req, CancellationToken ct)
        {
            User utilisateur = new User
            {
                Email = req.Email,
                UserName = req.Email,
                NormalizedEmail = req.Email.Normalize(),
                NormalizedUserName = req.Email,
                EmailConfirmed = true,
                TwoFactorEnabled = false
            };

            //Vérifier si l'utilisateur existe déjà
            bool existingUser = _userRepository.UserWithEmailExists(req.Email);
            if (existingUser)
            {
                Error error = new Error("UserEmailAlreadyExists", "Un utilisateur avec cet email existe déjà.");
                await SendOkAsync(new SucceededOrNotResponse(false, error), ct);
                return;
            }

            //Création de l'utilisateur
            IdentityResult? result = await _userManager.CreateAsync(utilisateur, req.Password);
            //Ajout de son rôle
            IdentityResult? result2;
            
            if (req.IsAdmin)
            {
                result2 = await _userManager.AddToRoleAsync(utilisateur, Domain.Constants.User.Roles.ADMINISTRATOR);
            }
            else
            {
                result2 = await _userManager.AddToRoleAsync(utilisateur, Domain.Constants.User.Roles.MEMBER);
            }


            if (!result.Succeeded)
            {
                await SendOkAsync(new SucceededOrNotResponse(false), ct);
                return;
            }



            Member member  = _mapper.Map<Member>(req);
            member.SetUser(utilisateur);
            member.Activate();
            await _memberCreationService.CreateMember(member);
            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }
    }
}
