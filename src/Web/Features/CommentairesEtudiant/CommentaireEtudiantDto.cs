﻿using Domain.Entities.Etudiants;

namespace Web.Features.CommentairesEtudiant
{
    public class CommentaireEtudiantDto
    {
        public string Commentaire { get; set; } = default!;

        public Guid Id { get; set; }

        public Guid EtudiantId { get; set; }

    }
}
