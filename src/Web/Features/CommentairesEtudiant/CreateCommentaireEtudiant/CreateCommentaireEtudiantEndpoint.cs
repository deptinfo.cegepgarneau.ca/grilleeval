﻿using Application.Interfaces.Services.CommentairesEtudiant;
using Domain.Entities.CommentairesEtudiant;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.CommentairesEtudiant.CreateCommentaireEtudiant
{
    public class CreateCommentaireEtudiantEndpoint : Endpoint<CreateCommentaireEtudiantRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly ICommentaireEtudiantCreationService _CommentaireEtudiantCreationService;

        public CreateCommentaireEtudiantEndpoint(IMapper mapper, ICommentaireEtudiantCreationService CommentaireEtudiantCreationService)
        {
            _mapper = mapper;
            _CommentaireEtudiantCreationService = CommentaireEtudiantCreationService;
        }

        public override void Configure()
        {
            AllowFileUploads();
            DontCatchExceptions();

            Post("CommentaireEtudiant/create");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CreateCommentaireEtudiantRequest req, CancellationToken ct)
        {
            var commentaireEtudiant = _mapper.Map<CommentaireEtudiant>(req);

            await _CommentaireEtudiantCreationService.CreateCommentaireEtudiant(commentaireEtudiant, req.ElementEtudiantId);
            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }
    }
}
