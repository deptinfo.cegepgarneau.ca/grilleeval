﻿namespace Web.Features.CommentairesEtudiant.CreateCommentaireEtudiant
{
    public class CreateCommentaireEtudiantRequest
    {
        public string Commentaire { get; set; } = default!;

        public Guid ElementEtudiantId { get; set; } 

    }
}
