using Application.Interfaces.Services.Baremes;
using Application.Interfaces.Services.Elements;
using Domain.Entities.Baremes;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

public class UpdateBaremeEndPoint : Endpoint<UpdateBaremeRequest, SucceededOrNotResponse>
{
	public readonly IMapper _mapper;
	public readonly IBaremeCreationService _baremeCreationService;

	public UpdateBaremeEndPoint(IMapper mapper, IBaremeCreationService baremeCreationService)
	{
		_mapper = mapper;
		_baremeCreationService = baremeCreationService;
	}

	public override void Configure()
	{
		AllowFileUploads();
		DontCatchExceptions();

		Put("bareme/{id}");
		AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
	}

	public override async Task HandleAsync(UpdateBaremeRequest req, CancellationToken ct)
	{
		var bareme = _mapper.Map<Bareme>(req);

		await _baremeCreationService.UpdateBareme(bareme);
		await SendOkAsync(new SucceededOrNotResponse(true), ct);
	}
}