using Application.Interfaces.Services.Baremes;
using Application.Interfaces.Services.Elements;
using Domain.Entities.Baremes;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

public class CreateBaremeEndPoint: Endpoint<CreateBaremeRequest, SucceededOrNotResponse>
{
	public readonly IMapper _mapper;
	public readonly IBaremeCreationService _baremeCreationService;

	public CreateBaremeEndPoint(IMapper mapper, IBaremeCreationService baremeCreationService)
	{
		_mapper = mapper;
		_baremeCreationService = baremeCreationService;
	}

	public override void Configure()
	{
		AllowFileUploads();
		DontCatchExceptions();

		Post("bareme/create");
		AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
	}

	public override async Task HandleAsync(CreateBaremeRequest req, CancellationToken ct)
	{
		var bareme = _mapper.Map<Bareme>(req);

		await _baremeCreationService.CreateBareme(bareme, req.ElementId);
		await SendOkAsync(new SucceededOrNotResponse(true), ct);
	}
}