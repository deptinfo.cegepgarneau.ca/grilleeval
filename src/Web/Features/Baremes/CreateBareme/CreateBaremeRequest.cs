
public class CreateBaremeRequest
{
	public Guid ElementId { get; set; }
	public string Titre { get; set; } = default!;
	public string Description { get; set; } = default!;
	public int? MinValue { get; set; } = null;
	public int MaxValue { get; set; } = default!;

}