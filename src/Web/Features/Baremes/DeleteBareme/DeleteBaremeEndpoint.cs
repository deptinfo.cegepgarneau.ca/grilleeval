using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Web.Features.Baremes.DeleteBareme;

public class DeleteBaremeEndPoint : Endpoint<DeleteBaremeRequest, EmptyResponse>
{
	private readonly IBaremeRepository _baremeRepository;

	public DeleteBaremeEndPoint(IBaremeRepository baremeRepository)
	{
		_baremeRepository = baremeRepository;
	}

	public override void Configure()
	{
		DontCatchExceptions();

		Delete("bareme/{id}");

		AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
	}

	public override async Task HandleAsync(DeleteBaremeRequest req, CancellationToken ct)
	{
		await _baremeRepository.DeleteBareme(req.Id);
		await SendNoContentAsync(ct);
	}
}