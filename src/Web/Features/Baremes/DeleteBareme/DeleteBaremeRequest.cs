namespace Web.Features.Baremes.DeleteBareme;

public class DeleteBaremeRequest
{
	public Guid Id { get; set; }
}