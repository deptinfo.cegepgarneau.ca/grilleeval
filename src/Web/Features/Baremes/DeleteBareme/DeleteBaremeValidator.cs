using FastEndpoints;
using FluentValidation;

namespace Web.Features.Baremes.DeleteBareme;

public class DeleteBaremeValidator : Validator<DeleteBaremeRequest>
{
	public DeleteBaremeValidator()
	{
		RuleFor(x => x.Id)
			.NotEqual(Guid.Empty)
			.WithErrorCode("EmptyBaremeId")
			.WithMessage("Bareme id is required");
	}
}