using FastEndpoints;
using FluentValidation;

namespace Web.Features.Criteres.DeleteCritere;

public class DeleteCritereValidator : Validator<DeleteCritereRequest>
{
	public DeleteCritereValidator()
	{
		RuleFor(x => x.Id)
			.NotEqual(Guid.Empty)
			.WithErrorCode("EmptyCritereId")
			.WithMessage("Critere id is required");
	}
}