using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Web.Features.Criteres.DeleteCritere;

public class DeleteCritereEndPoint : Endpoint<DeleteCritereRequest, EmptyResponse>
{
	private readonly ICritereRepository _critereRepository;

	public DeleteCritereEndPoint(ICritereRepository critereRepository)
	{
		_critereRepository = critereRepository;
	}

	public override void Configure()
	{
		DontCatchExceptions();

		Delete("critere/{id}");

		AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
	}

	public override async Task HandleAsync(DeleteCritereRequest req, CancellationToken ct)
	{
		await _critereRepository.DeleteCritere(req.Id);
		await SendNoContentAsync(ct);
	}
}