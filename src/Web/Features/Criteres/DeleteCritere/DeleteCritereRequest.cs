namespace Web.Features.Criteres.DeleteCritere;

public class DeleteCritereRequest
{
	public Guid Id { get; set; }
}