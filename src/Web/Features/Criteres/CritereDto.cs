﻿namespace Web.Features.Criteres
{
    public class CritereDto
    {
        public Guid Id { get; set; }

        public Guid ElementId { get; set; }

        public string Detail { get; set; } = default!;
    }
}
