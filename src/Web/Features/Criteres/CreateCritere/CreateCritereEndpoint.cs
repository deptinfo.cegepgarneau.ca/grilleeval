using Application.Interfaces.Services.Criteres;
using Domain.Entities.Criteres;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

public class CreateCritereEndPoint: Endpoint<CreateCritereRequest, SucceededOrNotResponse>
{
	public readonly IMapper _mapper;
	public readonly ICritereCreationService _critereCreationService;

	public CreateCritereEndPoint(IMapper mapper, ICritereCreationService critereCreationService)
	{
		_mapper = mapper;
		_critereCreationService = critereCreationService;
	}

	public override void Configure()
	{
		AllowFileUploads();
		DontCatchExceptions();

		Post("critere/create");
		AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
	}

	public override async Task HandleAsync(CreateCritereRequest req, CancellationToken ct)
	{
		var critere = _mapper.Map<Critere>(req);

		await _critereCreationService.CreateCritere(critere, req.ElementId);
		await SendOkAsync(new SucceededOrNotResponse(true), ct);
	}
}