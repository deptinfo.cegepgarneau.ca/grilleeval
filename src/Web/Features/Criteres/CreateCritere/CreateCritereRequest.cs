
public class CreateCritereRequest
{
	public Guid ElementId { get; set; }
	public string Detail { get; set; } = default!;
}