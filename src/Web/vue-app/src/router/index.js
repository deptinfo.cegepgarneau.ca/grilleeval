import i18n from "@/i18n";
import { Role } from "@/types";
import { useMemberStore } from "@/stores/memberStore";
import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Admin from "../views/admin/Admin.vue";
import AdminBookIndex from "@/views/admin/AdminBookIndex.vue";
import AdminAddBookForm from "@/views/admin/AdminAddBookForm.vue";
import AdminEditBookForm from "@/views/admin/AdminEditBookForm.vue";
import CreationCours from "@/views/cours/CreationCours.vue";
import ConsulterCours from "@/views/cours/ConsulterCours.vue";
import DetailsCours from "@/views/cours/DetailsCours.vue";
import ModifierCours from "@/views/cours/ModifierCours.vue";
import Grille from "../views/Grille/Grille.vue";
import GrilleConsultation from "@/views/Grille/GrilleConsultation.vue";
import UtiliserGrille from '@/views/ElementEtudiant/ElementEtudiantView.vue';
import AjouterGrille from "@/views/creation-grille/GrilleView.vue";
import AideView from "@/views/aide/AideView.vue";
import ConsulterMembres from "@/views/admin/ConsulterMembres.vue";
import TestExp from "@/views/etudiants/ExportationEtudiantView.vue";
import FormCreateAccount from "../views/creationCompte/creationCompte.vue";
/*import ConsulationEvaluation from "../views/evaluation/Consultation.vue";*/
const router = createRouter({
    // eslint-disable-next-line
    scrollBehavior(to, from, savedPosition) {
        // always scroll to top
        return { top: 0 };
    },
    history: createWebHistory(),
    routes: [
        {
            path: i18n.t("routes.home.path"),
            name: "home",
            component: Home,
        },
        {
            path: i18n.t("routes.grille.path"),
            name: "Grille",
            component: Grille,
            meta: {
                requiredRole: Role.Admin,
                noLinkInBreadcrumbs: false,
            },
            children: [
                {
                    path: i18n.t("routes.grille.children.grilles.path"),
                    name: "grille.children.grilles",
                    component: GrilleConsultation,
                },
                {
                    path: i18n.t("routes.grille.children.creation-grille.path"),
                    name: "grille.children.creation-grille",
                    component: AjouterGrille,
                },
                {
                    path: i18n.t("routes.grille.children.utiliserGrille.path"),
                    name: "grille.children.utiliserGrille",
                    component: UtiliserGrille
                },
            ],
        },
        {
            path: i18n.t("routes.cours.children.cours.details.path"),
            name: "cours.children.cours.details",
            component: DetailsCours,
        },
        {
            path: i18n.t("routes.cours.children.cours.edit.path"),
            name: "routes.cours.children.cours.edit.path",
            component: ModifierCours,
        },
        {
            path: i18n.t('routes.cours.children.cours.grilles.modifier.path'),
            name: 'cours.children.cours.grilles.modifier',
            component: AjouterGrille,
        },
        {
            path: i18n.t("routes.admin.path"),
            name: "admin",
            component: Admin,
            meta: {
                requiredRole: Role.Admin,
                noLinkInBreadcrumbs: true,
            },
            children: [
                {
                    path: i18n.t("routes.admin.children.books.path"),
                    name: "admin.children.books",
                    component: Admin,
                    children: [
                        {
                            path: "",
                            name: "admin.children.books.index",
                            component: AdminBookIndex,
                        },
                        {
                            path: i18n.t("routes.admin.children.books.add.path"),
                            name: "admin.children.books.add",
                            component: AdminAddBookForm,
                        },
                        {
                            path: i18n.t("routes.admin.children.books.edit.path"),
                            alias: i18n.t("routes.admin.children.books.edit.path"),
                            name: "admin.children.books.edit",
                            component: AdminEditBookForm,
                            props: true,
                        },
                    ],
                },
                {
                    path: i18n.t("routes.admin.children.creation-compte.fullPath"),
                    name: "admin.children.creation-compte",
                    component: FormCreateAccount,
                },
                {
                    path: i18n.t("routes.admin.children.membres.fullPath"),
                    name: "admin.children.membres",
                    component: ConsulterMembres,
                }
            ],
        },
        {
            path: i18n.t("routes.cours.path"),
            name: "cours",
            meta: {
                noLinkInBreadcrumbs: true,
            },
            children: [
                {
                    path: i18n.t("routes.cours.children.cours.consult.path"),
                    name: "cours.children.cours.consult",
                    component: ConsulterCours,
                },
                {
                    path: i18n.t("routes.cours.children.cours.add.path"),
                    name: "cours.children.cours.add",
                    component: CreationCours,
                },
            ],
        },
        {
            path: i18n.t("routes.aide.path"),
            name: "routes.aide",
            component: AideView,
        },
        {
            path: "/fr/testExp",
            name: "testExp",
            component: TestExp,
        },
    ],
});
// eslint-disable-next-line
router.beforeEach(async (to, from) => {
    const memberStore = useMemberStore();
    if (!to.meta.requiredRole)
        return;
    const isRoleArray = Array.isArray(to.meta.requiredRole);
    const doesNotHaveGivenRole = !isRoleArray && !memberStore.hasRole(to.meta.requiredRole);
    const hasNoRoleAmountRoleList = isRoleArray &&
        !memberStore.hasOneOfTheseRoles(to.meta.requiredRole);
    if (doesNotHaveGivenRole || hasNoRoleAmountRoleList) {
        return {
            name: "home",
        };
    }
});
export const Router = router;
//# sourceMappingURL=index.js.map