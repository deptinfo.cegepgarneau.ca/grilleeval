import { Container } from "inversify";
import axios from "axios";
import "reflect-metadata";
import { TYPES } from "@/injection/types";
import { ApiService, BookService, MemberService, UtilisateurService } from "@/services";
import { CoursService } from "@/services/coursService";
import { GrilleService } from "./services/grilleService";
import { EtudiantService } from "@/services/etudiantService";
import { EtudiantCoursService } from "@/services/etudiantCoursService";
import { CommentaireEtudiantService } from "@/services/commentaireEtudiantService";
import { ElementEtudiantService } from "@/services/elementEtudiantService";
import { CritereService } from "@/services/critereService";
const dependencyInjection = new Container();
dependencyInjection
    .bind(TYPES.AxiosInstance)
    .toConstantValue(axios.create());
dependencyInjection
    .bind(TYPES.IApiService)
    .to(ApiService)
    .inSingletonScope();
dependencyInjection
    .bind(TYPES.IMemberService)
    .to(MemberService)
    .inSingletonScope();
dependencyInjection
    .bind(TYPES.IBookService)
    .to(BookService)
    .inSingletonScope();
dependencyInjection
    .bind(TYPES.ICoursService)
    .to(CoursService)
    .inSingletonScope();
dependencyInjection
    .bind(TYPES.IGrilleService)
    .to(GrilleService)
    .inSingletonScope();
dependencyInjection
    .bind(TYPES.IEtudiantService)
    .to(EtudiantService)
    .inSingletonScope();
dependencyInjection
    .bind(TYPES.IEtudiantCoursService)
    .to(EtudiantCoursService)
    .inSingletonScope();
dependencyInjection
    .bind(TYPES.IElementEtudiantService)
    .to(ElementEtudiantService)
    .inSingletonScope();
dependencyInjection
    .bind(TYPES.ICommentaireEtudiantService)
    .to(CommentaireEtudiantService)
    .inSingletonScope();
dependencyInjection
    .bind(TYPES.ICritereService)
    .to(CritereService)
    .inSingletonScope();
dependencyInjection.bind(TYPES.IUtilisateurService).to(UtilisateurService).inSingletonScope();
function useMemberService() {
    return dependencyInjection.get(TYPES.IMemberService);
}
function useBookService() {
    return dependencyInjection.get(TYPES.IBookService);
}
function useCoursService() {
    return dependencyInjection.get(TYPES.ICoursService);
}
function useGrilleService() {
    return dependencyInjection.get(TYPES.IGrilleService);
}
function useEtudiantService() {
    return dependencyInjection.get(TYPES.IEtudiantService);
}
function useEtudiantCoursService() {
    return dependencyInjection.get(TYPES.IEtudiantCoursService);
}
function useElementEtudiantService() {
    return dependencyInjection.get(TYPES.IElementEtudiantService);
}
function useCommentaireEtudiantService() {
    return dependencyInjection.get(TYPES.ICommentaireEtudiantService);
}
function useUtilisateurService() {
    return dependencyInjection.get(TYPES.IUtilisateurService);
}
function useCritereService() {
    return dependencyInjection.get(TYPES.ICritereService);
}
export { dependencyInjection, useMemberService, useCoursService, useBookService, useGrilleService, useEtudiantService, useEtudiantCoursService, useCommentaireEtudiantService, useElementEtudiantService, useUtilisateurService, useCritereService };
//# sourceMappingURL=inversify.config.js.map