export function validate(value, rules) {
    for (const rule of rules) {
        const result = rule(value);
        if (!result.valid) {
            return result;
        }
    }
    return {
        valid: true,
    };
}
export function validateBoolean(value, rules) {
    for (const rule of rules) {
        const result = rule(value);
        if (!result.valid) {
            return result;
        }
    }
    return {
        valid: true,
    };
}
export function validateArray(value, rules) {
    for (const rule of rules) {
        const result = rule(value);
        if (!result.valid) {
            return result;
        }
    }
    return {
        valid: true,
    };
}
//# sourceMappingURL=index.js.map