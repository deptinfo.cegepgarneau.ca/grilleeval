import i18n from "@/i18n";
export const isFutureDate = (value) => {
    const startDate = new Date(value);
    startDate.setUTCMinutes(startDate.getTimezoneOffset());
    startDate.setHours(0, 0, 0, 0);
    const todayDate = new Date();
    todayDate.setHours(0, 0, 0, 0);
    const result = startDate >= todayDate;
    return {
        valid: result,
        message: result ? undefined : i18n.t("validation.futureDate"),
    };
};
export function minDate(min) {
    return function (value) {
        const result = value > min;
        return {
            valid: result,
            message: result
                ? undefined
                : i18n.t('validation.min').replace("{min}", min.toString())
        };
    };
}
export function min(min) {
    return function (value) {
        const result = Boolean(parseInt(value) >= min);
        return {
            valid: result,
            message: result
                ? undefined
                : i18n.t('validation.min').replace("{min}", min.toString()),
        };
    };
}
export function max(max) {
    return function (value) {
        const result = Boolean(parseInt(value) <= max);
        return {
            valid: result,
            message: result
                ? undefined
                : i18n.t('validation.max').toString().replace("{max}", max.toString()),
        };
    };
}
export const required = (value) => {
    const result = value == undefined ? false : Boolean(value.toString());
    return {
        valid: result,
        message: result ? undefined : i18n.t('validation.empty').toString()
    };
};
export const requiredTextEditor = (value) => {
    const result = Boolean(value !== "" && value !== "<p><br></p>");
    return {
        valid: result,
        message: result ? undefined : i18n.t("validation.empty").toString(),
    };
};
export const requiredBoolean = (value) => {
    return {
        valid: value,
        message: value ? undefined : i18n.t('validation.checked').toString()
    };
};
export const requiredArray = (array) => {
    const result = array != null && array.length > 0;
    return {
        valid: result,
        message: result ? undefined : i18n.t('validation.empty').toString()
    };
};
export const mustMatchZipCodeFormat = (value) => {
    const zipCodeRegex = new RegExp(/^[ABCEGHJ-NPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ -]?\d[ABCEGHJ-NPRSTV-Z]\d$/im);
    const valid = zipCodeRegex.test(value);
    return {
        valid: valid,
        message: valid ? undefined : i18n.t('validation.zipCode').toString()
    };
};
export const mustMatchPhoneNumberFormat = (value) => {
    const phoneNumberRegex = new RegExp(/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/);
    const valid = phoneNumberRegex.test(value);
    return {
        valid: valid,
        message: valid ? undefined : i18n.t('validation.phoneNumber').toString()
    };
};
export const mustMatchEmailFormat = (value) => {
    const emailRegex = new RegExp(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    const valid = emailRegex.test(value);
    return {
        valid: valid,
        message: valid ? undefined : i18n.t('validation.email').toString()
    };
};
//# sourceMappingURL=rules.js.map