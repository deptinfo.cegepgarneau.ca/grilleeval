import i18n from "@/i18n";
export async function getFileFromUrl(url, name, defaultType = 'image/jpeg') {
    const response = await fetch(url);
    const data = await response.blob();
    return new File([data], name, {
        type: data.type || defaultType,
    });
}
export function getValueForLocale(translatableString) {
    if (i18n.getLocale() === "fr")
        return translatableString?.fr ?? "";
    return translatableString?.en ?? "";
}
//# sourceMappingURL=helpers.js.map