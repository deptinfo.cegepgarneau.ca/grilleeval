// eslint-disable-next-line @typescript-eslint/no-empty-interface

import { SucceededOrNotResponse } from "@/types/responses";
import { Cours } from "@/types/entities/cours";
import { ICreateCoursRequest } from "../types/requests/createCoursRequest";
import { IEditCoursRequest } from "../types/requests/editCoursRequest";
import {Book, IAuthenticatedMember, Member} from "@/types/entities";
import { Grille } from "../types/entities/grille";
import { Grille as GrilleCreation } from "@/types/entities/creation-grille/Grille";
import { Etudiant } from "@/types/entities/etudiant";
import { Element } from "@/types/entities/creation-grille/Element";
import { Bareme } from "@/types/entities/creation-grille/Bareme";
import { Critere } from "@/types/entities/creation-grille/Critere";
import { CommentaireGlobal } from "@/types/entities/creation-grille/Commentaire";
import { CommentaireEtudiant } from "@/types/entities/commentaireEtudiant";
import { EtudiantCours } from "@/types/entities/etudiantCours";
import { ICreateAllEtudiantsRequest } from "@/types/requests/createAllEtudiantsRequest";
import { ICreateGrilleRequest } from "../types/requests/createGrilleRequest";
import { IEditCommentaireEtudiantRequest } from "../types/requests/editCommentaireEtudiantRequest";
import { ICreateElementEtudiantRequest } from "../types/requests/createElementEtudiantRequest";
import { IEditElementEtudiantRequest} from "../types/requests/editElementEtudiantRequest";
import { ICreateCommentaireEtudiantRequest } from "../types/requests/createCommentaireEtudiantRequest";
import { ElementEtudiant } from "../types/entities/elementEtudiant";
import { ICreateEtudiantRequest } from "../types/requests/createEtudiantRequest";
import { ICreateAllEtudiantsCoursRequest } from "../types/requests/createAllEtudiantsCoursRequest";
import { ICreateElementRequest } from "../types/requests/createElementRequest";
import { IUpdateElementRequest } from "@/types/requests/updateElementRequest";
import { ICreateBaremeRequest } from "@/types/requests/createBaremeRequest";


import {
  ICreateBookRequest,
  IEditBookRequest,
  ICreateUtilisateurRequest,
  ICreateMemberRequest
} from "@/types/requests"
import { IUpdateBaremeRequest } from "../types/requests/updateBaremeRequest";
import { ICreateCritereRequest } from "../types/requests/createCritereRequest";
import { ICreateCommentaireGlobalRequest } from "../types/requests/createCommentaireGlobalRequest";
import { IUpdateGrilleRequest } from "../types/requests/updateGrilleRequest";
import {ICopyGrilleRequest} from "@/types/requests/copyGrilleRequest";

export interface IApiService {
  headersWithJsonContentType(): any;

  headersWithFormDataContentType(): any;

  buildEmptyBody(): string;
}

export interface IMemberService {
    getCurrentMember(): Promise<IAuthenticatedMember | undefined>
    getAllMembers(): Promise<Member[]>;
    deleteMember(memberId: string): Promise<SucceededOrNotResponse>;
    createMember(request: ICreateMemberRequest): Promise<SucceededOrNotResponse>
}

export interface IBookService {
  getAllBooks(): Promise<Book[]>;

  getBook(bookId: string): Promise<Book>;

  deleteBook(bookId: string): Promise<SucceededOrNotResponse>;

  createBook(request: ICreateBookRequest): Promise<SucceededOrNotResponse>;

  editBook(request: IEditBookRequest): Promise<SucceededOrNotResponse>;
}

export interface ICoursService {
  getAllCours(): Promise<Cours[]>;

  getCours(coursId: string): Promise<Cours>;

  createCours(request: ICreateCoursRequest): Promise<SucceededOrNotResponse>;

  deleteCours(coursId: string): Promise<SucceededOrNotResponse>;

  editCours(request: IEditCoursRequest): Promise<SucceededOrNotResponse>;
    getAllCoursByMember(idMember: string): Promise<Cours[]>;
}

export interface IEtudiantService {
  getAllEtudiants(): Promise<Etudiant[]>;
  createEtudiant(
    request: ICreateEtudiantRequest
  ): Promise<SucceededOrNotResponse>;
  getEtudiant(id: string): Promise<Etudiant>;
  createAllEtudiants(
    request: ICreateAllEtudiantsRequest,coursId :string
  ): Promise<SucceededOrNotResponse>;
}
export interface IEtudiantCoursService {
  getAllGetAllEtudiantsByIdCours(idCours: string): Promise<EtudiantCours[]>;

  createAllEtudiantsCours(
    request: ICreateAllEtudiantsCoursRequest
    ): Promise<SucceededOrNotResponse>;

  DeleteEtudiantCoursWithId(idEtudiantCours: string): Promise<SucceededOrNotResponse>;
  
  deleteStudentsWithCoursId(coursId: string): Promise<SucceededOrNotResponse>;
}


export interface IGrilleService {
  getAllGrilles(): Promise<Grille[]>;

  getGrille(grilleId: string): Promise<Grille>;

  getGrilleWithCoursId(coursId: string): Promise<Grille[]>;

  deleteGrille(grilleId: string): Promise<SucceededOrNotResponse>;

    createGrille(request: ICreateGrilleRequest): Promise<SucceededOrNotResponse>

    UpdateGrille(request: IUpdateGrilleRequest): Promise<SucceededOrNotResponse>
    copyGrille(request: ICopyGrilleRequest): Promise<SucceededOrNotResponse>

}

export interface IUtilisateurService {
    createUtilisateur(request: ICreateUtilisateurRequest): Promise<SucceededOrNotResponse>
}


export interface IElementService {
    CreateElement(element: ICreateElementRequest): Promise<SucceededOrNotResponse>;
    DeleteElement(elementId: string): Promise<SucceededOrNotResponse>;
    UpdateElement(element: IUpdateElementRequest): Promise<SucceededOrNotResponse>;
}

export interface ICritereService {
    getAllCriteresByIdElement(idElement: string): Promise<Critere[]>;
    CreateCritere(critere: ICreateCritereRequest): Promise<SucceededOrNotResponse>;
    DeleteCritere(critereId: string): Promise<SucceededOrNotResponse>;
}

export interface IBaremeService {
    getAllBaremesByIdElement(idElement: string): Promise<Bareme[]>;
    CreateBareme(bareme: ICreateBaremeRequest): Promise<SucceededOrNotResponse>
    UpdateBareme(bareme: IUpdateBaremeRequest): Promise<SucceededOrNotResponse>
    DeleteBareme(baremeId: string): Promise<SucceededOrNotResponse>
}

export interface ICommentairesGlobauxService {
    getAllCommentairesGlobauxByIdElement(idElement: string): Promise<CommentaireGlobal[]>;
    CreateCommentaireGlobal(comment: ICreateCommentaireGlobalRequest): Promise<SucceededOrNotResponse>;
    DeleteCommentaireGlobal(commentId: string): Promise<SucceededOrNotResponse>;
}

export interface ICommentairesEtudiantService {

    getCommentairesEtudiant(): Promise<CommentaireEtudiant[]>;

    getCommentairesEtudiantByIdEtudiant(EtudiantId: string): Promise<CommentaireEtudiant[]>;

    getCommentaireEtudiant(CommentaireEtudiantId: string): Promise<CommentaireEtudiant>;


    createCommentaireEtudiant(request: ICreateCommentaireEtudiantRequest): Promise<SucceededOrNotResponse>;

    editCommentaireEtudiant(request: IEditCommentaireEtudiantRequest): Promise<SucceededOrNotResponse>;

    deleteCommentaireEtudiant(CommentaireEtudiantId: string): Promise<SucceededOrNotResponse>;
}



export interface IElementsEtudiantService {

    getElementsEtudiant(): Promise<ElementEtudiant[]>;

    getElementEtudiant(elementEtudiantId: string): Promise<ElementEtudiant>;

    getElementEtudiantByElementId(elementId: string): Promise<ElementEtudiant>;

    createElementEtudiant(request: ICreateElementEtudiantRequest): Promise<SucceededOrNotResponse>;

    editElementEtudiant(request: IEditElementEtudiantRequest): Promise<SucceededOrNotResponse>;

    deleteElementEtudiant(elementEtudiantId: string): Promise<SucceededOrNotResponse>;
}



