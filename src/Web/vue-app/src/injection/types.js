export const TYPES = {
    IApiService: Symbol.for("IApiService"),
    IMemberService: Symbol.for("IMemberService"),
    IBookService: Symbol.for("IBookService"),
    ICoursService: Symbol.for("ICoursService"),
    AxiosInstance: Symbol.for("AxiosInstance"),
    IGrilleService: Symbol.for("IGrilleService"),
    ICritereService: Symbol.for("ICritereService"),
    IEtudiantService: Symbol.for("IEtudiantService"),
    IEtudiantCoursService: Symbol.for("IEtudiantCoursService"),
    IUtilisateurService: Symbol.for("IUtilisateurService"),
    ICommentaireEtudiantService: Symbol.for("ICommentaireEtudiantService"),
    IElementEtudiantService: Symbol.for("IElementEtudiantService"),
};
//# sourceMappingURL=types.js.map