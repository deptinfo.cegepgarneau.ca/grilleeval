import i18n from "@/i18n";
import '@/extensions/string.extensions';
export class SucceededOrNotResponse {
    errors = [];
    succeeded;
    constructor(succeeded, errors) {
        this.succeeded = succeeded;
        if (errors)
            this.errors = errors;
    }
    getErrorMessages(translationLocation, fallbackKey) {
        const errorMessages = [];
        this.errors.forEach(error => {
            const errorKey = error.errorType.makeFirstLetterLowercase();
            const errorMessage = i18n.t(`${translationLocation}.${errorKey}`);
            if (fallbackKey)
                errorMessages.push(errorMessage ? errorMessage : i18n.t(fallbackKey));
            else
                errorMessages.push(errorMessage ? errorMessage : i18n.t('validation.errorOccured'));
        });
        return errorMessages;
    }
}
//# sourceMappingURL=succeededOrNotResponse.js.map