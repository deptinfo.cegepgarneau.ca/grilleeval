﻿import { Etudiant } from "./etudiant";
//import { ElementEvalue } from "./Element";

import { Grille } from "./grille";
import { CommentaireEtudiant } from "@/types/entities/commentaireEtudiant";


export class ElementEtudiant {
    etudiant?: Etudiant[];
    noteElement?: number;
    date?: Date;
    elementEvalue?: Element;
    commentairesEtudiant?: CommentaireEtudiant[];

}