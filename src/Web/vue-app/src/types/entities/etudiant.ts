import { Element } from "./creation-grille/Element";
import { ElementEtudiant } from "./elementEtudiant";
export class Etudiant {
    id?: string
   da?: string;
    nom?: string;
    prenom?: string;
    nomComplet?: string;
    groupe?: number;
    listeElement?: Element[];
    listeElementEtudiant?: ElementEtudiant[];

    constructor(id?: string, da?: string, nom?: string, prenom?: string, groupe?: number) {
        this.id = id;
        this.da = da;
        this.nom = nom;
        this.prenom = prenom;
        this.groupe = groupe;
        this.nomComplet = `${da} ${nom} ${prenom}`;
        this.listeElement = []
        
    }
}