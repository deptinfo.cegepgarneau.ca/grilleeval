import { Product } from "@/types/entities/product";
export class Book extends Product {
    isbn;
    author;
    editor;
    yearOfPublication;
    numberOfPages;
}
//# sourceMappingURL=book.js.map