export class Etudiant {
    id;
    da;
    nom;
    prenom;
    nomComplet;
    listeElement;
    listeElementEtudiant;
    constructor(id, da, nom, prenom) {
        this.id = id;
        this.da = da;
        this.nom = nom;
        this.prenom = prenom;
        this.nomComplet = `${da} ${nom} ${prenom}`;
        this.listeElement = [];
    }
}
//# sourceMappingURL=etudiant.js.map