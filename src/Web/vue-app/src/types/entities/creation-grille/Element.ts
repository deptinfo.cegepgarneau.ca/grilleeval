import { CommentaireGlobal } from './Commentaire';
import { Critere } from './Critere';
import { Bareme } from './Bareme';

export class Element {
    id?: string;
    titre?: string; 
    ponderation?: number; //Ponderation de l'element
    critere?: Critere[]; //Ajouter objet Critere
    commentaire?: CommentaireGlobal[]; //Ajouter objet Commentaires
    bareme?: Bareme[]; //Creer class Bareme (title, content, value)
}