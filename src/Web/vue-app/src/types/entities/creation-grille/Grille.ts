import { Element } from './Element';
import { Echelle } from './Echelle';
import { Guid } from '../../guid';

export class Grille {
    id?: Guid;
    titre?: string;
    element?: Element[];
    coursId?: string; //Use Cours.Code
    ponderation?: number;
}