export class Bareme {
    titre;
    description;
    minValue = null;
    maxValue;
}
/**
 * Model d'un Bareme
 *  _________
 * |         A| title
 * |----------|
 * | etudiant | description
 * | a bien   |
 * | respecter|
 * | ...      |
 * |----------|
 * | 10      0| maxValue - minValue?
 *  ----------
 */ 
//# sourceMappingURL=Bareme.js.map