export class Bareme {
    id?: string;
    elementId?: string;
    titre!: string;
    description!: string;
    minValue?: number|null = null;
    maxValue!: number; 
}

/**
 * Model d'un Bareme
 *  _________
 * |         A| title
 * |----------|
 * | etudiant | description
 * | a bien   |
 * | respecter|
 * | ...      |
 * |----------|
 * | 10      0| maxValue - minValue?
 *  ----------
 */