import { Grille } from "./creation-grille/Grille";
import { Etudiant } from "./etudiant";
import { EtudiantCours } from "./etudiantCours";
import { Member } from "./member";
/*import { Grille } from "./grille";*/

export class Cours {
    id?: string;
    code?: string;
    nomCours?: string;
    etudiantsCours: EtudiantCours[] = [];
    membre?: Member;
    idMembre?: string;
    //listGrilles?: Grille[];
}