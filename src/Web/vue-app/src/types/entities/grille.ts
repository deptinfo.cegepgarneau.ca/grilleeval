﻿import { Cours } from "./cours";
import { Element } from "./creation-grille/Element";
import { ElementEtudiant } from "./elementEtudiant";

export class Grille  {
    id?: string;
    cours: Cours = new Cours();
    titre?: string;
    ponderation?: number;
    element?: Element[];
    listeElementsEtudiants?: ElementEtudiant[];
}