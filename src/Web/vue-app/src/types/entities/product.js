export class Product {
    id;
    nameFr;
    nameEn;
    descriptionFr;
    descriptionEn;
    price;
    cardImage;
    savedCardImage;
    membersOnly;
}
//# sourceMappingURL=product.js.map