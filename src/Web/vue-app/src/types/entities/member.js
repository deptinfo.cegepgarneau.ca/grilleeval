export class Member {
    id;
    crmId;
    firstName;
    lastName;
    jobTitle;
    email;
    da;
    password;
    phoneNumber;
    phoneExtension;
    apartment;
    street;
    city;
    zipCode;
    userId;
    organizationId;
    roles;
}
//# sourceMappingURL=member.js.map