import { Etudiant } from "@/types/entities/etudiant";
import { Cours } from "@/types/entities/cours";

export class EtudiantCours {
    id?: string;
  etudiant: Etudiant = {};
  cours?: Cours;
  groupe?: string;
}
