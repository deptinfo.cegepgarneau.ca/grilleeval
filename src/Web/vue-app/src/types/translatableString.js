import i18n from "@/i18n";
export class TranslatableString {
    fr;
    en;
    constructor(fr, en) {
        this.fr = fr;
        this.en = en;
    }
    get getValueForLocale() {
        if (i18n.getLocale() === "fr")
            return this.fr ?? "";
        return this.en ?? "";
    }
}
//# sourceMappingURL=translatableString.js.map