export class Guid {
    static newGuid() {
        return new Guid("xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (c) => {
            const r = (Math.random() * 16) | 0;
            const v = c == "x" ? r : (r & 0x3) | 0x8;
            return v.toString(16);
        }));
    }
    static get empty() {
        return "00000000-0000-0000-0000-000000000000";
    }
    get empty() {
        return Guid.empty;
    }
    static isValid(str) {
        const validRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
        return validRegex.test(str);
    }
    value = this.empty;
    constructor(value) {
        if (value) {
            if (Guid.isValid(value)) {
                this.value = value;
            }
        }
    }
    toString() {
        return this.value;
    }
    toJSON() {
        return this.value;
    }
}
//# sourceMappingURL=guid.js.map