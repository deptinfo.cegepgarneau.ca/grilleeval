﻿export interface IUpdateBaremeRequest {
    id?: string
    elementId?: string
    titre?: string
    description?: string
    minValue?: number | null
    maxValue?:number
}