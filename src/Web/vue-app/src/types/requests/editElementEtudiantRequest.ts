
import { Element } from "@/types/entities/creation-grille/Element";

import { CommentaireEtudiant } from "@/types/entities/commentaireEtudiant";

import { Etudiant } from "@/types/entities/etudiant"

import { DateTime } from "luxon";


export interface IEditElementEtudiantRequest {
    id?: string;
    elementEvalue?: Element;
    noteElement?: number;
    commentairesEtudiant?: CommentaireEtudiant[];
    etudiantEvalue: Etudiant;
    date?: DateTime;
}
