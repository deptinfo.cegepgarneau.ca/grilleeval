﻿
export interface ICreateBaremeRequest {
    elementId?: string
    titre?: string
    description?: string
    minValue?: number|null
    maxValue?: number
}