import { Member } from "../entities";

export interface ICreateCoursRequest {
    code?: string;
    nomCours?: string;
    membre?: Member;
    idMembre?: string;
}