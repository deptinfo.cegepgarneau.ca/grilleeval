
import { Element } from "@/types/entities/creation-grille/Element";

import { CommentaireEtudiant } from "@/types/entities/commentaireEtudiant";


import { Etudiant } from "@/types/entities/etudiant"



export interface ICreateElementEtudiantRequest {

    ElementsEtudiant: ElementEtudiantSimplifie[];

}


export class ElementEtudiantSimplifie {
    commentairesEtudiant?: string[];
    date?: Date;
    elementEvalue?: string;
    etudiantsEvalues?: string[];
    noteElement?: number;
}