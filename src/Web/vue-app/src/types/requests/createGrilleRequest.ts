import { Element } from '@/types/entities/creation-grille/Element';
export interface ICreateGrilleRequest {
    id?: string,
    titre?: string,
    element?: Element[],
    coursId?: string,
    ponderation?: number
}