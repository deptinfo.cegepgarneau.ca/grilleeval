﻿export interface ICreateCritereRequest
{
    elementId?: string
    detail?: string
}