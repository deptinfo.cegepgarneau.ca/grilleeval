﻿import { Bareme } from '@/types/entities/creation-grille/Bareme';
import { CommentaireGlobal } from '@/types/entities/creation-grille/Commentaire';
import { Critere } from '@/types/entities/creation-grille/Critere';

export interface IUpdateElementRequest {
    id?: string
    grilleId?: string
    titre?: string
    ponderation?: number
    bareme?: Bareme[]
    commentaireGlobal?: CommentaireGlobal[]
    critere?: Critere[]
}