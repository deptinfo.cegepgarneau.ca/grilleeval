export class ICreateMemberRequest {
    FirstName?: string;
    LastName?: string;
    Email?: string;
    Password?: string;
    IsAdmin?: boolean;
}