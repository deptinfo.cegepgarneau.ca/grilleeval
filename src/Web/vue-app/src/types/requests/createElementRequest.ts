﻿import { Bareme } from '@/types/entities/creation-grille/Bareme';
import { CommentaireGlobal } from '@/types/entities/creation-grille/Commentaire';
import { Critere } from '@/types/entities/creation-grille/Critere';

export interface ICreateElementRequest {
    grilleId?: string,
    bareme?: Bareme[],
    commentaire?: CommentaireGlobal[],
    Critere?: Critere[],
    title?: string,
    ponderation?: number,
}