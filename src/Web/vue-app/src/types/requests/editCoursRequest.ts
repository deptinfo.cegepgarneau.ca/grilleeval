export interface IEditCoursRequest {
    id?: string;
    code?: string;
    nomCours?: string;
}