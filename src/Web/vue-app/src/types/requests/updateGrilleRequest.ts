﻿import { Element } from "../entities/creation-grille/Element"

export interface IUpdateGrilleRequest {
    id?: string
    titre?: string
    ponderation?: number
    coursId?: string
    element?: Element[]
}