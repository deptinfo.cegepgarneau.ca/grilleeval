export interface ICreateAllEtudiantsCoursRequest {
    EtudiantsGroupe?: EtudiantGroupe[];
}
export class EtudiantGroupe {
    Groupe?: string;
    EtudiantId?: string;
    CoursId?: string;
}