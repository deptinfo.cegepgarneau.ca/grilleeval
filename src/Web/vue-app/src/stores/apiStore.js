import { defineStore } from 'pinia';
export const useApiStore = defineStore('api', {
    state: () => ({
        needToLogout: false
    }),
    actions: {
        setNeedToLogout(needToLogout) {
            this.needToLogout = needToLogout;
        }
    },
    getters: {
        getNeedToLogout: (state) => state.needToLogout
    }
});
//# sourceMappingURL=apiStore.js.map