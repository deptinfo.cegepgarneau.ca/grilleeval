import { defineStore } from 'pinia';
export const useMemberStore = defineStore('member', {
    state: () => ({
        member: { roles: [] }
    }),
    actions: {
        setMember(member) {
            this.member = member;
        },
        hasRole(role) {
            return this.member.roles.some(x => x === role);
        },
        hasOneOfTheseRoles(roles) {
            return roles.some(x => this.member.roles.includes(x));
        }
    },
    getters: {
        getMember: (state) => state.member,
    },
    persist: {
        storage: sessionStorage
    }
});
//# sourceMappingURL=memberStore.js.map