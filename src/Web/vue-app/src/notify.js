import { notify } from "@kyvg/vue3-notification";
export function notifySuccess(text) {
    notify({
        text: text,
        type: "success",
    });
}
export function notifyError(text) {
    notify({
        text: text,
        type: "error",
    });
}
//# sourceMappingURL=notify.js.map