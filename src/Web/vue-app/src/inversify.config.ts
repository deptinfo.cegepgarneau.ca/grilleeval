import { Container } from "inversify";
import axios, { AxiosInstance } from "axios";
import "reflect-metadata";

import { TYPES } from "@/injection/types";
import {
  IApiService,
  IBookService,
  ICoursService,
  IMemberService,
  IGrilleService,
  IEtudiantService,
  ICommentairesEtudiantService,
  IElementsEtudiantService,
  IEtudiantCoursService,
  ICritereService,
  IUtilisateurService,
  IElementService,
  IBaremeService,
  ICommentairesGlobauxService
} from "@/injection/interfaces";

import {
  ApiService,
  BookService,
  MemberService,
  UtilisateurService

} from "@/services";

import { CoursService } from "@/services/coursService";
import { GrilleService } from "./services/grilleService";
import { EtudiantService } from "@/services/etudiantService";
import { EtudiantCoursService } from "@/services/etudiantCoursService";
import { CommentaireEtudiantService } from "@/services/commentaireEtudiantService";
import { ElementEtudiantService } from "@/services/elementEtudiantService";
import { CritereService } from "@/services/critereService";
import { ElementService } from "@/services/elementService";
import { BaremeService } from "@/services/baremeService";
import { CommentairesGlobauxService } from "@/services/commentairesGlobauxService";


const dependencyInjection = new Container();
dependencyInjection
  .bind<AxiosInstance>(TYPES.AxiosInstance)
  .toConstantValue(axios.create());
dependencyInjection
  .bind<IApiService>(TYPES.IApiService)
  .to(ApiService)
  .inSingletonScope();
dependencyInjection
  .bind<IMemberService>(TYPES.IMemberService)
  .to(MemberService)
  .inSingletonScope();
dependencyInjection
  .bind<IBookService>(TYPES.IBookService)
  .to(BookService)
  .inSingletonScope();
dependencyInjection
  .bind<ICoursService>(TYPES.ICoursService)
  .to(CoursService)
  .inSingletonScope();
dependencyInjection
  .bind<IGrilleService>(TYPES.IGrilleService)
  .to(GrilleService)
  .inSingletonScope();
dependencyInjection
  .bind<IEtudiantService>(TYPES.IEtudiantService)
  .to(EtudiantService)
  .inSingletonScope();
dependencyInjection
  .bind<IEtudiantCoursService>(TYPES.IEtudiantCoursService)
  .to(EtudiantCoursService)
    .inSingletonScope();
dependencyInjection
    .bind<IElementsEtudiantService>(TYPES.IElementEtudiantService)
    .to(ElementEtudiantService)
    .inSingletonScope();
dependencyInjection
    .bind<ICommentairesEtudiantService>(TYPES.ICommentaireEtudiantService)
    .to(CommentaireEtudiantService)
    .inSingletonScope();
dependencyInjection
    .bind<ICritereService>(TYPES.ICritereService)
    .to(CritereService)
    .inSingletonScope();
dependencyInjection.bind<IUtilisateurService>(TYPES.IUtilisateurService).to(UtilisateurService).inSingletonScope();
dependencyInjection.bind<IElementService>(TYPES.IElementService).to(ElementService).inSingletonScope();
dependencyInjection.bind<IBaremeService>(TYPES.IBaremeService).to(BaremeService).inSingletonScope();
dependencyInjection.bind<ICommentairesGlobauxService>(TYPES.ICommentairesGlobauxService).to(CommentairesGlobauxService).inSingletonScope();

function useMemberService() {
  return dependencyInjection.get<IMemberService>(TYPES.IMemberService);
}

function useBookService() {
  return dependencyInjection.get<IBookService>(TYPES.IBookService);
}

function useCoursService() {
  return dependencyInjection.get<ICoursService>(TYPES.ICoursService);
}
function useGrilleService() {
  return dependencyInjection.get<IGrilleService>(TYPES.IGrilleService);
}
function useEtudiantService() {
  return dependencyInjection.get<IEtudiantService>(TYPES.IEtudiantService);
}
function useEtudiantCoursService() {
  return dependencyInjection.get<IEtudiantCoursService>(TYPES.IEtudiantCoursService);
}

function useElementEtudiantService() {
    return dependencyInjection.get<IElementsEtudiantService>(TYPES.IElementEtudiantService);
}

function useCommentaireEtudiantService() {
    return dependencyInjection.get<ICommentairesEtudiantService>(TYPES.ICommentaireEtudiantService);
}

function useUtilisateurService() {
    return dependencyInjection.get<IUtilisateurService>(TYPES.IUtilisateurService);
}

function useCritereService() {
    return dependencyInjection.get<ICritereService>(TYPES.ICritereService);
}

function useElementService() {
    return dependencyInjection.get<IElementService>(TYPES.IElementService);
}

function useBaremeService() {
    return dependencyInjection.get<IBaremeService>(TYPES.IBaremeService);
}

function useCommentaireGlobalService() {
    return dependencyInjection.get<ICommentairesGlobauxService>(TYPES.ICommentairesGlobauxService);
}

export {
  dependencyInjection,
  useMemberService,
  useCoursService,
  useBookService,
  useGrilleService,
  useEtudiantService,
  useEtudiantCoursService,
  useCommentaireEtudiantService,
  useElementEtudiantService,
  useUtilisateurService,
  useCritereService,
  useElementService,
  useBaremeService,
  useCommentaireGlobalService
};