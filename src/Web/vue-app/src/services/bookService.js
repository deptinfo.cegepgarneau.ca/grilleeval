var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
let BookService = class BookService extends ApiService {
    async getAllBooks() {
        const response = await this
            ._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/books`)
            .catch(function (error) {
            return error.response;
        });
        return response.data;
    }
    async getBook(bookId) {
        const response = await this
            ._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/books/${bookId}`)
            .catch(function (error) {
            return error.response;
        });
        return response.data;
    }
    async deleteBook(bookId) {
        const response = await this
            ._httpClient
            .delete(`${process.env.VUE_APP_API_BASE_URL}/api/books/${bookId}`)
            .catch(function (error) {
            return error.response;
        });
        return new SucceededOrNotResponse(response.status === 204);
    }
    async createBook(request) {
        console.log(request);
        const response = await this
            ._httpClient
            .post(`${process.env.VUE_APP_API_BASE_URL}/api/books`, request, this.headersWithFormDataContentType())
            .catch(function (error) {
            return error.response;
        });
        const succeededOrNotResponse = response.data;
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors);
    }
    async editBook(request) {
        const response = await this
            ._httpClient
            .put(`${process.env.VUE_APP_API_BASE_URL}/api/books/${request.id}`, request, this.headersWithFormDataContentType())
            .catch(function (error) {
            return error.response;
        });
        const succeededOrNotResponse = response.data;
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors);
    }
};
BookService = __decorate([
    injectable()
], BookService);
export { BookService };
//# sourceMappingURL=bookService.js.map