var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { injectable } from "inversify";
import "@/extensions/date.extensions";
import { ApiService } from "@/services/apiService";
import { SucceededOrNotResponse } from "../types/responses";
let UtilisateurService = class UtilisateurService extends ApiService {
    async createUtilisateur(request) {
        const response = await this
            ._httpClient
            .post(`${process.env.VUE_APP_API_BASE_URL}/api/utilisateurasd`, request, this.headersWithFormDataContentType())
            .catch(function (error) {
            console.log(error);
            return error.response;
        });
        const succeededOrNotResponse = response.data;
        console.log(response.data);
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors);
    }
};
UtilisateurService = __decorate([
    injectable()
], UtilisateurService);
export { UtilisateurService };
//# sourceMappingURL=utilisateurService.js.map