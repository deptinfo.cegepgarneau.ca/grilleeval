﻿import { IGrilleService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
import { AxiosError, AxiosResponse } from "axios";
import { Grille } from "../types/entities/grille";
import { Grille as GrilleCreation } from "@/types/entities/creation-grille/Grille";
import { ICreateGrilleRequest } from "../types/requests/createGrilleRequest";
import { IUpdateGrilleRequest } from "../types/requests/updateGrilleRequest";
import { ICopyGrilleRequest } from "@/types/requests/copyGrilleRequest";


@injectable()
export class GrilleService extends ApiService implements IGrilleService {
    public async copyGrille(request: ICopyGrilleRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .post<ICreateGrilleRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/grille/copy`,
                request,
                this.headersWithFormDataContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async createGrille(request: ICreateGrilleRequest): Promise<SucceededOrNotResponse> {
        console.log({request})
        const response = await this
            ._httpClient
            .post<ICreateGrilleRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/grille/create`,
                request,
                this.headersWithFormDataContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async getAllGrilles(): Promise<Grille[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Grille[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/grilles`)
            .catch(function (error: AxiosError): AxiosResponse<Grille[]> {
                return error.response as AxiosResponse<Grille[]>
            })
        return response.data as Grille[]
    }

    public async getGrille(grilleId: string): Promise<Grille> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<GrilleCreation>>(`${process.env.VUE_APP_API_BASE_URL}/api/grilles/${grilleId}`)
            .catch(function (error: AxiosError): AxiosResponse<GrilleCreation> {
                return error.response as AxiosResponse<GrilleCreation>;
            });
        return response.data as Grille
    }

    public async getGrilleWithCoursId(coursId: string): Promise<Grille[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Grille[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/grilles/cours/${coursId}`)
            .catch(function (error: AxiosError): AxiosResponse<Grille[]> {
                return error.response as AxiosResponse<Grille[]>
            })
        return response.data as Grille[]
    }
    
    public async deleteGrille(grilleId: string): Promise<SucceededOrNotResponse> {
        console.log(grilleId)
        const response = await this
            ._httpClient
            .delete<AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/grilles/${grilleId}`)
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status === 204)
    }

    public async UpdateGrille(req: IUpdateGrilleRequest): Promise<SucceededOrNotResponse> {
        const response = await this._httpClient
            .put<IUpdateGrilleRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/grille/${req.id}`,
                req,
                this.headersWithFormDataContentType()
            )
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })

        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }
}