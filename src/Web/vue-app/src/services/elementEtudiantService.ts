import {IElementsEtudiantService} from "@/injection/interfaces";
import {injectable} from "inversify";
import {ApiService} from "./apiService";
import {SucceededOrNotResponse} from "@/types/responses";
import {AxiosError, AxiosResponse} from "axios";
import {ICreateElementEtudiantRequest} from "@/types/requests/createElementEtudiantRequest";
import {ElementEtudiant} from "@/types/entities/elementEtudiant";
import {IEditElementEtudiantRequest} from "@/types/requests/editElementEtudiantRequest";

@injectable()
export class ElementEtudiantService extends ApiService implements IElementsEtudiantService {
  public async getElementsEtudiant(): Promise<ElementEtudiant[]> {
    const response = await this
      ._httpClient
      .get<AxiosResponse<ElementEtudiant[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/elementsEtudiant`)
      .catch(function (error: AxiosError): AxiosResponse<ElementEtudiant[]> {
        return error.response as AxiosResponse<ElementEtudiant[]>
      })
    return response.data as ElementEtudiant[]
  }

    public async getElementEtudiantByElementId(elementId: string): Promise<ElementEtudiant> {
    const response = await this
      ._httpClient
      .get<AxiosResponse<ElementEtudiant>>(`${process.env.VUE_APP_API_BASE_URL}/api/elementsEtudiant/${elementId}`)
      .catch(function (error: AxiosError): AxiosResponse<ElementEtudiant> {
        return error.response as AxiosResponse<ElementEtudiant>;
      });
    return response.data as ElementEtudiant
    }

    public async getElementEtudiant(elementEtudiantId: string): Promise<ElementEtudiant> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<ElementEtudiant>>(`${process.env.VUE_APP_API_BASE_URL}/api/elementsEtudiant/${elementEtudiantId}`)
            .catch(function (error: AxiosError): AxiosResponse<ElementEtudiant> {
                return error.response as AxiosResponse<ElementEtudiant>;
            });
        return response.data as ElementEtudiant
    }

  public async deleteElementEtudiant(elementEtudiantId: string): Promise<SucceededOrNotResponse> {
    const response = await this
      ._httpClient
      .delete<AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/elementsEtudiant/${elementEtudiantId}`)
      .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
        return error.response as AxiosResponse<SucceededOrNotResponse>
      })
    return new SucceededOrNotResponse(response.status === 204)
  }

  public async createElementEtudiant(request: ICreateElementEtudiantRequest): Promise<SucceededOrNotResponse> {
    const response = await this
      ._httpClient
      .post<ICreateElementEtudiantRequest, AxiosResponse<any>>(
        `${process.env.VUE_APP_API_BASE_URL}/api/elementsEtudiant`,
          request,
          this.headersWithJsonContentType())
      .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
        return error.response as AxiosResponse<SucceededOrNotResponse>
      })
    const succeededOrNotResponse = response.data as SucceededOrNotResponse
    return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
  }

  public async editElementEtudiant(request: IEditElementEtudiantRequest): Promise<SucceededOrNotResponse> {
    const response = await this
      ._httpClient
      .put<ICreateElementEtudiantRequest, AxiosResponse<any>>(
        `${process.env.VUE_APP_API_BASE_URL}/api/elementsEtudiant/${request.id}`,
        request,
        this.headersWithFormDataContentType())
      .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
        return error.response as AxiosResponse<SucceededOrNotResponse>
      })
    const succeededOrNotResponse = response.data as SucceededOrNotResponse
    return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
  }
}