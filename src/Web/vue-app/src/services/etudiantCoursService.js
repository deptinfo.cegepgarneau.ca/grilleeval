var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { ApiService } from "./apiService";
import { injectable } from "inversify";
import { SucceededOrNotResponse } from "@/types/responses";
let EtudiantCoursService = class EtudiantCoursService extends ApiService {
    async getAllGetAllEtudiantsByIdCours(idCours) {
        const response = await this._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/etudiantsCours/${idCours}`)
            .catch(function (err) {
            return err.response;
        });
        return response.data;
    }
    async createAllEtudiantsCours(request) {
        const response = await this._httpClient
            .post(`${process.env.VUE_APP_API_BASE_URL}/api/etudiantsCours`, request, this.headersWithJsonContentType())
            .catch(function (err) {
            return err.response;
        });
        const succeededOrNotResponse = response.data;
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors);
    }
    async DeleteEtudiantCoursWithId(idEtudiantCours) {
        const response = await this
            ._httpClient
            .delete(`${process.env.VUE_APP_API_BASE_URL}/api/etudiantsCours/${idEtudiantCours}`)
            .catch(function (error) {
            return error.response;
        });
        console.log(response);
        return new SucceededOrNotResponse(response.status === 204);
    }
};
EtudiantCoursService = __decorate([
    injectable()
], EtudiantCoursService);
export { EtudiantCoursService };
//# sourceMappingURL=etudiantCoursService.js.map