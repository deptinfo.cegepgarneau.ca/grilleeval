import { ICritereService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
import { Axios, AxiosError, AxiosResponse } from "axios";
import { Critere } from "../types/entities/creation-grille/Critere";
import { ICreateCritereRequest } from "../types/requests/createCritereRequest";



@injectable()
export class CritereService extends ApiService implements ICritereService {

    public async getAllCriteresByIdElement(
        idElement: string
  ): Promise <Critere[] > {
            const response = await this._httpClient
                .get<AxiosResponse<Critere[]>>(
                    `${process.env.VUE_APP_API_BASE_URL}/api/Critere/${idElement}`
                )
                .catch(function (err: AxiosError): AxiosResponse<Critere[]> {
                    return err.response as AxiosResponse<Critere[]>;
                });
            return response.data as Critere[];


    }
    
    public async CreateCritere(req: ICreateCritereRequest): Promise<SucceededOrNotResponse> {

        const response = await this._httpClient
            .post<ICreateCritereRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/critere/create`,
                req,
                this.headersWithFormDataContentType()
            )
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                  return error.response as AxiosResponse<SucceededOrNotResponse>
            })

        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async DeleteCritere(critereId: string): Promise<SucceededOrNotResponse> {
        const response = await this._httpClient
            .delete<AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/critere/${critereId}`
        )
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status == 204)
    }
}