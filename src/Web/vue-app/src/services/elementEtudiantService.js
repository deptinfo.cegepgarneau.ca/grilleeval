var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
let ElementEtudiantService = class ElementEtudiantService extends ApiService {
    async getElementsEtudiant() {
        const response = await this
            ._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/elementsEtudiant`)
            .catch(function (error) {
            return error.response;
        });
        return response.data;
    }
    async getElementEtudiant(elementEtudiantId) {
        const response = await this
            ._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/elementsEtudiant/${elementEtudiantId}`)
            .catch(function (error) {
            return error.response;
        });
        return response.data;
    }
    async deleteElementEtudiant(elementEtudiantId) {
        const response = await this
            ._httpClient
            .delete(`${process.env.VUE_APP_API_BASE_URL}/api/elementsEtudiant/${elementEtudiantId}`)
            .catch(function (error) {
            return error.response;
        });
        return new SucceededOrNotResponse(response.status === 204);
    }
    async createElementEtudiant(request) {
        const response = await this
            ._httpClient
            .post(`${process.env.VUE_APP_API_BASE_URL}/api/elementsEtudiant`, request, this.headersWithFormDataContentType())
            .catch(function (error) {
            return error.response;
        });
        const succeededOrNotResponse = response.data;
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors);
    }
    async editElementEtudiant(request) {
        const response = await this
            ._httpClient
            .put(`${process.env.VUE_APP_API_BASE_URL}/api/elementsEtudiant/${request.id}`, request, this.headersWithFormDataContentType())
            .catch(function (error) {
            return error.response;
        });
        const succeededOrNotResponse = response.data;
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors);
    }
};
ElementEtudiantService = __decorate([
    injectable()
], ElementEtudiantService);
export { ElementEtudiantService };
//# sourceMappingURL=elementEtudiantService.js.map