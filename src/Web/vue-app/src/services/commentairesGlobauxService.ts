import { ICommentairesGlobauxService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
import { AxiosError, AxiosResponse } from "axios";
import { CommentaireGlobal } from "../types/entities/creation-grille/Commentaire";
import { ICreateCommentaireGlobalRequest } from "../types/requests/createCommentaireGlobalRequest";



@injectable()
export class CommentairesGlobauxService extends ApiService implements ICommentairesGlobauxService {

    public async getAllCommentairesGlobauxByIdElement(
        idElement: string
    ): Promise<CommentaireGlobal[] > {
            const response = await this._httpClient
                .get<AxiosResponse<CommentaireGlobal[]>>(
                    `${process.env.VUE_APP_API_BASE_URL}/api/Commentaire/${idElement}`
                )
                .catch(function (err: AxiosError): AxiosResponse<CommentaireGlobal[]> {
                    return err.response as AxiosResponse<CommentaireGlobal[]>;
                });
        return response.data as CommentaireGlobal[];
    }
    
    public async CreateCommentaireGlobal(req: ICreateCommentaireGlobalRequest): Promise<SucceededOrNotResponse> {

        const response = await this._httpClient
            .post<ICreateCommentaireGlobalRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/commentaireGlobal/create`,
                req,
                this.headersWithFormDataContentType()
            )
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })

        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async DeleteCommentaireGlobal(commentId: string): Promise<SucceededOrNotResponse> {
        const response = await this._httpClient
            .delete<AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/commentaireGlobal/${commentId}`
        )
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status == 204)
    }
}