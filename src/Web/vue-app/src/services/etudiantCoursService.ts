import { AxiosError, AxiosResponse } from "axios";
import { IEtudiantCoursService } from "../injection/interfaces";
import { ApiService } from "./apiService";
import { injectable } from "inversify";
import { EtudiantCours } from "@/types/entities/etudiantCours";
import { SucceededOrNotResponse } from "@/types/responses";
import { ICreateAllEtudiantsCoursRequest } from "@/types/requests/createAllEtudiantsCoursRequest";

@injectable()
export class EtudiantCoursService
  extends ApiService
  implements IEtudiantCoursService
{
  public async getAllGetAllEtudiantsByIdCours(
    idCours: string
  ): Promise<EtudiantCours[]> {
    const response = await this._httpClient
      .get<AxiosResponse<EtudiantCours[]>>(
        `${process.env.VUE_APP_API_BASE_URL}/api/etudiantsCours/${idCours}`
      )
      .catch(function (err: AxiosError): AxiosResponse<EtudiantCours[]> {
        return err.response as AxiosResponse<EtudiantCours[]>;
      });
    return response.data as EtudiantCours[];
  }

  public async createAllEtudiantsCours(
    request: ICreateAllEtudiantsCoursRequest
  ): Promise<SucceededOrNotResponse> {
    const response = await this._httpClient
      .post<ICreateAllEtudiantsCoursRequest, AxiosResponse<any>>(
        `${process.env.VUE_APP_API_BASE_URL}/api/etudiantsCours`,
        request,
        this.headersWithJsonContentType()
      )
      .catch(function (err: AxiosError): AxiosResponse<SucceededOrNotResponse> {
        return err.response as AxiosResponse<SucceededOrNotResponse>;
      });
    const succeededOrNotResponse = response.data as SucceededOrNotResponse;
    return new SucceededOrNotResponse(
      succeededOrNotResponse.succeeded,
      succeededOrNotResponse.errors
    );
  }

  public async DeleteEtudiantCoursWithId(
    idEtudiantCours: string
  ): Promise<SucceededOrNotResponse> {
    const response = await this._httpClient
      .delete<AxiosResponse<any>>(
        `${process.env.VUE_APP_API_BASE_URL}/api/etudiantsCours/${idEtudiantCours}`
      )
      .catch(function (
        error: AxiosError
      ): AxiosResponse<SucceededOrNotResponse> {
        return error.response as AxiosResponse<SucceededOrNotResponse>;
      });
    console.log(response);
    return new SucceededOrNotResponse(response.status === 204);
  }

  public async deleteStudentsWithCoursId(coursId: string): Promise<SucceededOrNotResponse> {
    const response = await this._httpClient
      .delete<AxiosResponse<any>>(
        `${process.env.VUE_APP_API_BASE_URL}/api/cours/${coursId}/deleteStudents`
      )
      .catch(function (
        error: AxiosError
      ): AxiosResponse<SucceededOrNotResponse> {
        return error.response as AxiosResponse<SucceededOrNotResponse>;
      });
    return new SucceededOrNotResponse(response.status === 204);
  }
}
