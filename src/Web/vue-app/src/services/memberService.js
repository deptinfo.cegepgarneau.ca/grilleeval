var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { injectable } from "inversify";
import "@/extensions/date.extensions";
import { ApiService } from "@/services/apiService";
import { SucceededOrNotResponse } from "../types/responses";
let MemberService = class MemberService extends ApiService {
    async getCurrentMember() {
        try {
            const response = await this
                ._httpClient
                .get(`${process.env.VUE_APP_API_BASE_URL}/api/members/me`);
            return response.data;
        }
        catch (error) {
            return Promise.reject(error);
        }
    }
    async getAllMembers() {
        const response = await this
            ._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/members`)
            .catch(function (error) {
            return error.response;
        });
        console.log(response.data);
        return response.data;
    }
    async createMember(request) {
        console.log(request);
        const response = await this
            ._httpClient
            .post(`${process.env.VUE_APP_API_BASE_URL}/api/utilisateur`, request, this.headersWithJsonContentType())
            .catch(function (error) {
            return error.response;
        });
        const succeededOrNotResponse = response.data;
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors);
    }
    async deleteMember(memberId) {
        console.log(memberId);
        const response = await this
            ._httpClient
            .delete(`${process.env.VUE_APP_API_BASE_URL}/api/delete-member/${memberId}`)
            .catch(function (error) {
            return error.response;
        });
        return new SucceededOrNotResponse(response.status === 204);
    }
};
MemberService = __decorate([
    injectable()
], MemberService);
export { MemberService };
//# sourceMappingURL=memberService.js.map