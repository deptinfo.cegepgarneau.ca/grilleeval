import { IBaremeService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
import { AxiosError, AxiosResponse } from "axios";
import { Bareme } from "../types/entities/creation-grille/Bareme";
import { ICreateBaremeRequest } from "@/types/requests/createBaremeRequest";
import { IUpdateBaremeRequest } from "../types/requests/updateBaremeRequest";



@injectable()
export class BaremeService extends ApiService implements IBaremeService {
    public async getAllBaremesByIdElement(
        idElement: string
  ): Promise < Bareme[] > {
            const response = await this._httpClient
                .get<AxiosResponse<Bareme[]>>(
                    `${process.env.VUE_APP_API_BASE_URL}/api/Bareme/${idElement}`
                )
                .catch(function (err: AxiosError): AxiosResponse<Bareme[]> {
                    return err.response as AxiosResponse<Bareme[]>;
                });
            return response.data as Bareme[];


    }

    public async CreateBareme(req: ICreateBaremeRequest): Promise<SucceededOrNotResponse> {

        const response = await this._httpClient
            .post<ICreateBaremeRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/bareme/create`,
                req,
                this.headersWithFormDataContentType()
        )
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }
    
    public async UpdateBareme(req: IUpdateBaremeRequest): Promise<SucceededOrNotResponse> {
        const response = await this._httpClient
            .put<IUpdateBaremeRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/bareme/${req.id}`,
                req,
                this.headersWithFormDataContentType()
        )
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async DeleteBareme(baremeId: string): Promise<SucceededOrNotResponse> {
        const response = await this._httpClient
            .delete<AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/bareme/${baremeId}`
        )
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status == 204)
    }
}