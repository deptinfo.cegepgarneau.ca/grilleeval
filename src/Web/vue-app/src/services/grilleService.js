var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
let GrilleService = class GrilleService extends ApiService {
    async createGrille(request) {
        console.log("GrilleService request: ", request);
        const response = await this
            ._httpClient
            .post(`${process.env.VUE_APP_API_BASE_URL}/api/grille/create`, request, this.headersWithFormDataContentType())
            .catch(function (error) {
            return error.response;
        });
        const succeededOrNotResponse = response.data;
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors);
    }
    async getAllGrilles() {
        const response = await this
            ._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/grilles`)
            .catch(function (error) {
            return error.response;
        });
        return response.data;
    }
    async getGrille(grilleId) {
        const response = await this
            ._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/grilles/${grilleId}`)
            .catch(function (error) {
            return error.response;
        });
        return response.data;
    }
    async getGrilleWithCoursId(coursId) {
        const response = await this
            ._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/grilles/cours/${coursId}`)
            .catch(function (error) {
            return error.response;
        });
        return response.data;
    }
    async deleteGrille(grilleId) {
        const response = await this
            ._httpClient
            .delete(`${process.env.VUE_APP_API_BASE_URL}/api/grilles/${grilleId}`)
            .catch(function (error) {
            return error.response;
        });
        return new SucceededOrNotResponse(response.status === 204);
    }
};
GrilleService = __decorate([
    injectable()
], GrilleService);
export { GrilleService };
//# sourceMappingURL=grilleService.js.map