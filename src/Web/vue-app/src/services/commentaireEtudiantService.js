var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
let CommentaireEtudiantService = class CommentaireEtudiantService extends ApiService {
    getCommentairesEtudiant() {
        throw new Error("Method not implemented.");
    }
    createCommentaireEtudiant(request) {
        throw new Error("Method not implemented.");
    }
    async getCommentairesEtudiantByIdEtudiant(etudiantId) {
        const response = await this
            ._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/commentairesEtudiant/${etudiantId}`)
            .catch(function (error) {
            return error.response;
        });
        return response.data;
    }
    async getCommentaireEtudiant(commentaireEtudiantId) {
        const response = await this
            ._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/commentairesEtudiant/${commentaireEtudiantId}`)
            .catch(function (error) {
            return error.response;
        });
        return response.data;
    }
    async editCommentaireEtudiant(request) {
        throw new Error("Method not implemented.");
    }
    async deleteCommentaireEtudiant(commentaireEtudiantId) {
        const response = await this
            ._httpClient
            .delete(`${process.env.VUE_APP_API_BASE_URL}/api/commentairesEtudiant/${commentaireEtudiantId}`)
            .catch(function (error) {
            return error.response;
        });
        return new SucceededOrNotResponse(response.status === 204);
    }
};
CommentaireEtudiantService = __decorate([
    injectable()
], CommentaireEtudiantService);
export { CommentaireEtudiantService };
//# sourceMappingURL=commentaireEtudiantService.js.map