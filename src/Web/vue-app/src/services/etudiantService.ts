import { AxiosError, AxiosResponse } from "axios";
import { IEtudiantService } from "../injection/interfaces";
import { Etudiant } from "../types/entities/etudiant";
import { ApiService } from "./apiService";
import { injectable } from "inversify";
import { ICreateAllEtudiantsRequest } from "@/types/requests/createAllEtudiantsRequest";
import { SucceededOrNotResponse } from "@/types/responses";
import { ICreateEtudiantRequest } from "@/types/requests/createEtudiantRequest";

@injectable()
export class EtudiantService extends ApiService implements IEtudiantService {

    public async getAllEtudiants(): Promise<Etudiant[]> {
        const response = await this._httpClient
            .get<AxiosResponse<Etudiant[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/etudiants`)
            .catch(function (err: AxiosError): AxiosResponse<Etudiant[]> {
                return err.response as AxiosResponse<Etudiant[]>
            })
        return response.data as Etudiant[]
    }

    public async getEtudiant(id: string): Promise<Etudiant> {
        const response = await this._httpClient
            .get<AxiosResponse<Etudiant>>(`${process.env.VUE_APP_API_BASE_URL}/api/etudiants/${id}`)
            .catch(
                function (err: AxiosError): AxiosResponse<Etudiant> {

                    return err.response as AxiosResponse<Etudiant>

                }
        )
        return response.data as Etudiant
    }
    public async createAllEtudiants(request: ICreateAllEtudiantsRequest,courId :string): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .post<ICreateAllEtudiantsRequest, AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/${courId}/etudiants`,
                request,
                this.headersWithJsonContentType())
            .catch(function (err: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return err.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }
    public async createEtudiant(request: ICreateEtudiantRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .post<ICreateEtudiantRequest, AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/etudiant`,
                request,
                this.headersWithFormDataContentType())
            .catch(function (err: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return err.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }
}