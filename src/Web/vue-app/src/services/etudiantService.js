var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { ApiService } from "./apiService";
import { injectable } from "inversify";
import { SucceededOrNotResponse } from "@/types/responses";
let EtudiantService = class EtudiantService extends ApiService {
    async getAllEtudiants() {
        const response = await this._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/etudiants`)
            .catch(function (err) {
            return err.response;
        });
        return response.data;
    }
    async getEtudiant(id) {
        const response = await this._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/etudiants/${id}`)
            .catch(function (err) {
            return err.response;
        });
        return response.data;
    }
    async createAllEtudiants(request, courId) {
        const response = await this
            ._httpClient
            .post(`${process.env.VUE_APP_API_BASE_URL}/api/${courId}/etudiants`, request, this.headersWithJsonContentType())
            .catch(function (err) {
            return err.response;
        });
        const succeededOrNotResponse = response.data;
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors);
    }
    async createEtudiant(request) {
        const response = await this
            ._httpClient
            .post(`${process.env.VUE_APP_API_BASE_URL}/api/etudiant`, request, this.headersWithFormDataContentType())
            .catch(function (err) {
            return err.response;
        });
        const succeededOrNotResponse = response.data;
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors);
    }
};
EtudiantService = __decorate([
    injectable()
], EtudiantService);
export { EtudiantService };
//# sourceMappingURL=etudiantService.js.map