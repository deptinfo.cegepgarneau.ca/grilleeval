import {AxiosError, AxiosResponse} from "axios"
import {injectable} from "inversify"

import "@/extensions/date.extensions"
import {ApiService} from "@/services/apiService"
import {IMemberService} from "@/injection/interfaces"
import { IAuthenticatedMember } from "@/types/entities/authenticatedMember"
import { ICreateMemberRequest } from "../types/requests/createMemberRequest"
import { SucceededOrNotResponse } from "../types/responses"
import {Book, Guid, Member} from "@/types";


@injectable()
export class MemberService extends ApiService implements IMemberService {
    public async getCurrentMember(): Promise<IAuthenticatedMember | undefined> {
        try {
            const response = await this
                ._httpClient
                .get<IAuthenticatedMember, AxiosResponse<IAuthenticatedMember>>(`${process.env.VUE_APP_API_BASE_URL}/api/members/me`)
            return response.data
        } catch (error) {
            return Promise.reject(error)
        }
    }
    public async getAllMembers(): Promise<Member[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Member[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/members`)
            .catch(function (error: AxiosError): AxiosResponse<Member[]> {
                return error.response as AxiosResponse<Member[]>
            })
        console.log(response.data)
        return response.data as Member[]
    }
    public async createMember(request: ICreateMemberRequest): Promise<SucceededOrNotResponse> {
        console.log(request)
        const response = await this
            ._httpClient
            .post<ICreateMemberRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/utilisateur`,
                request,
                this.headersWithJsonContentType())
            .catch(function (error: any): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })

        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }
    
    public async deleteMember(memberId: string): Promise<SucceededOrNotResponse> {
        console.log(memberId)
        const response = await this
            ._httpClient
            .delete<AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/delete-member/${memberId}`)
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status === 204)
    }
}


