var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { SucceededOrNotResponse } from "../types/responses";
import { ApiService } from "./apiService";
import { injectable } from "inversify";
let CoursService = class CoursService extends ApiService {
    async getAllCours() {
        const response = await this._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/cours`)
            .catch(function (err) {
            return err.response;
        });
        return response.data;
    }
    async getAllCoursByMember(idMember) {
        console.log("Hello");
        const response = await this._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/coursMember/${idMember}`)
            .catch(function (err) {
            return err.response;
        });
        return response.data;
    }
    async getCours(coursId) {
        const response = await this._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/cours/${coursId}`)
            .catch(function (err) {
            return err.response;
        });
        return response.data;
    }
    async createCours(request) {
        console.log(request);
        const response = await this
            ._httpClient
            .post(`${process.env.VUE_APP_API_BASE_URL}/api/cours`, request, this.headersWithFormDataContentType())
            .catch(function (err) {
            return err.response;
        });
        const succeededOrNotResponse = response.data;
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors);
    }
    async deleteCours(coursId) {
        const response = await this
            ._httpClient
            .delete(`${process.env.VUE_APP_API_BASE_URL}/api/cours/${coursId}`)
            .catch(function (err) {
            return err.response;
        });
        return new SucceededOrNotResponse(response.status == 204);
    }
    async editCours(request) {
        const response = await this
            ._httpClient
            .put(`${process.env.VUE_APP_API_BASE_URL}/api/cours/${request.id}`, request, this.headersWithFormDataContentType())
            .catch(function (err) {
            return err.response;
        });
        const succededOrNotResponse = response.data;
        return new SucceededOrNotResponse(succededOrNotResponse.succeeded, succededOrNotResponse.errors);
    }
};
CoursService = __decorate([
    injectable()
], CoursService);
export { CoursService };
//# sourceMappingURL=coursService.js.map