import {AxiosError, AxiosResponse} from "axios"
import {injectable} from "inversify"

import "@/extensions/date.extensions"
import {ApiService} from "@/services/apiService"
import {IUtilisateurService} from "@/injection/interfaces"
import { IAuthenticatedMember } from "@/types/entities/authenticatedMember"
import { ICreateUtilisateurRequest } from "../types/requests"
import { SucceededOrNotResponse } from "../types/responses"


@injectable()
export class UtilisateurService extends ApiService implements IUtilisateurService {
    public async createUtilisateur(request: ICreateUtilisateurRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .post<ICreateUtilisateurRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/utilisateurasd`,
                request,
                this.headersWithFormDataContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                console.log(error)
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        console.log(response.data)
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }
}