var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { injectable } from "inversify";
import { ApiService } from "./apiService";
let CommentairesGlobauxService = class CommentairesGlobauxService extends ApiService {
    async getAllCommentairesGlobauxByIdElement(idElement) {
        const response = await this._httpClient
            .get(`${process.env.VUE_APP_API_BASE_URL}/api/Commentaire/${idElement}`)
            .catch(function (err) {
            return err.response;
        });
        return response.data;
    }
};
CommentairesGlobauxService = __decorate([
    injectable()
], CommentairesGlobauxService);
export { CommentairesGlobauxService };
//# sourceMappingURL=commentairesGlobauxService.js.map