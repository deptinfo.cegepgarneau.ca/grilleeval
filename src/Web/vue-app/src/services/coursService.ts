import { Axios, AxiosError, AxiosResponse } from "axios";
import { ICoursService } from "../injection/interfaces";
import { Cours } from "../types/entities/cours";
import { ICreateCoursRequest } from "../types/requests/createCoursRequest";
import { IEditCoursRequest } from "../types/requests/editCoursRequest";
import { SucceededOrNotResponse } from "../types/responses";
import { ApiService } from "./apiService";
import { injectable } from "inversify";
import { th } from "element-plus/es/locale";


@injectable()
export class CoursService extends ApiService implements ICoursService {
    public async getAllCours(): Promise<Cours[]> {
        
        const response = await this._httpClient
            .get<AxiosResponse<Cours[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/cours`)
            .catch(function (err: AxiosError): AxiosResponse<Cours[]> {
                return err.response as AxiosResponse<Cours[]>
            })
        return response.data as Cours[]
    }
    public async getAllCoursByMember(idMember: string): Promise<Cours[]> {
        
        const response = await this._httpClient
            .get<AxiosResponse<Cours[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/coursMember/${idMember}`)
            .catch(function (err: AxiosError): AxiosResponse<Cours[]> {
                return err.response as AxiosResponse<Cours[]>
            })
        console.log(response.data)
        return response.data as Cours[]
    }
    public async getCours(coursId: string): Promise<Cours> {
        const response = await this._httpClient
            .get<AxiosResponse<Cours>>(`${process.env.VUE_APP_API_BASE_URL}/api/cours/${coursId}`)
            .catch(
                function (err: AxiosError): AxiosResponse<Cours> {

                    return err.response as AxiosResponse<Cours>

                }
        )
        return response.data as Cours
    }

    public async createCours(request: ICreateCoursRequest): Promise<SucceededOrNotResponse> {
        console.log(request)
        const response = await this
            ._httpClient
            .post<ICreateCoursRequest, AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/cours`,
                request,
                this.headersWithFormDataContentType())
            .catch(function (err: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return err.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async deleteCours(coursId: string): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .delete<AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/cours/${coursId}`)
            .catch(function (err: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return err.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status == 204)
    }

    public async editCours(request: IEditCoursRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .put<IEditCoursRequest,AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/cours/${request.id}`,
                request,
                this.headersWithFormDataContentType())
            .catch(function (err: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return err.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succededOrNotResponse.succeeded, succededOrNotResponse.errors)
    }

}