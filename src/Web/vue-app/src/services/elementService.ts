import { IElementService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
import { AxiosError, AxiosResponse } from "axios";
import { Element } from "../types/entities/creation-grille/Element";
import { ICreateElementRequest } from "../types/requests/createElementRequest";
import { IUpdateElementRequest } from "../types/requests/updateElementRequest";



@injectable()
export class ElementService extends ApiService implements IElementService {


    public async CreateElement(request: ICreateElementRequest): Promise<SucceededOrNotResponse> {

        const response = await this._httpClient
            .post<ICreateElementRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/element/create`,
                request,
                this.headersWithFormDataContentType()
            )
            .catch(function (err: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return err.response as AxiosResponse<SucceededOrNotResponse>;
            });

        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async UpdateElement(request: IUpdateElementRequest): Promise<SucceededOrNotResponse> {

        const response = await this._httpClient
            .put<IUpdateElementRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/element/${request.id}`,
                request,
                this.headersWithFormDataContentType()
        )
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })

        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }


    public async DeleteElement(elementId: string): Promise<SucceededOrNotResponse> {
        const response = await this._httpClient
            .delete<AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/element/${elementId}`
            )
            .catch(function (err: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return err.response as AxiosResponse<SucceededOrNotResponse>;
            });

        return new SucceededOrNotResponse(response.status === 204)
    }

}