﻿import { ICommentairesEtudiantService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
import { AxiosError, AxiosResponse } from "axios";
import { CommentaireEtudiant } from "../types/entities/commentaireEtudiant";
import { ICreateBookRequest } from "../types/requests";
import { ICreateCommentaireEtudiantRequest } from "../types/requests/createCommentaireEtudiantRequest";
import { IEditElementEtudiantRequest } from "../types/requests/editElementEtudiantRequest";
import { IEditCommentaireEtudiantRequest } from "../types/requests/editCommentaireEtudiantRequest";


@injectable()
export class CommentaireEtudiantService extends ApiService implements ICommentairesEtudiantService {

  

    getCommentairesEtudiant(): Promise<CommentaireEtudiant[]> {
        throw new Error("Method not implemented.");
    }


    createCommentaireEtudiant(request: ICreateCommentaireEtudiantRequest): Promise<SucceededOrNotResponse> {
        throw new Error("Method not implemented.");
    }



    public async getCommentairesEtudiantByIdEtudiant(etudiantId: string): Promise<CommentaireEtudiant[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<CommentaireEtudiant[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/commentairesEtudiant/${etudiantId}`)
            .catch(function (error: AxiosError): AxiosResponse<CommentaireEtudiant[]> {
                return error.response as AxiosResponse<CommentaireEtudiant[]>
            })
        return response.data as CommentaireEtudiant[]
    }


    public async getCommentaireEtudiant(commentaireEtudiantId: string): Promise<CommentaireEtudiant> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<CommentaireEtudiant>>(`${process.env.VUE_APP_API_BASE_URL}/api/commentairesEtudiant/${commentaireEtudiantId}`)
            .catch(function (error: AxiosError): AxiosResponse<CommentaireEtudiant> {
                return error.response as AxiosResponse<CommentaireEtudiant>;
            });
        return response.data as CommentaireEtudiant
    }

    public async editCommentaireEtudiant(request: IEditCommentaireEtudiantRequest): Promise<SucceededOrNotResponse> {
        throw new Error("Method not implemented.");
    }



    public async deleteCommentaireEtudiant(commentaireEtudiantId: string): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .delete<AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/commentairesEtudiant/${commentaireEtudiantId}`)
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status === 204)
    }

   
}