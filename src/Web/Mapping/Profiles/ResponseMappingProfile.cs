using Application.Common;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Books;
using Domain.Entities.CoursEtudiants;
using Domain.Entities.DossierCours;
using Domain.Entities.ElementsEtudiant;
using Domain.Entities.Etudiants;
using Domain.Entities.Grilles;
using Microsoft.AspNetCore.Identity;
using Web.Features.Admins.Books;
using Web.Features.Common;
using Web.Features.Members;
using Web.Features.Etudiants;
using Web.Features.EtudiantsCours;
using Web.Features.Grilles;
using Web.Features.Members.Me.GetMe;
using Web.Features.Utilisateur.Cours;
using Web.Features.ElementsEtudiant;
using Web.Features.CommentairesEtudiant;
using Domain.Entities.CommentairesEtudiant;
using Web.Features.CommentairesGlobaux;
using Domain.Entities.CommentaireGlobaux;
using Domain.Entities.Criteres;
using Web.Features.Criteres;
using Domain.Entities.Baremes;
using Web.Features.Baremes;
using Domain.Entities.Elements;
using Web.Features.Elements;

namespace Web.Mapping.Profiles;

public class ResponseMappingProfile : Profile
{
    public ResponseMappingProfile()
    {
        CreateMap<IdentityResult, SucceededOrNotResponse>();

        CreateMap<IdentityError, Error>()
            .ForMember(error => error.ErrorType, opt => opt.MapFrom(identity => identity.Code))
            .ForMember(error => error.ErrorMessage, opt => opt.MapFrom(identity => identity.Description));

        CreateMap<Member, GetMeResponse>()
            .ForMember(x => x.Roles, opt => opt.MapFrom(x => x.User.RoleNames))
            .ForMember(x => x.PhoneNumber, opt => opt.MapFrom(x => x.GetPhoneNumber()))
            .ForMember(x => x.PhoneExtension, opt => opt.MapFrom(x => x.GetPhoneExtension()));

        CreateMap<Member, MemberDto>()
            .ForMember(memberDto => memberDto.FirstName, opt => opt.MapFrom(member => member.FirstName))
            .ForMember(memberDto => memberDto.LastName, opt => opt.MapFrom(member => member.LastName))
            .ForMember(memberDto => memberDto.Email, opt => opt.MapFrom(member => member.Email))
            .ForMember(memberDto => memberDto.Roles, opt => opt.MapFrom(member => member.User.RoleNames))
            .ForMember(memberDto => memberDto.IsAdmin, opt => opt.MapFrom(member => member.IsAdmin));
        
        CreateMap<Book, BookDto>()
            .ForMember(bookDto => bookDto.Created, opt => opt.MapFrom(book => book.Created.ToDateTimeUtc()))
            .ForMember(bookDto => bookDto.NameFr, opt => opt.MapFrom(book => book.NameFr))
            .ForMember(bookDto => bookDto.NameEn, opt => opt.MapFrom(book => book.NameEn))
            .ForMember(bookDto => bookDto.DescriptionFr, opt => opt.MapFrom(book => book.DescriptionFr))
            .ForMember(bookDto => bookDto.DescriptionEn, opt => opt.MapFrom(book => book.DescriptionEn));
        CreateMap<Cours, CoursDto>()
            .ForMember(coursDto => coursDto.Created, opt => opt.MapFrom(cours => cours.Created.ToDateTimeUtc()))
            .ForMember(coursDto => coursDto.Code, opt => opt.MapFrom(cours => cours.Code))
            .ForMember(coursDto => coursDto.NomCours, opt => opt.MapFrom(cours => cours.NomCours))
            .ForMember(coursDto => coursDto.listEtudiants, opt => opt.MapFrom(cours => cours.EtudiantsCours));
        
        CreateMap<Grille, GrilleDto>()
           .ForMember(grilleDto => grilleDto.Titre, opt => opt.MapFrom(grille => grille.Titre))
           .ForMember(grilleDto => grilleDto.Ponderation, opt => opt.MapFrom(grille => grille.Ponderation))
           .ForMember(grilleDto => grilleDto.Element, opt => opt.MapFrom(grille => grille.Element))
           .ForMember(grilleDto => grilleDto.Cours, opt => opt.MapFrom(grille => grille.Cours))
           .ForMember(grilleDto => grilleDto.CoursId, opt => opt.MapFrom(grille => grille.Cours.Id))
           .ForMember(grilleDto => grilleDto.ListeElementsEtudiants, opt => opt.MapFrom(grille => grille.ListeElementsEtudiants));

        CreateMap<EtudiantCours, EtudiantCoursDto>()
              .ForMember(EtudiantCoursDto => EtudiantCoursDto.Created, opt => opt.MapFrom
              (etudiantCours => etudiantCours.Created.ToDateTimeUtc()))
              .ForMember(EtudiantCoursDto => EtudiantCoursDto.CoursId, opt => opt.MapFrom
              (etudiantCours => etudiantCours.Cours.Id))
              .ForMember(EtudiantCoursDto => EtudiantCoursDto.EtudiantId, opt => opt.MapFrom
              (etudiantCours => etudiantCours.Etudiant.Id))
              .ForMember(EtudiantCoursDto => EtudiantCoursDto.Groupe, opt => opt.MapFrom
              (etudiantCours => etudiantCours.Groupe));
              
        CreateMap<EtudiantDto, Etudiant>();

        CreateMap<ElementEtudiant, ElementEtudiantDto>()
             .ForMember(ElementEtudiantDto => ElementEtudiantDto.Id, opt => opt.MapFrom(ElementEtudiant => ElementEtudiant.Id))
             .ForMember(ElementEtudiantDto => ElementEtudiantDto.NoteElement, opt => opt.MapFrom(ElementEtudiant => ElementEtudiant.NoteElement))
             .ForMember(ElementEtudiantDto => ElementEtudiantDto.ElementEvalue, opt => opt.MapFrom(ElementEtudiant => ElementEtudiant.ElementEvalue))
             .ForMember(ElementEtudiantDto => ElementEtudiantDto.CommentairesEtudiant, opt => opt.MapFrom(ElementEtudiant => ElementEtudiant.CommentairesEtudiant))
             .ForMember(ElementEtudiantDto => ElementEtudiantDto.Date, opt => opt.MapFrom(ElementEtudiant => ElementEtudiant.Date));

        CreateMap<CommentaireEtudiant, CommentaireEtudiantDto>()
            .ForMember(CommentaireEtudiantDto => CommentaireEtudiantDto.Id, opt => opt.MapFrom(CommentaireEtudiant => CommentaireEtudiant.Id))
            .ForMember(CommentaireEtudiantDto => CommentaireEtudiantDto.Commentaire, opt => opt.MapFrom(CommentaireEtudiant => CommentaireEtudiant.Commentaire));


        CreateMap<CommentaireGlobal, CommentairesGlobauxDto>()
          .ForMember(CommentairesGlobauxDto => CommentairesGlobauxDto.Id, opt => opt.MapFrom(CommentaireGlobal => CommentaireGlobal.Id))
          .ForMember(CommentairesGlobauxDto => CommentairesGlobauxDto.Commentaire, opt => opt.MapFrom(CommentaireGlobal => CommentaireGlobal.Commentaire));


        CreateMap<Critere, CritereDto>()
          .ForMember(CritereDto => CritereDto.Id, opt => opt.MapFrom(Critere => Critere.Id))
          .ForMember(CritereDto => CritereDto.Detail, opt => opt.MapFrom(Critere => Critere.Detail));

        CreateMap<Bareme, BaremeDto>()
           .ForMember(BaremeDto => BaremeDto.Id, opt => opt.MapFrom(Bareme => Bareme.Id))
           .ForMember(BaremeDto => BaremeDto.Titre, opt => opt.MapFrom(Bareme => Bareme.Titre))
           .ForMember(BaremeDto => BaremeDto.MinValue, opt => opt.MapFrom(Bareme => Bareme.MinValue))
           .ForMember(BaremeDto => BaremeDto.MaxValue, opt => opt.MapFrom(Bareme => Bareme.MaxValue));

        CreateMap<Element, ElementDto>()
         .ForMember(ElementDto => ElementDto.Id, opt => opt.MapFrom(Element => Element.Id))
         .ForMember(ElementDto => ElementDto.Titre, opt => opt.MapFrom(Element => Element.Titre))
         .ForMember(ElementDto => ElementDto.Ponderation, opt => opt.MapFrom(Element => Element.Ponderation))
         .ForMember(ElementDto => ElementDto.Criteres, opt => opt.MapFrom(Element => Element.Critere))
         .ForMember(ElementDto => ElementDto.Baremes, opt => opt.MapFrom(Element => Element.Bareme))
         .ForMember(ElementDto => ElementDto.CommentairesGlobaux, opt => opt.MapFrom(Element => Element.Commentaire));
		 //.ForMember(ElementDto => ElementDto.GrilleId, opt => opt.MapFrom(Element => Element.Grille.Id));


    }
}