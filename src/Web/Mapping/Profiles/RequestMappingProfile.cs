using AutoMapper;
using Domain.Common;
using Domain.Entities.Books;
using Domain.Entities.DossierCours;
using Domain.Entities.Etudiants;
using Domain.Entities.CoursEtudiants;
using Web.Dtos;
using Web.Features.Admins.Books.CreateBook;
using Web.Features.Admins.Books.EditBook;
using Web.Features.Etudiants.CreateAllEtudiants;
using Web.Features.Utilisateur.Cours.CreateCours;
using Web.Features.Utilisateur.Cours.EditCours;
using Domain.Entities.Grilles;
using Web.Features.Etudiants.CreateEtudiant;
using Web.Features.Members.CreateMember;
using Domain.Entities;
using Web.Features.EtudiantsCours.DeleteEtudiantCours;
using Domain.Entities.Elements;
using Web.Features.Grilles.GetGrille;
using Domain.Entities.Baremes;
using Domain.Entities.Criteres;
using Domain.Entities.CommentaireGlobaux;
using Web.Features.CommentairesEtudiant.CreateCommentaireEtudiant;
using Domain.Entities.CommentairesEtudiant;
using Web.Features.ElementsEtudiant.CreateElementsEtudiant;
using Domain.Entities.ElementsEtudiant;
using Application.Services.ElementsEtudiant;

namespace Web.Mapping.Profiles;

public class RequestMappingProfile : Profile
{
    public RequestMappingProfile()
    {
        CreateMap<TranslatableStringDto, TranslatableString>().ReverseMap();

        CreateMap<CreateBookRequest, Book>();

        CreateMap<EditBookRequest, Book>();

        CreateMap<CreateMemberRequest, Member>();


        CreateMap<CreateCoursRequest, Cours>()
            .ForMember(opt => opt.EtudiantsCours, opt => opt.Ignore())
            .ForMember(opt => opt.Membre, opt => opt.MapFrom(x => x.Membre)) ;
        CreateMap<EditCoursRequest, Cours>().ForMember(opt => opt.EtudiantsCours, opt => opt.Ignore());
        
        //CreateMap<DeleteEtudiantCoursByIdRequest,EtudiantCours>();

        CreateMap<CreateAllEtudiantsRequest, Etudiant>().ForMember(opt => opt.EtudiantsCours, opt => opt.Ignore());

        CreateMap<CreateGrilleRequest, Grille>().ForMember(opt => opt.Element, opt => opt.Ignore());
        CreateMap<UpdateGrilleRequest, Grille>().ForMember(opt => opt.Element, opt => opt.Ignore());

        CreateMap<UpdateElementRequest, Element>();

        CreateMap<CreateAllEtudiantsRequest, List<Etudiant>>();
        CreateMap<CreateEtudiantRequest, Etudiant>().ForMember(opt => opt.EtudiantsCours, opt => opt.Ignore());

        CreateMap<CreateElementRequest, Element>()
            .ForMember(opt => opt.Bareme, opt => opt.Ignore())
            .ForMember(opt => opt.Commentaire, opt => opt.Ignore())
            .ForMember(opt => opt.Critere, opt => opt.Ignore());

        CreateMap<CreateBaremeRequest, Bareme>();
        CreateMap<UpdateBaremeRequest, Bareme>();

        CreateMap<CreateCritereRequest, Critere>();

        CreateMap<CreateCommentaireGlobalRequest, CommentaireGlobal>();

        CreateMap<CreateElementsEtudiantRequest, ElementEtudiantSimplifie>();   
        CreateMap<CreateCommentaireEtudiantRequest, CommentaireEtudiant>();
    }
}