﻿using System.Reflection;
using Application.Interfaces.Services.Books;
using Application.Interfaces.Services.Cours;
using Application.Interfaces.Services.Grilles;
using Application.Interfaces.Services.Etudiants;
using Application.Interfaces.Services.EtudiantsCours;
using Application.Interfaces.Services.Members;
using Application.Interfaces.Services.Notifications;
using Application.Interfaces.Services.Users;
using Application.Interfaces.Services.Utilisateurs;
using Application.Services.Books;
using Application.Services.Cours;
using Application.Services.Grilles;
using Application.Services.Etudiants;
using Application.Services.EtudiantsCours;
using Application.Services.Members;
using Application.Services.Notifications;
using Application.Services.Users;
using Application.Settings;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Slugify;
using Application.Interfaces.Services.Elements;
using Application.Services.Elements;
using Application.Interfaces.Services.Baremes;
using Application.Services.Baremes;
using Application.Interfaces.Services.Criteres;
using Application.Services.Criteres;
using Application.Interfaces.Services.CommentairesGlobaux;
using Application.Services.CommentairesGlobaux;
using Application.Interfaces.Services.CommentairesEtudiant;
using Application.Services.CommentairesEtudiant;
using Application.Interfaces.Services.ElementsEtudiant;
using Application.Services.ElementsEtudiants;

namespace Application;

public static class ConfigureServices
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddMediatR(Assembly.GetExecutingAssembly());

        services.Configure<ApplicationSettings>(configuration.GetSection("Application"));

        services.AddScoped<ISlugHelper, SlugHelper>();

        services.AddScoped<IBookCreationService, BookCreationService>();
        services.AddScoped<IBookUpdateService, BookUpdateService>();
        services.AddScoped<ICoursCreationServices, CoursCreationService>();
        services.AddScoped<ICoursUpdateService, CoursUpdateService>();
        services.AddScoped<INotificationService, EmailNotificationService>();
        services.AddScoped<IGrilleCreationService, GrilleCreationService>();

        services.AddScoped<IElementCreationService, ElementCreationService>();
        services.AddScoped<IElementEtudiantCreationService, ElementEtudiantCreationService>();
        services.AddScoped<IBaremeCreationService, BaremeCreationService>();
        services.AddScoped<ICritereCreationService, CritereCreationService>();
        services.AddScoped<ICommentaireGlobalCreationService, CommentaireGlobalService>();
        services.AddScoped<ICommentaireEtudiantCreationService, CommentaireEtudiantCreationService>();


        services.AddScoped<IEtudiantsCreationService,EtudiantsCreationsService>();
        services.AddScoped<IEtudiantsCoursCreationService, EtudiantsCoursCreationService>();
        services.AddScoped<IMemberCreationService, MemberCreationService>();
        services.AddScoped<IAuthenticatedUserService, AuthenticatedUserService>();
        services.AddScoped<IAuthenticatedMemberService, AuthenticatedMemberService>();

        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

        return services;
    }
}