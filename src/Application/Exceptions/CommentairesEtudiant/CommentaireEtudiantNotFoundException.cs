﻿using System.Runtime.Serialization;

namespace Application.Exceptions.CommentairesEtudiant
{
    [Serializable]
    public class CommentaireEtudiantNotFoundException : Exception
    {
        public CommentaireEtudiantNotFoundException()
        {
        }

        public CommentaireEtudiantNotFoundException(string? message) : base(message)
        {
        }

        public CommentaireEtudiantNotFoundException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected CommentaireEtudiantNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}