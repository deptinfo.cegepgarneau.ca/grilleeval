﻿using System.Runtime.Serialization;

namespace Application.Exceptions.CommentairesEtudiant
{
    [Serializable]
    public class CommentaireEtudiantAlreadyExistsException : Exception
    {
        public CommentaireEtudiantAlreadyExistsException()
        {
        }

        public CommentaireEtudiantAlreadyExistsException(string? message) : base(message)
        {
        }

        public CommentaireEtudiantAlreadyExistsException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected CommentaireEtudiantAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}