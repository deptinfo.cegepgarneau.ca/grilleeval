﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions.Grilles
{
	public class GrilleNotFoundException: Exception
	{
		public GrilleNotFoundException(string message): base(message) { }
	}
}
