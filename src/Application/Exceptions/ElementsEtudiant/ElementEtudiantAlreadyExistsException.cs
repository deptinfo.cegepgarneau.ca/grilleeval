﻿using System.Runtime.Serialization;

namespace Application.Exceptions.ElementsEtudiant
{
    [Serializable]
    public class ElementEtudiantAlreadyExistsException : Exception
    {
        public ElementEtudiantAlreadyExistsException()
        {
        }

        public ElementEtudiantAlreadyExistsException(string? message) : base(message)
        {
        }

        public ElementEtudiantAlreadyExistsException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected ElementEtudiantAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}