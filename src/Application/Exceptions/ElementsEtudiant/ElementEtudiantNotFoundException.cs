﻿using System.Runtime.Serialization;

namespace Application.Exceptions.ElementsEtudiant
{
    [Serializable]
    public class ElementEtudiantNotFoundException : Exception
    {
        public ElementEtudiantNotFoundException()
        {
        }

        public ElementEtudiantNotFoundException(string? message) : base(message)
        {
        }

        public ElementEtudiantNotFoundException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected ElementEtudiantNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}