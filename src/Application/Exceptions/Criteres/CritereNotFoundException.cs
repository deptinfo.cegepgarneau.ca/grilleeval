﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions.Criteres
{
	public class CritereNotFoundException : Exception
	{
		public CritereNotFoundException() : base() { }

		public CritereNotFoundException(string message) : base(message) { }
	}
}
