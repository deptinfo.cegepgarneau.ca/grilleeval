using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Exceptions.Etudiants
{
    public class EtudiantNotFoundException : Exception
    {
        public EtudiantNotFoundException(string message) : base(message)
        {
        }
    }
}