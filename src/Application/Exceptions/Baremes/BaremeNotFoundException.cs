﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions.Baremes
{
	public class BaremeNotFoundException : Exception
	{
		public BaremeNotFoundException(): base() { }
		public BaremeNotFoundException(string message) : base(message) { }
	}
}
