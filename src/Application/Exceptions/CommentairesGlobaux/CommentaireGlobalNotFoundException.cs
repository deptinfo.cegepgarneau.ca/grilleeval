﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions.CommentairesGlobaux
{
	public class CommentaireGlobalNotFoundException: Exception
	{
		public CommentaireGlobalNotFoundException(string message) : base(message) { }
		public CommentaireGlobalNotFoundException() : base() { }
	}
}
