﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions.Cours
{
    public class CoursNotFoundException : Exception
    {
        public CoursNotFoundException(string message) : base(message) { }
    }
}
