﻿using Domain.Entities;
using Domain.Entities.Baremes;
using Domain.Entities.Books;
using Domain.Entities.CommentaireGlobaux;
using Domain.Entities.CommentairesEtudiant;
using Domain.Entities.CoursEtudiants;
using Domain.Entities.Criteres;
using Domain.Entities.DossierCours;
using Domain.Entities.Elements;
using Domain.Entities.ElementsEtudiant;
using Domain.Entities.Etudiants;
using Domain.Entities.Grilles;
using Domain.Entities.Identity;
using Domain.Entities.Utilisateur;
using Microsoft.EntityFrameworkCore;

namespace Application.Interfaces;

public interface IGrilleEvalDbContext
{
    DbSet<Member> Members { get; }
    DbSet<Book> Books { get; }
    DbSet<Etudiant> Etudiants { get; }
    DbSet<Cours> Cours { get; }
    DbSet<EtudiantCours> EtudiantsCours {get;}
    DbSet<Grille> Grilles { get; set; }
    DbSet<Element> Elements { get;}
    DbSet<Critere> Criteres { get;}
    DbSet<CommentaireGlobal> CommentairesGlobaux {get;}
    DbSet<Bareme> Baremes { get;}
    DbSet<ElementEtudiant> ElementsEtudiant { get; set; }
    DbSet<CommentaireEtudiant> CommentairesEtudiant { get; set; }
    Task<int> SaveChangesAsync(CancellationToken? cancellationToken = null);
}