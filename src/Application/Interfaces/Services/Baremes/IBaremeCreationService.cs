﻿using Domain.Entities.Baremes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.Baremes
{
	public interface IBaremeCreationService
	{
		public Task CreateBareme(Bareme bareme, Guid elementId);
		public Task DeleteBareme(Guid baremeId);
		public Task UpdateBareme(Bareme bareme);

	}
}
