﻿using Domain.Entities.Criteres;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.Criteres
{
	public interface ICritereCreationService
	{
		Task CreateCritere(Critere critere, Guid elementId);
		Task DeleteCritere(Guid critereId);
	}
}
