﻿using Domain.Entities;

namespace Application.Interfaces.Services.Members
{
    public interface IMemberCreationService
    {
        Task CreateMember(Member member);
        Member ObtenirMember(Guid id); 
    }
}
