﻿using Domain.Entities.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.Elements
{
	public interface IElementCreationService
	{
		public Task CreateElement(Element element, Guid grilleId);
		public Task DeleteElement(Guid elementId);
		public Task UpdateElement(Element element);
	}
}
