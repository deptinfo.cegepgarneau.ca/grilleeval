﻿using Application.Services.ElementsEtudiant;
using Domain.Entities.ElementsEtudiant;
using Domain.Entities.Etudiants;
using Domain.Entities.Grilles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.ElementsEtudiant
{
    public interface IElementEtudiantCreationService
    {
        public Task CreateElementEtudiant(List<ElementEtudiantSimplifie> ElementsEtudiant);
    }
}
