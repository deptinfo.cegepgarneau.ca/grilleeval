﻿using Domain.Entities.Grilles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.Grilles
{
	public interface IGrilleCreationService
	{
		public Task CreateGrille(Grille grille, Guid coursId);
		public Task UpdateGrille(Grille grille);
	}
}
