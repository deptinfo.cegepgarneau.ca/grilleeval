using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities.Etudiants;

namespace Application.Interfaces.Services.Etudiants
{
    public interface IEtudiantsCreationService
    {
        Task CreateEtudiants(List<Etudiant> etudiants);
    }
}