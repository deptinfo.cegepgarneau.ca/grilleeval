﻿using Domain.Entities.CommentaireGlobaux;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.CommentairesGlobaux
{
	public interface ICommentaireGlobalCreationService
	{
		Task CreateCommentaireGlobal(CommentaireGlobal comment, Guid elementId);
		Task DeleteCommentaireGlobal(Guid commentId);
	}
}
