﻿using Domain.Entities.Utilisateur;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.Utilisateurs;

public interface IUtilisateurCreationService
{
    Task CreateUser(Utilisateur utilisateur);
}