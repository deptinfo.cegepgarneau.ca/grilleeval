using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities.CoursEtudiants;
using Domain.Entities.Etudiants;

namespace Application.Interfaces.Services.EtudiantsCours
{
    public interface IEtudiantsCoursCreationService
    {
        Task CreateEtudiantsCours(List<EtudiantCours> etudiantsCours);
    }
}