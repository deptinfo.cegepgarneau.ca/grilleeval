﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.Cours
{
    public interface ICoursUpdateService
    {
        public Task UpdateCours(Domain.Entities.DossierCours.Cours cours);
    }
}
