﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.Cours
{
    public interface ICoursCreationServices
    {
        public Task CreateCours(Domain.Entities.DossierCours.Cours cours);
    }
}
