﻿namespace Application.Interfaces.Services;

public interface IHttpContextUserService
{
    string? UserEmail { get; }
}