﻿using Application.Interfaces.Services.CommentairesGlobaux;
using Domain.Entities.CommentaireGlobaux;
using Domain.Entities.CommentairesEtudiant;
using Domain.Entities.ElementsEtudiant;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.CommentairesEtudiant
{
    public interface ICommentaireEtudiantCreationService
    {
       
            Task CreateCommentaireEtudiant(CommentaireEtudiant commentaireEtudiant, Guid elemenEtudiantId);
            Task DeleteCommentaireEtudiant(Guid commentaireEtudiant);
        


    }
}
