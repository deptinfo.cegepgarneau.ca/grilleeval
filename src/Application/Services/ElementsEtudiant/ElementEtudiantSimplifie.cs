﻿namespace Application.Services.ElementsEtudiant
{
    public record class ElementEtudiantSimplifie(

        List<Guid> commentairesEtudiant,
        DateTime date,
        Guid elementEvalue,
        List<Guid> etudiantsEvalues,
        float noteElement

        );



}
