﻿using Application.Exceptions.ElementsEtudiant;
using Application.Interfaces.Services.ElementsEtudiant;
using Application.Services.ElementsEtudiant;
using Domain.Entities.CommentairesEtudiant;
using Domain.Entities.ElementsEtudiant;
using Domain.Entities.Etudiants;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.ElementsEtudiants
{
	public class ElementEtudiantCreationService: IElementEtudiantCreationService
    {
		private readonly IElementEtudiantRepository _elementEtudiantRepository;
        private readonly IElementRepository _elementRepository;
        private readonly IEtudiantRepository _etudiantRepository;
        private readonly ICommentaireEtudiantRepository _commentaireEtudiantRepository;

		public ElementEtudiantCreationService(IElementEtudiantRepository elementEtudiantRepository, IElementRepository elementRepository, IEtudiantRepository etudiantRepository, ICommentaireEtudiantRepository commentaireEtudiantRepository)
		{
			_elementEtudiantRepository = elementEtudiantRepository;
            _elementRepository = elementRepository;
            _etudiantRepository = etudiantRepository; 
            _commentaireEtudiantRepository = commentaireEtudiantRepository;
		}

        public async Task CreateElementEtudiant(List<ElementEtudiantSimplifie> elementsEtudiant)
        {
            if (elementsEtudiant.Count == 0)
                throw new Exception("Liste d'éléments étudiant est vide.");


            foreach (var elementEtudiantSimplifie in elementsEtudiant)
            {
                var elementEvalue = await _elementRepository.GetById(elementEtudiantSimplifie.elementEvalue);

          

                foreach (var etudiant in elementEtudiantSimplifie.etudiantsEvalues)
                {

                    var etudiantEvalue = await _etudiantRepository.GetById(etudiant);


                    ElementEtudiant elementEtudiant = new ElementEtudiant()
                    {
                        Etudiant = etudiantEvalue,
                        NoteElement = elementEtudiantSimplifie.noteElement,
                        Date = elementEtudiantSimplifie.date,
                        ElementEvalue = elementEvalue,
                        GrilleId = elementEvalue.GrilleId

                    };
                
                     await _elementEtudiantRepository.CreateElementEtudiant(elementEtudiant);

                }

            }
        }
    }
}
