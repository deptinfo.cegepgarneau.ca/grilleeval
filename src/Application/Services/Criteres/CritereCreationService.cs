﻿using Application.Interfaces.Services.Criteres;
using Domain.Entities.Criteres;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Criteres
{
	public class CritereCreationService: ICritereCreationService
	{
		private readonly ICritereRepository _critereRepository;

		public CritereCreationService(ICritereRepository critereRepository)
		{
			_critereRepository = critereRepository;
		}

		public async Task CreateCritere(Critere critere, Guid elementId)
		{
			await _critereRepository.CreateCritere(critere, elementId);
		}

		public async Task DeleteCritere(Guid critereId)
		{
			await _critereRepository.DeleteCritere(critereId);
		}
	}
}
