﻿using Application.Interfaces.Services.Cours;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Cours
{
    public class CoursUpdateService : ICoursUpdateService
    {
        public readonly ICoursRepository _coursRepository;

        public CoursUpdateService(ICoursRepository coursRepository)
        {
            _coursRepository = coursRepository;
        }
        public async Task UpdateCours(Domain.Entities.DossierCours.Cours cours)
        {
            await _coursRepository.UpdateCours(cours);
        }
    }
}
