﻿using Application.Interfaces.Services.Cours;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Cours
{
    public class CoursCreationService : ICoursCreationServices
    {
        public readonly ICoursRepository _coursRepository;

        public CoursCreationService(ICoursRepository coursRepository)
        {
            _coursRepository = coursRepository;
        }
        public async Task CreateCours(Domain.Entities.DossierCours.Cours cours)
        {
            await _coursRepository.CreateCours(cours);
        }
    }
}
