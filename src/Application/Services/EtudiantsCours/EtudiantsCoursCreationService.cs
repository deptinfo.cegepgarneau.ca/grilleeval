using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces.Services.EtudiantsCours;
using Domain.Entities.CoursEtudiants;
using Domain.Entities.Etudiants;
using Domain.Repositories;

namespace Application.Services.EtudiantsCours
{
    public class EtudiantsCoursCreationService : IEtudiantsCoursCreationService
    {
        private readonly IEtudiantCoursRepository _etudiantsCoursRepository;
        public EtudiantsCoursCreationService(IEtudiantCoursRepository etudiantsCoursRepository)
        {
            _etudiantsCoursRepository = etudiantsCoursRepository;
        }

        public async Task CreateEtudiantsCours(List<EtudiantCours> etudiantsCours)
        {

            await _etudiantsCoursRepository.CreateEtudiantsCours(etudiantsCours);
        }
    }
}