﻿using Application.Interfaces.Services.Elements;
using Domain.Entities.Elements;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Elements
{
	public class ElementCreationService : IElementCreationService
	{
		private readonly IElementRepository _elementRepository;

		public ElementCreationService(IElementRepository elementRepository)
		{
			_elementRepository = elementRepository;
		}
		public async Task CreateElement(Element element, Guid grilleId)
		{
			await _elementRepository.CreateElement(element, grilleId);
		}

		public async Task DeleteElement(Guid elementId)
		{
			await _elementRepository.DeleteElement(elementId);
		}

		public async Task UpdateElement(Element element)
		{
			await _elementRepository.UpdateElement(element);
		}
	}
}
