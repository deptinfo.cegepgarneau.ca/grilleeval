﻿using Application.Interfaces.Services.Members;
using Domain.Entities;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Members
{
    public  class MemberCreationService : IMemberCreationService
    {
        private readonly IMemberRepository _memberRepository;

        public MemberCreationService(IMemberRepository memberRepository)
        {
            _memberRepository = memberRepository;
        }

        public async Task CreateMember(Member member)
        {
           await  _memberRepository.CreateMember(member);
        }

        public Member ObtenirMember(Guid id)
        {
            return _memberRepository.FindById(id);
        }
    }
}
