using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces.Services.Etudiants;
using Domain.Entities.Etudiants;
using Domain.Repositories;

namespace Application.Services.Etudiants
{
    public class EtudiantsCreationsService : IEtudiantsCreationService
    {
        private readonly IEtudiantRepository _etudiantRepository;
        public EtudiantsCreationsService(IEtudiantRepository etudiantRepository)
        {
            _etudiantRepository = etudiantRepository;
        }

        public async Task CreateEtudiants(List<Etudiant> etudiants)
        {
            await _etudiantRepository.AddEtudiants(etudiants);
        }
    }
}