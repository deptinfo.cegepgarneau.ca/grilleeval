﻿using Application.Interfaces.Services.CommentairesGlobaux;
using Domain.Entities.CommentaireGlobaux;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.CommentairesGlobaux
{
	public class CommentaireGlobalService : ICommentaireGlobalCreationService
	{

		private readonly ICommentaireGlobalRepository _commentaireGlobalRepository;

		public CommentaireGlobalService(ICommentaireGlobalRepository commentaireGlobalRepository)
		{
			_commentaireGlobalRepository = commentaireGlobalRepository;
		}

		public async Task CreateCommentaireGlobal(CommentaireGlobal comment, Guid elementId)
		{
			await _commentaireGlobalRepository.CreateCommentaireGlobal(comment, elementId);
		}

		public async Task DeleteCommentaireGlobal(Guid commentId)
		{
			await _commentaireGlobalRepository.DeleteCommentaireGlobal(commentId);
		}
	}
}
