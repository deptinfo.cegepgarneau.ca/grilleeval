﻿using Application.Interfaces.Services.Grilles;
using Domain.Entities.Grilles;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Grilles
{
	public class GrilleCreationService: IGrilleCreationService
	{
		private readonly IGrilleRepository _grilleRepository;

		public GrilleCreationService(IGrilleRepository grilleRepository)
		{
			_grilleRepository = grilleRepository;
		}

		public async Task CreateGrille(Grille grille, Guid coursId)
		{
			await _grilleRepository.CreateGrille(grille, coursId);
		}

		public async Task UpdateGrille(Grille grille)
		{
			await _grilleRepository.UpdateGrille(grille);
		}
	}
}
