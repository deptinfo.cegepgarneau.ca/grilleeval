﻿using Application.Interfaces.Services.CommentairesEtudiant;
using Application.Interfaces.Services.CommentairesGlobaux;
using Domain.Entities.CommentaireGlobaux;
using Domain.Entities.CommentairesEtudiant;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.CommentairesEtudiant
{
    public class CommentaireEtudiantCreationService : ICommentaireEtudiantCreationService
    {

        private readonly ICommentaireEtudiantRepository _CommentaireEtudiantRepository;

        public CommentaireEtudiantCreationService(ICommentaireEtudiantRepository CommentaireEtudiantRepository)
        {
            _CommentaireEtudiantRepository = CommentaireEtudiantRepository;
        }

        public async Task CreateCommentaireEtudiant(CommentaireEtudiant commentaireEtudiant, Guid elemenEtudiantId)
        {
            await _CommentaireEtudiantRepository.CreateCommentaireEtudiant(commentaireEtudiant, elemenEtudiantId);
        }

        public async Task DeleteCommentaireEtudiant(Guid commentaireEtudiant)
        {
            await _CommentaireEtudiantRepository.DeleteCommentaireEtudiant(commentaireEtudiant);
        }
    }
}