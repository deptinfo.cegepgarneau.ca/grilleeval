﻿using Application.Interfaces.Services.Baremes;
using Domain.Entities.Baremes;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Baremes
{
	public class BaremeCreationService : IBaremeCreationService
	{
		private readonly IBaremeRepository _baremeRepository;

		public BaremeCreationService(IBaremeRepository baremeRepository)
		{
			_baremeRepository = baremeRepository;
		}

		public async Task CreateBareme(Bareme bareme, Guid elementId)
		{
			await _baremeRepository.CreateBareme(bareme, elementId);
		}

		public async Task UpdateBareme(Bareme bareme)
		{
			await _baremeRepository.UpdateBareme(bareme);
		}

		public async Task DeleteBareme(Guid baremeId)
		{
			await _baremeRepository.DeleteBareme(baremeId);	
		}

		
	}
}
