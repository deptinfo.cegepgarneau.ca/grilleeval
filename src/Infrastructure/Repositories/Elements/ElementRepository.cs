﻿using Application.Exceptions.Grilles;
using Application.Exceptions.Elements;
using Application.Interfaces;
using Domain.Entities.Elements;
using Domain.Entities.Grilles;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Application.Extensions;

namespace Infrastructure.Repositories.Elements
{
	public class ElementRepository : IElementRepository
	{
		private readonly IGrilleEvalDbContext _context;

		public ElementRepository(IGrilleEvalDbContext context)
		{
			_context = context;
		}

		public async Task CreateElement(Element element, Guid grilleId)
		{
			if(element is null || grilleId == Guid.Empty)
				throw new ArgumentNullException("Couldn't create Element with null args");

			Grille grille = _context.Grilles.FirstOrDefault(g => g.Id == grilleId)!;

			if (grille is null)
				throw new GrilleNotFoundException("The given id doesn't correspond to any \"Grille\" in the database");

			if (grille.Element is null)
				grille.Element = new List<Element>() { element };
			else
				grille.Element.Add(element);
			
			await _context.SaveChangesAsync();
		}

		public async Task DeleteElement(Guid elementId)
		{
			if (elementId == Guid.Empty)
				throw new ArgumentNullException("Provide an ID to delete an Element from the database");

			Element element = _context.Elements.FirstOrDefault<Element>(x => x.Id == elementId)!;

			if(element is null)
			{
				throw new ElementNotFoundException("\"The given id doesn't correspond to any \\\"Element\\\" in the database\"");
			}
			_context.Elements.Remove(element);
			await _context.SaveChangesAsync();
		}

        public async Task<Element> GetById(Guid id)
        {
            var peutEtreElement = _context.Elements
			.Include(e => e.Bareme)
            .Include(e => e.Critere)
            .Include(e => e.Commentaire)
            .FirstOrDefault(e => e.Id == id);

            if (peutEtreElement is null)
				throw new InvalidOperationException($"L'élément avec l'id {id} n'existe pas");

			return peutEtreElement;
        }

        public async Task UpdateElement(Element element)
		{
			if (element is null)
				throw new ArgumentNullException("Couldn't update Element with null args");

			_context.Elements.Update(element);
			await _context.SaveChangesAsync();
		}
	}
}
