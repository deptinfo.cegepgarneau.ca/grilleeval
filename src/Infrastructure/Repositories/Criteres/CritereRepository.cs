﻿using Application.Exceptions.CommentairesEtudiant;
using Application.Exceptions.Criteres;
using Application.Exceptions.Elements;
using Application.Interfaces;
using Domain.Entities;

using Domain.Entities.CommentairesEtudiant;
using Domain.Entities.Criteres;
using Domain.Entities.Elements;
using Domain.Entities.Etudiants;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Slugify;

namespace Infrastructure.Repositories.Criteres;

public class CritereRepository : ICritereRepository
{
    private readonly IGrilleEvalDbContext _context;

    public CritereRepository(IGrilleEvalDbContext context)
    {
        _context = context;
    }

    public List<Critere> getAllCriteresByIdElement(Guid id)
    {
        return _context.Criteres.AsNoTracking().ToList();
    }

    public async Task CreateCritere(Critere critere, Guid elementId)
    {
        if (critere is null || elementId == Guid.Empty)
            throw new ArgumentNullException("Couldn't create Critere with null args");

        Element element = _context.Elements.FirstOrDefault(el => el.Id == elementId)!;

        if (element is null)
            throw new ElementNotFoundException("The given id doesn't correspond to any \"Element\" in the database");

        if(element.Critere is null)
            element.Critere = new List<Critere> { critere };
        else
            element.Critere.Add(critere);

        await _context.SaveChangesAsync();
    }

    public async Task DeleteCritere(Guid critereId)
    {
        if (critereId == Guid.Empty)
            throw new ArgumentNullException("Couldn't delete Critere with null args");

        Critere critere = _context.Criteres.FirstOrDefault(x => x.Id == critereId)!;

        if (critere is null)
            throw new CritereNotFoundException("The given id doesn't correspond to any \"Critere\" in the database");

        _context.Criteres.Remove(critere);
        await _context.SaveChangesAsync();
    }
}

    

