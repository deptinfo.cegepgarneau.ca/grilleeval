using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Exceptions.Etudiants;
using Application.Interfaces;
using Domain.Entities.Elements;
using Domain.Entities.Etudiants;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Infrastructure.Repositories.Etudiants
{
    public class EtudiantRepository : IEtudiantRepository
    {
        private readonly IGrilleEvalDbContext _context;
        public EtudiantRepository(IGrilleEvalDbContext context)
        {
            _context = context;
        }
        public Etudiant FindById(string da)
        {
            return _context.Etudiants.AsNoTracking().FirstOrDefault(e => e.Da == da);
        }

        public List<Etudiant> GetAll()
        {
            return _context.Etudiants.AsNoTracking().ToList();
        }
        public async Task AddEtudiants(List<Etudiant> etudiants)
        {
            if (etudiants == null || etudiants.Count == 0)
                throw new Exception("Liste d'étudiants vide");
            

            foreach (var etudiant in etudiants)
            {
                var existingEtudiant = FindById(etudiant.Da);

                if (existingEtudiant == null)
                    _context.Etudiants.Add(etudiant);
            }

            await _context.SaveChangesAsync();
        }

        public async Task AddEtudiant(Etudiant etudiant)
        {
            if (etudiant == null)
                throw new ArgumentNullException(nameof(etudiant));
            await _context.Etudiants.AddAsync(etudiant);
            await _context.SaveChangesAsync();
        }

        public async Task<Etudiant> GetById(Guid id)
        {
            var peutEtreEtudiant = _context.Etudiants
             .Include(e => e.EtudiantsCours)
            .FirstOrDefault(g => g.Id == id);

            if (peutEtreEtudiant is null)
                throw new InvalidOperationException($"L'étudiant avec l'id {id} n'existe pas");

            return peutEtreEtudiant;
        }
    }
}