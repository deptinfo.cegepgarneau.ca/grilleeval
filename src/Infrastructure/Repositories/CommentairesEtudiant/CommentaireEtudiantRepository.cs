﻿using Application.Exceptions.CommentairesEtudiant;
using Application.Exceptions.Elements;
using Application.Interfaces;
using Domain.Entities;

using Domain.Entities.CommentairesEtudiant;
using Domain.Entities.ElementsEtudiant;
using Domain.Entities.Etudiants;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Slugify;

namespace Infrastructure.Repositories.CommentairesEtudiant;

public class CommentaireEtudiantRepository : ICommentaireEtudiantRepository
{
    private readonly ISlugHelper _slugHelper;
    private readonly IGrilleEvalDbContext _context;

    public CommentaireEtudiantRepository(IGrilleEvalDbContext context, ISlugHelper slugHelper)
    {
        _context = context;
        _slugHelper = slugHelper;
    }

    public List<CommentaireEtudiant> GetAllCommentairesEtudiant()
    {
        return _context.CommentairesEtudiant.AsNoTracking().ToList();
    }


    public List<CommentaireEtudiant> GetAllCommentairesEtudiantByIdEtudiant(Etudiant id)
    {
        throw new NotImplementedException();
    }


    public CommentaireEtudiant GetById(Guid id)
    {
        var commentaire = _context.CommentairesEtudiant
            .AsNoTracking()
            .FirstOrDefault(x => x.Id == id);
        if (commentaire == null)
            throw new CommentaireEtudiantNotFoundException($"Impossible de trouver le commentaire avec l'identifiant {id}");
        return commentaire;
    }

    public async Task CreateCommentaireEtudiant(CommentaireEtudiant commentaireEtudiant, Guid elementEtudiantId)
    {

        if (commentaireEtudiant is null || elementEtudiantId == Guid.Empty)
            throw new ArgumentNullException("Impossible de créer un commentaire vide");

        ElementEtudiant elementEtudiant = _context.ElementsEtudiant.FirstOrDefault(e => e.Id == elementEtudiantId)!;

        if (elementEtudiant is null)
            throw new ElementNotFoundException("L'elementEtudiant n'existe pas.");

        if (elementEtudiant.CommentairesEtudiant is null)
            elementEtudiant.CommentairesEtudiant = new List<CommentaireEtudiant> { commentaireEtudiant };
        else
            elementEtudiant.CommentairesEtudiant.Add(commentaireEtudiant);


        if (_context.CommentairesEtudiant.Any(x => x.Commentaire == commentaireEtudiant.Commentaire))
            throw new CommentaireEtudiantAlreadyExistsException($"Le commentaire {commentaireEtudiant.Commentaire} existe déjà.");

        _context.CommentairesEtudiant.Add(commentaireEtudiant);

        await _context.SaveChangesAsync();
    }

    public async Task UpdateCommentaireEtudiant(CommentaireEtudiant CommentaireEtudiant)
    {
        if (_context.CommentairesEtudiant.Any(x => x.Commentaire == CommentaireEtudiant.Commentaire)) 
            throw new CommentaireEtudiantAlreadyExistsException($"Le commentaire {CommentaireEtudiant.Commentaire} existe déjà.");
        _context.CommentairesEtudiant.Update(CommentaireEtudiant);
        await _context.SaveChangesAsync();
    }

    public async Task DeleteCommentaireEtudiant(Guid id)
    {
        var CommentaireEtudiant = _context.CommentairesEtudiant.FirstOrDefault(x => x.Id == id);
        if (CommentaireEtudiant == null)
            throw new CommentaireEtudiantNotFoundException($"Impossible de trouver le commentaire avec l'identifiant {id}");

        _context.CommentairesEtudiant.Remove(CommentaireEtudiant);
        await _context.SaveChangesAsync();
    }



  
}
