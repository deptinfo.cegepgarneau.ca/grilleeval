using Application.Exceptions.Members;
using Application.Interfaces;
using Domain.Entities;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Members;

public class MemberRepository : IMemberRepository
{
    private readonly IGrilleEvalDbContext _context;

    public MemberRepository(IGrilleEvalDbContext context)
    {
        _context = context;
    }

    public int GetMemberCount()
    {
        return _context.Members.Count();
    }

    public List<Member> GetAllWithUserEmail(string userEmail)
    {
        return _context.Members
            .Include(x => x.User)
            .Where(x => x.User.Email == userEmail)
            .AsNoTracking()
            .ToList();
    }
    
    public List<Member> GetAll()
    {
        return _context.Members
            .Include(x => x.User)
            .ThenInclude(x => x.UserRoles)
            .ThenInclude(x => x.Role)
            .AsNoTracking()
            .ToList();
    }

    public Member FindById(Guid id)
    {
        var member = _context.Members
            .Include(x => x.User)
            .ThenInclude(x => x.UserRoles)
            .ThenInclude(x => x.Role)
            .FirstOrDefault(x => x.Id == id);
        if (member == null)
            throw new MemberNotFoundException($"No member with id {id} was found.");
        return member;
    }

    public Member? FindByUserId(Guid userId, bool asNoTracking = true)
    {
        var query = _context.Members as IQueryable<Member>;
        if (asNoTracking)
            query = query.AsNoTracking();
        return query
            .Include(x => x.User)
            .ThenInclude(x => x.UserRoles)
            .ThenInclude(x => x.Role)
            .FirstOrDefault(x => x.User.Id == userId);
    }

    public Member? FindByUserEmail(string userEmail)
    {
        return _context.Members
            .Include(x => x.User)
            .ThenInclude(x => x.UserRoles)
            .ThenInclude(x => x.Role)
            .FirstOrDefault(x => x.User.Email == userEmail);

    }
    
    public async Task CreateMember(Member member)
    {
        _context.Members.Add(member);
        await _context.SaveChangesAsync();
    }
    
    public async Task DeleteMemberById(Guid id)
    {
        var member = _context.Members.FirstOrDefault(x => x.Id == id);
        if (member == null)
            throw new MemberNotFoundException($"No member with id {id} was found.");
        _context.Members.Remove(member);
        await _context.SaveChangesAsync();
    }
}