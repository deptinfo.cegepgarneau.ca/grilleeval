﻿using Application.Exceptions.Books;
using Application.Exceptions.Cours;
using Application.Exceptions.Grilles;
using Application.Interfaces;
using Domain.Entities.Books;
using Domain.Entities.Elements;
using Domain.Entities.Grilles;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using RazorLight.Extensions;
using Slugify;



namespace Infrastructure.Repositories.Grilles
{
    public class GrilleRepository : IGrilleRepository
    {

        private readonly IGrilleEvalDbContext _context;

        public GrilleRepository(IGrilleEvalDbContext context)
        {
            _context = context;
        }

        public async Task CreateGrille(Grille grille, Guid coursId)
        {
            if (grille == null || coursId == Guid.Empty)
                throw new ArgumentNullException("Could not create grille with null args");

            grille.Cours = _context.Cours.FirstOrDefault(x => x.Id == coursId)!;
            if (grille.Cours is null)
                throw new CoursNotFoundException("The given id doesn't correspond to any \"Cours\" in the database");

			_context.Grilles.Add(grille);
            await _context.SaveChangesAsync();
        }

		public async Task UpdateGrille(Grille grille)
        {
            if (grille is null)
                throw new ArgumentNullException("Couldn't update Grille with null args");

			_context.Grilles.Update(grille);
            await _context.SaveChangesAsync();
        }

		public async Task DeleteGrilleWithId(Guid id)
        {
            var grille = _context.Grilles.FirstOrDefault(x => x.Id == id);
            if (grille == null)
                throw new GrilleNotFoundException($"Could not find book with id {id}.");

            _context.Grilles.Remove(grille);
            await _context.SaveChangesAsync();
        }

        public Grille FindById(Guid id)
        {
           var grille = _context.Grilles
	        .Include(g => g.Cours)
                .ThenInclude(c => c.EtudiantsCours)
                .ThenInclude(ec => ec.Etudiant)
			.Include(g => g.ListeElementsEtudiants)
				.ThenInclude(lee => lee.Etudiant)
			.Include(g => g.Element)
		        .ThenInclude(e => e.Bareme)
	        .Include(g => g.Element)
				.ThenInclude(e => e.Critere)
	        .Include(g => g.Element)
		        .ThenInclude(e => e.Commentaire)
            
	        .FirstOrDefault(g => g.Id == id);

            

            if (grille is null)
                throw new GrilleNotFoundException($"Could not find grille with id {id}");
            return grille;
        }

        public List<Grille> GetAll()
        {
	        var listeGrille = _context.Grilles
		        .Include(c => c.Cours)
		        .Include(g => g.Element)
		        .ThenInclude(e => e.Bareme)
		        .Include(g => g.Element)
		        .ThenInclude(e => e.Critere)
		        .Include(g => g.Element)
		        .ThenInclude(e => e.Commentaire)
		        .ToList();
            
            return listeGrille;
        }

		public List<Grille> GetGrilleWithCoursId(Guid coursId)
		{
			var grilles = _context.Grilles.Where(x => x.Cours.Id == coursId).ToList();
			// if (grilles.Count == 0)
			// 	throw new GrilleNotFoundException("Aucune grille n'a été trouvée dans ce cours");

			return grilles;
		}

		public async Task CopyGrille(Guid grilleId, Guid coursId, string titre, int ponderation)
		{
			var grille = _context.Grilles
				.Include(g => g.Element)
				.ThenInclude(e => e.Bareme)
				.Include(g => g.Element)
				.ThenInclude(e => e.Critere)
				.Include(g => g.Element)
				.ThenInclude(e => e.Commentaire)
				.FirstOrDefault(g => g.Id == grilleId);
			grille.Cours = _context.Cours.FirstOrDefault(x => x.Id == coursId)!;
			grille.SetId(Guid.NewGuid());
			grille.Titre = titre;
			grille.Ponderation = ponderation;
			foreach (var element in grille.Element)
			{
				element.SetId(Guid.NewGuid());
				foreach (var commentaire in element.Commentaire)
				{
					commentaire.SetId(Guid.NewGuid());
				}

				foreach (var bareme in element.Bareme)
				{
					bareme.SetId(Guid.NewGuid());
				}

				foreach (var critere in element.Critere)
				{
					critere.SetId(new Guid());
				}
			}
			_context.Grilles.Add(grille);
			foreach (var element in grille.Element)
			{
				_context.Elements.Add(element);
				foreach (var commentaire in element.Commentaire)
				{
					_context.CommentairesGlobaux.Add(commentaire);
				}

				foreach (var bareme in element.Bareme)
				{
					_context.Baremes.Add(bareme);
				}

				foreach (var critere in element.Critere)
				{
					_context.Criteres.Add(critere);
				}
			}
			
			
			await _context.SaveChangesAsync();
		}
	}
}
