﻿using Application.Exceptions.Cours;
using Application.Interfaces;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Cours
{
    public class CoursRepository : ICoursRepository
    {

        public readonly IGrilleEvalDbContext _context;
        private IMemberRepository _memberRepository;
        public CoursRepository(IGrilleEvalDbContext context, IMemberRepository memberRepository)
        {
            _context = context;
            _memberRepository = memberRepository;
        }

        /// <summary>
        /// Récupère tous les cours disponibles dans la base de données.
        /// </summary>
        /// <returns>
        /// Une liste de tous les cours sans suivi par le contexte de suivi des modifications.
        /// </returns>
        public List<Domain.Entities.DossierCours.Cours> GetAll()
        {
            return _context.Cours.AsNoTracking().Distinct()
                .Include(c => c.EtudiantsCours)
                .Include(c => c.Membre)
                .ToList();
        }

        /// <summary>
        /// Trouve un cours spécifique dans la base de données par son identifiant unique.
        /// </summary>
        /// <param name="id">L'identifiant unique du cours à rechercher.</param>
        /// <returns>
        /// Le cours correspondant à l'identifiant donné, sans suivi par le contexte de suivi des modifications.
        /// </returns>
        /// <exception cref="CoursNotFoundException">
        /// Si aucun cours n'est trouvé avec l'identifiant spécifié.
        /// </exception>
        public Domain.Entities.DossierCours.Cours FindById(Guid id)
        {
           
            var cours = _context.Cours.AsNoTracking().FirstOrDefault(c => c.Id == id);

            if (cours == null)
                throw new CoursNotFoundException($"Impossible de trouver le cours avec l'identifiant {id}");

            return cours;
        }

        /// <summary>
        /// Crée un nouveau cours dans la base de données.
        /// </summary>
        /// <param name="cours">L'objet Cours à ajouter à la base de données.</param>
        /// <exception cref="CoursWithCodeAlreadyExisteException">
        /// Si un cours avec le même code existe déjà dans la base de données.
        /// </exception>
        public async Task CreateCours(Domain.Entities.DossierCours.Cours cours)
        {
            if (_context.Cours.Any(c => c.Code.Trim() == cours.Code.Trim()))
                throw new CoursWithCodeAlreadyExisteException($"Un cours avec le même code existe déjà.");
            if (_context.Cours.Any(c => c.NomCours.Trim() == cours.NomCours.Trim()))
                throw new CoursWithCodeAlreadyExisteException($"Un cours avec le même nom existe déjà.");

             _context.Cours.Add(cours);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Met à jour un cours existant dans la base de données avec les informations fournies.
        /// </summary>
        /// <param name="cours">L'objet Cours contenant les informations mises à jour.</param>
        /// <exception cref="CoursWithCodeAlreadyExisteException">
        /// Si un autre cours avec le même code existe déjà dans la base de données, sauf s'il s'agit du même cours.
        /// </exception>
        public async Task UpdateCours(Domain.Entities.DossierCours.Cours cours)
        {
            var existingCours = await _context.Cours
                .FirstOrDefaultAsync(c => c.Id == cours.Id);

            if (existingCours == null)
                throw new CoursNotFoundException($"Cours with Id not found.");
            


            if (_context.Cours.Any(c => c.Id != cours.Id && c.Code.Trim() == cours.Code.Trim()))
                throw new CoursWithCodeAlreadyExisteException($"Un cours avec le même code existe déjà.");
            

            if (_context.Cours.Any(c => c.Id != cours.Id && c.NomCours.Trim() == cours.NomCours.Trim()))
                throw new CoursWithCodeAlreadyExisteException($"Un cours avec le même nom existe déjà.");
            


            _context.Cours.Update(existingCours).CurrentValues.SetValues(cours);
            await _context.SaveChangesAsync();
        }


        /// <summary>
        /// Supprime un cours spécifique de la base de données en utilisant son identifiant unique.
        /// </summary>
        /// <param name="id">L'identifiant unique du cours à supprimer.</param>
        /// <exception cref="CoursNotFoundException">
        /// Si aucun cours n'est trouvé avec l'identifiant spécifié.
        /// </exception>
        public async Task DeleteCoursWithId(Guid id)
        {
            var cours = _context.Cours.FirstOrDefault(c => c.Id == id);
            if (cours == null)
                throw new CoursNotFoundException($"Impossible de trouver le cours avec l'identifiant {id}");

            _context.Cours.Remove(cours);
            await _context.SaveChangesAsync();

        }

        public List<Domain.Entities.DossierCours.Cours> GetAllByMember(Guid idMember)
        {

            var membre = _memberRepository.GetAll();
            var cours = _context.Cours.Where(x => x.Membre.Id == idMember)
                .Include(c=>c.Membre)
                .ToList();
            return cours;
        }
    }
}
