﻿using Application.Exceptions.Books;
using Application.Exceptions.ElementsEtudiant;
using Application.Interfaces;
using Domain.Entities.Books;
using Domain.Entities.CoursEtudiants;
using Domain.Entities.ElementsEtudiant;
using Domain.Entities.Etudiants;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Slugify;

namespace Infrastructure.Repositories.ElementEtudiants;

public class ElementEtudiantRepository : IElementEtudiantRepository
{
    private readonly ISlugHelper _slugHelper;
    private readonly IGrilleEvalDbContext _context;

    public ElementEtudiantRepository(IGrilleEvalDbContext context, ISlugHelper slugHelper)
    {
        _context = context;
        _slugHelper = slugHelper;
    }



    public List<ElementEtudiant> GetElementsEtudiant()
    {
        return _context.ElementsEtudiant.AsNoTracking().ToList();
    }

    public ElementEtudiant getElementEtudiant(Guid idEtudiant)
    {
        var element = _context.ElementsEtudiant
            .AsNoTracking()
            .FirstOrDefault(x => x.Id == idEtudiant);
        if (element == null)
            throw new ElementEtudiantNotFoundException($"Impossible de trouver l'elementEtudiant avec l'identifiant {idEtudiant}");
        return element;
    }

    public async Task CreateElementEtudiant(ElementEtudiant elementEtudiant)
    {
        _context.ElementsEtudiant.Add(elementEtudiant);
        await _context.SaveChangesAsync();


    }

    public async Task EditElementEtudiant(ElementEtudiant ElementEtudiant)
    {
        if ((_context.ElementsEtudiant.Any(x => x.Id == ElementEtudiant.Id) && (_context.ElementsEtudiant.Any(x => x.ElementEvalue.Id == ElementEtudiant.ElementEvalue.Id))))
            throw new ElementEtudiantAlreadyExistsException($"L'ElementEtudiant avec l'id étudiant {ElementEtudiant.Id} et {ElementEtudiant.ElementEvalue.Id} existe déjà.");

        _context.ElementsEtudiant.Update(ElementEtudiant);
        await _context.SaveChangesAsync();
    }

    public async Task DeleteElementEtudiant(Guid id)
    {
        var elementEtudiant = _context.ElementsEtudiant.FirstOrDefault(x => x.Id == id);
        if (elementEtudiant == null)
            throw new ElementEtudiantNotFoundException($"Impossible de trouver l'elementEtudiant avec l'identifiant {id}");

        _context.ElementsEtudiant.Remove(elementEtudiant);
        await _context.SaveChangesAsync();
    }


}