﻿using Application.Exceptions.Baremes;
using Application.Exceptions.Elements;
using Application.Interfaces;
using Domain.Entities.Baremes;
using Domain.Entities.Elements;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Baremes
{
	public class BaremeRepository : IBaremeRepository
	{
		private readonly IGrilleEvalDbContext _context;

		public BaremeRepository(IGrilleEvalDbContext context)
		{
			_context = context;
		}

		public async Task CreateBareme(Bareme bareme, Guid elementId)
		{
			if (bareme == null || elementId == Guid.Empty)
				throw new ArgumentNullException("Couldn't create Bareme with null args");

			Element element = _context.Elements.FirstOrDefault(el => el.Id == elementId)!;

			if (element is null)
				throw new ElementNotFoundException("The given id doesn't correspond to any \"Element\" in the database");

			if(element.Bareme is null)
				element.Bareme = new List<Bareme> { bareme};
			else
				element.Bareme.Add(bareme);

			await _context.SaveChangesAsync();
		}

		public async Task DeleteBareme(Guid baremeId)
		{
			if (baremeId == Guid.Empty)
				throw new ArgumentNullException("Couldn't delete Bareme with null args");

			Bareme bareme = _context.Baremes.FirstOrDefault(x => x.Id == baremeId)!;

			if (bareme is null)
				throw new BaremeNotFoundException("The given id doesn't correspond to any \\\"Bareme\\\" in the database");

			_context.Baremes.Remove(bareme);

			await _context.SaveChangesAsync();
		}

		public async  Task UpdateBareme(Bareme bareme)
		{
			if (bareme is null)
				throw new ArgumentNullException("Couldn't update Bareme with null args");
			_context.Baremes.Update(bareme);
			await _context.SaveChangesAsync();
		}
	}
}
