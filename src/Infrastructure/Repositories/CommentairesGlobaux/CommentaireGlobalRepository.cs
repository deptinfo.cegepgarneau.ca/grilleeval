﻿using Application.Exceptions.CommentairesGlobaux;
using Application.Exceptions.Elements;
using Application.Interfaces;
using Domain.Entities.CommentaireGlobaux;
using Domain.Entities.Elements;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.CommentairesGlobaux
{
	public class CommentaireGlobalRepository : ICommentaireGlobalRepository
	{
		private readonly IGrilleEvalDbContext _context;

		public CommentaireGlobalRepository(IGrilleEvalDbContext context)
		{
			_context = context;
		}

		public async Task CreateCommentaireGlobal(CommentaireGlobal comment, Guid elementId)
		{
			if (comment is null || elementId == Guid.Empty)
				throw new ArgumentNullException("Couldn't create CommentaireGlobal with null args");

			Element element = _context.Elements.FirstOrDefault(e => e.Id == elementId)!;

			if (element is null)
				throw new ElementNotFoundException("The given id doesn't correspond to any \"Element\" in the database");

			if(element.Commentaire is null)
				element.Commentaire = new List<CommentaireGlobal> { comment };
			else
				element.Commentaire.Add(comment);

			await _context.SaveChangesAsync();
		}

		public async Task DeleteCommentaireGlobal(Guid commentId)
		{
			if (commentId == Guid.Empty)
				throw new ArgumentNullException("Couldn't delete CommentaireGlobal with null args");

			CommentaireGlobal comment = _context.CommentairesGlobaux.FirstOrDefault(e => e.Id == commentId)!;

			if (comment is null)
				throw new CommentaireGlobalNotFoundException("The given id doesn't correspond to any \"CommentaireGlobal\" in the database");

			_context.CommentairesGlobaux.Remove(comment);
			await _context.SaveChangesAsync();
		}
	}
}
