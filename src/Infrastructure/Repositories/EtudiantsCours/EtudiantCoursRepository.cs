using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Application.Exceptions.Books;
using Application.Exceptions.Cours;
using Application.Interfaces;
using Domain.Entities.CoursEtudiants;
using Domain.Entities.ElementsEtudiant;
using Domain.Entities.Etudiants;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.EtudiantsCours
{
    public class EtudiantCoursRepository : IEtudiantCoursRepository
    {
        public readonly IGrilleEvalDbContext _context;
        public EtudiantCoursRepository(IGrilleEvalDbContext context)
        {
            _context = context;
        }
        public List<EtudiantCours> GetAllByIdCours(Guid idCours)
        {
            var cours = _context.Cours.AsNoTracking().FirstOrDefault(x => x.Id == idCours);
            if (cours == null)
                throw new CoursNotFoundException($"Impossible de trouver le cours avec l'identifiant {idCours}");

            return _context.EtudiantsCours.AsNoTracking().Include(c => c.Cours).Include(ec => ec.Etudiant).Where(x => x.Cours.Id == idCours).ToList();
        }
        public async Task CreateEtudiantsCours(List<EtudiantCours> etudiantsCours)
        {
            foreach (var etudiantCour in etudiantsCours)
            {
                if (etudiantCour.EtudiantId != Guid.Empty && !EtudiantCoursExists(etudiantCour.CoursId, etudiantCour.EtudiantId))
                {
                    await _context.EtudiantsCours.AddAsync(etudiantCour);
                }
            }
            await _context.SaveChangesAsync();
        }

        public bool EtudiantCoursExists(Guid idCours, Guid idEtudiant)
        {
            return _context.EtudiantsCours
                .Any(ec => ec.Cours.Id == idCours && ec.Etudiant.Id == idEtudiant);
        }
        public async Task DeleteEtudiantCoursWithId(Guid id)
        {
            var etudiantCours = _context.EtudiantsCours.FirstOrDefault(x => x.Id == id);
            if (etudiantCours == null)
                throw new BookNotFoundException($"Could not find etudiantsCours with id {id}");

            _context.EtudiantsCours.Remove(etudiantCours);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteStudentsWithCourseId(Guid coursId)
        {
            var etudiantsCours = _context.EtudiantsCours.Where(x => x.CoursId == coursId).ToList();
            _context.EtudiantsCours.RemoveRange(etudiantsCours);
            var eltEtudiants = new List<ElementEtudiant>();
            foreach (var ec in etudiantsCours)
            {
                var guid = ec.EtudiantId;
                var el = _context.ElementsEtudiant.Where(x => x.Etudiant.Id == guid).FirstOrDefault();
                if (el != null)
                    eltEtudiants.Add(el);
            }
            _context.ElementsEtudiant.RemoveRange(eltEtudiants);
            await _context.SaveChangesAsync();
        }
    }
}