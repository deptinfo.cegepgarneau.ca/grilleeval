﻿using Domain.Entities.CommentairesEtudiant;
using Domain.Entities.Etudiants;

namespace Domain.Repositories;

public interface ICommentaireEtudiantRepository
{
    List<CommentaireEtudiant> GetAllCommentairesEtudiant();

    List<CommentaireEtudiant> GetAllCommentairesEtudiantByIdEtudiant(Etudiant id );
    CommentaireEtudiant GetById(Guid id);
    Task CreateCommentaireEtudiant(CommentaireEtudiant commentaireEtudiant, Guid elementEtudiantId);
    Task UpdateCommentaireEtudiant(CommentaireEtudiant commentaireEtudiant);
    Task DeleteCommentaireEtudiant(Guid commentaireEtudiantid);
}