﻿
using Domain.Entities.ElementsEtudiant;
using Domain.Entities.Etudiants;


namespace Domain.Repositories;

public interface IElementEtudiantRepository
{
    List<ElementEtudiant> GetElementsEtudiant();
    ElementEtudiant getElementEtudiant(Guid id);

    Task CreateElementEtudiant(ElementEtudiant elementEtudiant);
    Task EditElementEtudiant(ElementEtudiant elementEtudiant);
    Task DeleteElementEtudiant(Guid id);
}