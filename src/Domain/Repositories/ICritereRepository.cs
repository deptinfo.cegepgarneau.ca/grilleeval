﻿using Domain.Entities.Criteres;
using Domain.Entities.Grilles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface ICritereRepository
    {
        List<Critere> getAllCriteresByIdElement(Guid idElement);

        Task CreateCritere(Critere critere, Guid elementId);
        Task DeleteCritere(Guid critereId);
    }
}
