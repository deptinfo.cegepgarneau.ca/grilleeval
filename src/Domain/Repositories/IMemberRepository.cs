﻿using Domain.Entities;

namespace Domain.Repositories;

public interface IMemberRepository
{
    int GetMemberCount();
    List<Member> GetAllWithUserEmail(string userEmail);
    List<Member> GetAll();
    Member FindById(Guid id);
    Member? FindByUserId(Guid userId, bool asNoTracking = true);
    Member? FindByUserEmail(string userEmail);
    Task CreateMember(Member member);
    Task DeleteMemberById(Guid id);
}