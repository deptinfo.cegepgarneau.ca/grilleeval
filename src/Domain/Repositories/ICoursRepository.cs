﻿using Domain.Entities.DossierCours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface ICoursRepository
    {
        List<Cours> GetAll();
        Cours FindById(Guid id);
        Task CreateCours(Cours cours);
        Task UpdateCours(Cours cours);
        Task DeleteCoursWithId(Guid id);
        List<Cours> GetAllByMember(Guid idMember);
    }
}
