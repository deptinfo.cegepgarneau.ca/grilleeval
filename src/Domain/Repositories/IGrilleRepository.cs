﻿using Domain.Entities.Books;
using Domain.Entities.Grilles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IGrilleRepository
    {
        Task CreateGrille(Grille grille, Guid coursId);
        List<Grille> GetAll();
        Grille FindById(Guid id);
        Task DeleteGrilleWithId(Guid id);
        List<Grille> GetGrilleWithCoursId(Guid id);
        Task UpdateGrille(Grille grille);
        Task CopyGrille(Guid grilleId, Guid coursId, string titre, int ponderation);
    }
}