﻿using Domain.Entities.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
	public interface IElementRepository
	{
		Task CreateElement(Element element, Guid grilleId);
		Task DeleteElement(Guid elementId);
		Task UpdateElement(Element element);

		Task<Element> GetById(Guid id);
	}
}
