﻿using Domain.Entities.Baremes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
	public interface IBaremeRepository
	{
		Task CreateBareme(Bareme bareme, Guid elementId);
		Task UpdateBareme(Bareme bareme);
		Task DeleteBareme(Guid baremeId);
	}
}
