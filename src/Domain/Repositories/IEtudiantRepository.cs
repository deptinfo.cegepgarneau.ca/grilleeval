using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities.Elements;
using Domain.Entities.Etudiants;

namespace Domain.Repositories
{
    public interface IEtudiantRepository
    {
        List<Etudiant> GetAll();
        Etudiant FindById(string da);
        Task AddEtudiants(List<Etudiant> etudiants);
        Task AddEtudiant(Etudiant etudiant);

        Task<Etudiant> GetById(Guid id);
    }
}