﻿using Domain.Entities.CommentaireGlobaux;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
	public interface ICommentaireGlobalRepository
	{
		Task CreateCommentaireGlobal(CommentaireGlobal comment, Guid elementId);
		Task DeleteCommentaireGlobal(Guid elementId);
	}
}
