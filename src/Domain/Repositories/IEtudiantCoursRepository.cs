using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities.CoursEtudiants;
using Domain.Entities.Etudiants;

namespace Domain.Repositories
{
    public interface IEtudiantCoursRepository
    {
        List<EtudiantCours> GetAllByIdCours(Guid idCours);
        Task CreateEtudiantsCours(List<EtudiantCours> etudiantsCours);
        bool EtudiantCoursExists(Guid idCours, Guid idEtudiant);
        Task DeleteEtudiantCoursWithId(Guid id);
        Task DeleteStudentsWithCourseId(Guid CoursId);
    }
}