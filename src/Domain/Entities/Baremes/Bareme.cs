﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Baremes
{
	public class Bareme : AuditableAndSoftDeletableEntity
	{
		public string Titre { get; set; } = default!;
		public string Description { get; set; } = default!;
		public int? MinValue { get; set; } = null;
		public int MaxValue { get; set; } = default!;

		public Guid ElementId { get; set; }

	}
}
