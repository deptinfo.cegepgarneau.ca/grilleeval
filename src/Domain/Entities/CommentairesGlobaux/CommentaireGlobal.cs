﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.CommentaireGlobaux
{
	public class CommentaireGlobal : AuditableAndSoftDeletableEntity
	{
		public string Commentaire { get; set; } = default!;
		public Guid ElementId { get; set; }
        
    }
}
