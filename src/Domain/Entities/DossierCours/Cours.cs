﻿using Domain.Common;
using Domain.Entities.CoursEtudiants;
using Domain.Entities.Etudiants;
using Domain.Entities.Grilles;
using Domain.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Domain.Entities.DossierCours
{
    public class Cours : AuditableAndSoftDeletableEntity
    {
        public string Code { get; set; } = default!;
        public string NomCours { get; set; } = default!;
       
        public List<EtudiantCours> EtudiantsCours { get; set; } = default!;
        [JsonIgnore]
        public List<Grille> Grille { get; set; } = default!;

       
        public Member? Membre { get; set; } = default!;
    }
}
