﻿using Domain.Common;
using Domain.Entities.DossierCours;
using Domain.Entities.Elements;
using Domain.Entities.ElementsEtudiant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Domain.Entities.Grilles
{
	public class Grille: AuditableAndSoftDeletableEntity
	{ 
		public string Titre { get; set; } = default!;
		public float Ponderation { get; set; } = default!;
		//public JsonObject Echelles { get; set; } = default!;

		//A ne pas supprimer => c'est avec ca que je récupère les note de chaque etudiant (elemetEtudiant) dans ma grille a consulter
		public List<ElementEtudiant> ListeElementsEtudiants { get; set; } = new List<ElementEtudiant>();
		
		public List<Element>? Element { get; set; }
        [JsonIgnore]
        public Cours Cours { get; set; }
       

    }
}
