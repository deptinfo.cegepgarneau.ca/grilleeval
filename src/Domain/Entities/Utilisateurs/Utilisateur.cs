﻿using Domain.Common;
using Microsoft.EntityFrameworkCore;

namespace Domain.Entities.Utilisateur;

public class Utilisateur : AuditableAndSoftDeletableEntity, ISanitizable
{
    public string? Nom { get; private set;} = default!;
    public string? Prenom { get; private set;} = default!;
    public string? DA { get; private set;} = default!;
    public string? Email { get; private set;} = default!;
    //Si true, l'utilisateur est un admin, sinon c'est un utilisateur normal
    public bool? Role { get; private set;} = default!;


    public void SetNom(string nom) => Nom = nom;
    public void SetIsbn(string prenom) => Prenom = prenom;
    public void SetDA(string da) => DA = da;
    public void SetEmail(string email) => Email = email;
    public void SetRole(bool role) => Role = role;

    public void SanitizeForSaving()
    {
        Nom = Nom.Trim();
        Prenom = Prenom.Trim();
        DA = DA.Trim();
        Email = Email.Trim();
    }
}