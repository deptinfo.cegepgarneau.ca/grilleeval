﻿using Domain.Common;
using Domain.Entities.Baremes;
using Domain.Entities.CommentaireGlobaux;
using Domain.Entities.Criteres;
using Domain.Entities.Grilles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Elements
{
	public class Element : AuditableAndSoftDeletableEntity
	{
		public string Titre { get; set; } = default!;
		public int Ponderation { get; set; } = default!;
		public List<CommentaireGlobal> Commentaire { get; set; } = new List<CommentaireGlobal>();
		public List<Critere> Critere { get; set; } = new List<Critere>();
		public List<Bareme> Bareme { get; set; } = new List<Bareme>();

		//public Grille Grille { get; set; }
		public Guid GrilleId { get; set; }
        
    }
}
