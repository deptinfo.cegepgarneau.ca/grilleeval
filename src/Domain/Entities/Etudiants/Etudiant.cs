using Domain.Common;
using Domain.Entities.CoursEtudiants;
using Domain.Entities.Elements;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Domain.Entities.Etudiants
{
    public class Etudiant : AuditableAndSoftDeletableEntity
    {
        public string Da { get; set; } = default!;
        public string Nom { get; set; } = default!;
        public string Prenom { get; set; } = default!;
        [JsonIgnore]
        public List<EtudiantCours> EtudiantsCours { get; set; } = default!;

    }
}
