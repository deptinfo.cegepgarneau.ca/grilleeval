﻿using Domain.Common;
using Domain.Entities.Books;
using Domain.Entities.CommentairesEtudiant;
using Domain.Entities.Elements;
using Domain.Entities.Etudiants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Domain.Entities.ElementsEtudiant
{
    public class ElementEtudiant:AuditableAndSoftDeletableEntity       
    {
       
        public Etudiant Etudiant { get; set; } = default!;

        public float? NoteElement { get; set; }

        public DateTime Date { get; set; } = default!;

        public Element ElementEvalue { get; set; } = default!;

        public List<CommentaireEtudiant> CommentairesEtudiant { get; set; } = default!;

        public Guid GrilleId { get; set; } = default!;
    }
}
