﻿using Domain.Common;
using Domain.Entities.DossierCours;
using Domain.Entities.Etudiants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Xml;

namespace Domain.Entities.CoursEtudiants
{
    public class EtudiantCours : AuditableAndSoftDeletableEntity
    {
        public Guid EtudiantId { get; set; }
        public Etudiant Etudiant { get; set; } = default!;

        [JsonIgnore]
		public Cours Cours { get; set; } = default!;
        public Guid CoursId { get; set; }
        public string Groupe { get; set; } = default!;
    }
}
