﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Criteres
{
	public class Critere : AuditableAndSoftDeletableEntity
	{
		public string Detail { get; set; } = default!;
        public Guid ElementId { get; set; }
    }
}
